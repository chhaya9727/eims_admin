ALTER TABLE `guard_duty` ADD `reserve_shift_guard_id` VARCHAR(11) NOT NULL AFTER `night_shift_guard_id`;

ALTER TABLE `guard_duty`
ADD `first_shift_guard_id` varchar(11) NOT NULL AFTER `checkpost_distillery_id`,
ADD `second_shift_guard_id` varchar(11) NOT NULL AFTER `first_shift_guard_id`,
ADD `night_shift_guard_id` varchar(11) NOT NULL AFTER `second_shift_guard_id`,
ADD `reserve_shift_guard_id` varchar(11) NOT NULL AFTER `night_shift_guard_id`;


