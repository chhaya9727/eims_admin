-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `checkpost_distillery`;
CREATE TABLE `checkpost_distillery` (
  `checkpost_distillery_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `address` varchar(250) NOT NULL,
  `no_of_eg_for_first_shift` int(11) NOT NULL,
  `no_of_eg_for_second_shift` int(11) NOT NULL,
  `no_of_eg_for_night_shift` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  PRIMARY KEY (`checkpost_distillery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `checkpost_distillery` (`checkpost_distillery_id`, `name`, `address`, `no_of_eg_for_first_shift`, `no_of_eg_for_second_shift`, `no_of_eg_for_night_shift`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1,	'Dabhel Checkpost',	'Dabhel Check Post, Vapi - Daman Main Rd, Dabhel, Daman, Daman and Diu 396210, India',	2,	2,	2,	1,	'2022-02-01 12:12:19',	1,	'2022-02-01 13:09:24',	0),
(2,	'Pataliya Checkpost',	'Pataliya Checkpost',	2,	2,	1,	1,	'2022-02-01 13:26:00',	1,	'2022-02-15 19:45:54',	0),
(3,	'Kachigam Checkpost',	'Kachigam Checkpost',	1,	1,	2,	1,	'2022-02-01 13:26:28',	1,	'2022-02-15 19:48:27',	0),
(4,	'Atiyawad Checkpost',	'Atiyawad Checkpost',	1,	2,	2,	1,	'2022-02-01 13:26:55',	1,	'2022-02-15 19:50:22',	0),
(5,	'Zari Checkpost',	'Zari Checkpost',	1,	2,	2,	1,	'2022-02-01 13:28:30',	0,	'0000-00-00 00:00:00',	0),
(6,	'Bamanpuja Checkpost',	'Bamanpuja Checkpost',	1,	1,	2,	1,	'2022-02-01 13:28:54',	0,	'0000-00-00 00:00:00',	0),
(7,	'Blossom Industries',	'Blossom Industries',	1,	1,	1,	1,	'2022-02-01 13:29:23',	0,	'0000-00-00 00:00:00',	0),
(8,	'Khemani Distillery',	'Khemani Distillery',	1,	1,	1,	1,	'2022-02-01 13:29:49',	0,	'0000-00-00 00:00:00',	0),
(9,	'Royal Distillery',	'Royal Distillery',	0,	1,	1,	1,	'2022-02-01 13:30:11',	1,	'2022-02-15 19:55:32',	0),
(10,	'Silverstar Distillery',	'Silverstar Distillery',	1,	1,	0,	1,	'2022-02-01 13:31:14',	0,	'0000-00-00 00:00:00',	0),
(11,	'Jupiter Distillery',	'Jupiter Distillery',	1,	1,	0,	1,	'2022-02-01 13:31:35',	0,	'0000-00-00 00:00:00',	0),
(12,	'Krimpi Distillery',	'Krimpi Distillery',	1,	1,	0,	1,	'2022-02-01 13:31:54',	0,	'0000-00-00 00:00:00',	0),
(13,	'Dharmesh Distillery',	'Dharmesh Distillery',	1,	1,	0,	1,	'2022-02-01 13:32:19',	0,	'0000-00-00 00:00:00',	0),
(14,	'Jani Vankad Checkpost',	'Jani Vankad Checkpost',	2,	1,	1,	1,	'2022-02-01 13:26:55',	1,	'2022-02-15 20:08:09',	0),
(15,	'Daman Distillery',	'Daman Distillery',	1,	0,	0,	1,	'2022-02-15 19:06:13',	1,	'2022-02-15 19:09:43',	0);

DROP TABLE IF EXISTS `guard`;
CREATE TABLE `guard` (
  `guard_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `guard_name` varchar(200) NOT NULL,
  `guard_designation` varchar(200) NOT NULL,
  `guard_mob` varchar(10) NOT NULL,
  `guard_email` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `status_datetime` datetime NOT NULL,
  `submitted_datetime` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` int(11) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`guard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `guard` (`guard_id`, `user_id`, `guard_name`, `guard_designation`, `guard_mob`, `guard_email`, `status`, `status_datetime`, `submitted_datetime`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1,	1,	'Shri. Dharmesh G Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(2,	1,	'Shri Kamlesh Dhodi',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(3,	1,	'Shri. Pinal G Patel ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(4,	1,	'Shri. Sagarkumar R Patel ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(5,	1,	'Shri. Ronalkumar N Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(6,	1,	'Shri. Darpan R Bhandari ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(7,	1,	'Shri Rajendra Solanki',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(8,	1,	'Shri. Rahul D Solanki',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(9,	1,	'Shri. Jaysukh D Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(10,	1,	'Kum. Sanjana Mangela',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(11,	1,	'Shri. Rahul H. Patel ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(12,	1,	'Shri. Prakash J Sikotariya ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(13,	1,	'Shri Jaykumar K. Dhodi',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(14,	1,	'Shri. Axay Kotiya',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(15,	1,	'Shri. Surajsingh S Tamta ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(16,	1,	'Shri. Jignesh H Bhandari ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(17,	1,	'Shri. Pravinkumar R Baraiya',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(18,	1,	'Shri. Kamlesh N Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(19,	1,	'Shri. Bhupendra Halpati',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(20,	1,	'Shri. Tushar Laxman ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(21,	1,	'Shri. Krinal P Patel ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(22,	1,	'Shri. Shivam Pandey ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(23,	1,	'Shri. Ronit K Patel ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(24,	1,	'Shri. Ravikumar Singh ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(25,	1,	'Shri. Divyesh R Halpati',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(26,	1,	'Shri. Rohit Singh',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(27,	1,	'Shri. Bhavesh Apandkar ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(28,	1,	'Shri Hemant Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(29,	1,	'Shri. Yatinkumar S Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(30,	1,	'Shri. Harishtey N Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(31,	1,	'Shri. Amit Tiwari',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(32,	1,	'Shri. Patel Yagneshkumar S',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(33,	1,	'Shri Bharat Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(34,	1,	'Shri Pandu Chaudhary',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(35,	1,	'Shri. Bhavish R Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(36,	1,	'Shri Nimesh S. Ahir',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(37,	1,	'Shri. Swaraj Singh G',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(38,	1,	'Shri. Mayank J Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(39,	1,	'Shri. Dhodiya Sauravkumar K',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(40,	1,	'Shri Srinath Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(41,	1,	'Shri. Chetankumar Pavara',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(42,	1,	'Shri. Sunil P Dhodia ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(43,	1,	'Shri Jayesh K. Bij',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(44,	1,	'Shri Ganesh Kurkutia',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(45,	1,	'Shri. Sanjaykumar V Solanki',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(46,	1,	'Shri. Vijay Pal',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(47,	1,	'Shri. Bamaniya Sanjaykumar R',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(48,	1,	'Shri. Sumiten C Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(49,	1,	'Shri Takhat Rathod',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(50,	1,	'Shri. Manish Kumar Yadav ',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(51,	1,	'Shri. Nitin R Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(52,	1,	'Shri. Vedang D Ramanuj',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0),
(53,	1,	'Shri. Akshay N Patel',	'Guard',	'',	'',	2,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	'0000-00-00 00:00:00',	0,	0,	0);

DROP TABLE IF EXISTS `guard_duty`;
CREATE TABLE `guard_duty` (
  `guard_duty_id` int(11) NOT NULL AUTO_INCREMENT,
  `checkpost_distillery_id` int(11) NOT NULL,
  `first_shift_guard_id` varchar(11) NOT NULL,
  `second_shift_guard_id` varchar(11) NOT NULL,
  `night_shift_guard_id` varchar(11) NOT NULL,
  `reserve_shift_guard_id` varchar(11) NOT NULL,
  `first_shift_guard_name` varchar(200) NOT NULL,
  `second_shift_guard_name` varchar(200) NOT NULL,
  `night_shift_guard_name` varchar(200) NOT NULL,
  `reserve_shift_guard_name` varchar(200) NOT NULL,
  `week_from` date NOT NULL,
  `week_to` date NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_time` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  PRIMARY KEY (`guard_duty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `guard_duty` (`guard_duty_id`, `checkpost_distillery_id`, `first_shift_guard_id`, `second_shift_guard_id`, `night_shift_guard_id`, `reserve_shift_guard_id`, `first_shift_guard_name`, `second_shift_guard_name`, `night_shift_guard_name`, `reserve_shift_guard_name`, `week_from`, `week_to`, `created_by`, `created_time`, `is_delete`) VALUES
(1,	1,	'3,10,',	'6,31,',	'21,36,',	'50',	'Shri. Pinal G Patel,Kum. Sanjana Mangela,',	'Shri. Darpan R Bhandari,Shri. Amit Tiwari,',	'Shri. Krinal P Patel,Shri Nimesh S. Ahir,',	'Shri. Manish Kumar Yadav ',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(2,	2,	'44,34,',	'23,29,',	'19',	'13',	'Shri Ganesh Kurkutia,Shri Pandu Chaudhary,',	'Shri. Ronit K Patel,Shri. Yatinkumar S Patel,',	'Shri. Bhupendra Halpati,',	'Shri Jaykumar K. Dhodi',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(3,	3,	'22,',	'42,',	'28,1,',	'40',	'Shri. Shivam Pandey,',	'Shri. Sunil P Dhodia,',	'Shri Hemant Patel,Shri. Dharmesh G Patel,',	'Shri Srinath Patel',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(4,	4,	'24,',	'14,53,',	'18,7,',	'',	'Shri. Ravikumar Singh,',	'Shri. Axay Kotiya,Shri. Akshay N Patel,',	'Shri. Kamlesh N Patel,Shri Rajendra Solanki,',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(5,	5,	'38,',	'39,11,',	'45,5,',	'',	'Shri. Mayank J Patel,',	'Shri. Dhodiya Sauravkumar K,Shri. Rahul H. Patel,',	'Shri. Sanjaykumar V Solanki,Shri. Ronalkumar N Patel,',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(6,	6,	'48,',	'37,',	'35,27,',	'0',	'Shri. Sumiten C Patel,',	'Shri. Swaraj Singh G,',	'Shri. Bhavish R Patel,Shri. Bhavesh Apandkar ,',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(7,	7,	'25,',	'26,',	'2,',	'0',	'Shri. Divyesh R Halpati,',	'Shri. Rohit Singh,',	'Shri Kamlesh Dhodi,',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(8,	8,	'43,',	'20,',	'30,',	'0',	'Shri Jayesh K. Bij,',	'Shri. Tushar Laxman,',	'Shri. Harishtey N Patel,',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(9,	9,	'0,',	'33,',	'17,',	'0',	'-',	'Shri Bharat Patel,',	'Shri. Pravinkumar R Baraiya,',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(10,	10,	'16,',	'41,',	'0',	'0',	'Shri. Jignesh H Bhandari,',	'Shri. Chetankumar Pavara,',	'-',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(11,	11,	'46,',	'51,',	'0',	'0',	'Shri. Vijay Pal,',	'Shri. Nitin R Patel,',	'-',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(12,	12,	'12,',	'47,',	'0',	'0',	'Shri. Prakash J Sikotariya,',	'Shri. Bamaniya Sanjaykumar R,',	'-',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(13,	13,	'9,',	'49,',	'0',	'0',	'Shri. Jaysukh D Patel,',	'Shri Takhat Rathod,',	'-',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(14,	14,	'52,32,',	'8,',	'4,',	'0',	'Shri. Vedang D Ramanuj,Shri. Patel Yagneshkumar S,',	'Shri. Rahul D Solanki,',	'Shri. Sagarkumar R Patel,',	'-',	'2022-02-13',	'2022-02-19',	1,	'2022-02-04 14:00:59',	0),
(15,	15,	'15,',	'0,',	'0,',	'0',	'Shri. Surajsingh S Tamta',	'-',	'-',	'-',	'2022-02-13',	'2022-02-19',	1,	'0000-00-00 00:00:00',	0);

DROP TABLE IF EXISTS `logs_change_pin`;
CREATE TABLE `logs_change_pin` (
  `logs_change_pin_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `old_pin` text NOT NULL,
  `new_pin` text NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`logs_change_pin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `logs_email`;
CREATE TABLE `logs_email` (
  `email_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `email_type` tinyint(1) NOT NULL,
  `module_type` tinyint(4) NOT NULL,
  `module_id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`email_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `logs_login_details`;
CREATE TABLE `logs_login_details` (
  `logs_login_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `login_timestamp` int(11) NOT NULL,
  `logout_timestamp` int(11) NOT NULL,
  `logs_data` text NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` datetime NOT NULL,
  PRIMARY KEY (`logs_login_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `logs_sms`;
CREATE TABLE `logs_sms` (
  `logs_sms_id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_type` tinyint(1) NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `ErrorCode` tinyint(4) NOT NULL,
  `ErrorMessage` text NOT NULL,
  `JobId` varchar(100) NOT NULL,
  `MessageData` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`logs_sms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `otp`;
CREATE TABLE `otp` (
  `otp_id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_number` varchar(10) NOT NULL,
  `otp` varchar(10) NOT NULL,
  `otp_type` tinyint(1) NOT NULL,
  `created_time` datetime NOT NULL,
  `is_expired` tinyint(1) NOT NULL,
  PRIMARY KEY (`otp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sa_logs_change_password`;
CREATE TABLE `sa_logs_change_password` (
  `sa_logs_change_password_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sa_user_id` bigint(20) NOT NULL,
  `old_password` text NOT NULL,
  `new_password` text NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`sa_logs_change_password_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sa_logs_login_details`;
CREATE TABLE `sa_logs_login_details` (
  `sa_logs_login_details_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sa_user_id` bigint(20) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `login_timestamp` int(11) NOT NULL,
  `logout_timestamp` int(11) NOT NULL,
  `logs_data` text NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` datetime NOT NULL,
  PRIMARY KEY (`sa_logs_login_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sa_users`;
CREATE TABLE `sa_users` (
  `sa_user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_type` tinyint(1) NOT NULL,
  `district` tinyint(1) NOT NULL,
  `is_deactive` tinyint(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`sa_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sa_users` (`sa_user_id`, `name`, `username`, `password`, `user_type`, `district`, `is_deactive`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1,	'Admin',	'admin',	'f6e1823488b1bd4e72127c4b3005cf0862396666366436393539326636656236353437636264623065383461323334653363316339653864303632666264613634333965346439623931383036366334400b37037b94f4452462',	1,	0,	0,	1,	'2020-03-25 17:20:00',	1,	'2020-10-20 20:08:47',	0),
(2,	'EXCISE DEPARTMENT DAMAN',	'excise.daman',	'457f734d2010c82726cd137993565c396231343161613631666435336330383135643934323331326332383164626237313731623539356565316533343564623265346261363864613934323365386126fc3088ec664a28ed7d',	2,	1,	0,	1,	'2020-08-28 13:44:06',	1,	'2022-02-09 15:38:56',	0);

DROP TABLE IF EXISTS `sa_user_type`;
CREATE TABLE `sa_user_type` (
  `sa_user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`sa_user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sa_user_type` (`sa_user_type_id`, `type`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1,	'Admin',	1,	'2020-10-16 10:41:00',	1,	'2020-10-16 10:41:00',	0),
(2,	'Department',	1,	'2020-10-16 10:41:00',	0,	'0000-00-00 00:00:00',	0);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(1) NOT NULL,
  `applicant_name` varchar(100) NOT NULL,
  `applicant_address` varchar(200) NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `pin` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `is_verify_mobile` tinyint(1) NOT NULL,
  `verify_mobile_datetime` datetime NOT NULL,
  `is_verify_email` tinyint(1) NOT NULL,
  `verify_email_datetime` datetime NOT NULL,
  `temp_access_token` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2022-02-16 04:27:42