ALTER TABLE `guard_duty`
ADD `reserve_shift_guard_name` varchar(200) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `night_shift_guard_name`;

-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `guard_duty` (`guard_duty_id`, `checkpost_distillery_id`, `first_shift_guard_id`, `second_shift_guard_id`, `night_shift_guard_id`, `reserve_shift_guard_id`, `first_shift_guard_name`, `second_shift_guard_name`, `night_shift_guard_name`, `reserve_shift_guard_name`, `week_from`, `week_to`, `created_by`, `created_time`, `is_delete`) VALUES
(1,	1,	'3,10,',	'6,31,',	'21,55,',	'51',	'Guard 3,Guard 10,',	'Guard 6,Guard 31,',	'Guard 21,Guard 55,',	'Guard 51',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(2,	2,	'44,34,',	'60,58,',	'19,40,',	'13',	'Guard 44,Guard 34,',	'Guard 60,Guard 58,',	'Guard 19,Guard 40,',	'Guard 13',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(3,	3,	'22,',	'42,56,',	'28,1,',	'70',	'Guard 22,',	'Guard 42,Guard 56,',	'Guard 28,Guard 1,',	'Guard 70',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(4,	4,	'24,63,',	'14,54,',	'18,66,',	'64',	'Guard 24,Guard 63,',	'Guard 14,Guard 54,',	'Guard 18,Guard 66,',	'Guard 64',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(5,	5,	'38,',	'39,11,',	'67,5,',	'15',	'Guard 38,',	'Guard 39,Guard 11,',	'Guard 67,Guard 5,',	'Guard 15',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(6,	6,	'49,',	'37,41,',	'62,27,',	'0',	'Guard 49,',	'Guard 37,Guard 41,',	'Guard 62,Guard 27,',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(7,	7,	'25,',	'26,',	'2,',	'0',	'Guard 25,',	'Guard 26,',	'Guard 2,',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(8,	8,	'43,',	'20,',	'30,',	'0',	'Guard 43,',	'Guard 20,',	'Guard 30,',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(9,	9,	'35,',	'33,',	'17,',	'0',	'Guard 35,',	'Guard 33,',	'Guard 17,',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(10,	10,	'16,',	'61,',	'0',	'0',	'Guard 16,',	'Guard 61,',	'-',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(11,	11,	'46,',	'52,',	'0',	'0',	'Guard 46,',	'Guard 52,',	'-',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(12,	12,	'65,',	'57,',	'0',	'0',	'Guard 65,',	'Guard 57,',	'-',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(13,	13,	'9,',	'50,',	'0',	'0',	'Guard 9,',	'Guard 50,',	'-',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0),
(14,	14,	'53,32,',	'8,68,',	'4,45,',	'0',	'Guard 53,Guard 32,',	'Guard 8,Guard 68,',	'Guard 4,Guard 45,',	'-',	'2022-02-01',	'2022-02-28',	1,	'2022-02-04 14:00:59',	0);

-- 2022-02-04 08:48:48