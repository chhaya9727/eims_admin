-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2022 at 11:42 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nicdexgx_eims`
--

-- --------------------------------------------------------

--
-- Table structure for table `checkpost_distillery`
--

CREATE TABLE `checkpost_distillery` (
  `checkpost_distillery_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(250) NOT NULL,
  `no_of_eg_for_first_shift` int(11) NOT NULL,
  `no_of_eg_for_second_shift` int(11) NOT NULL,
  `no_of_eg_for_night_shift` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkpost_distillery`
--

INSERT INTO `checkpost_distillery` (`checkpost_distillery_id`, `name`, `address`, `no_of_eg_for_first_shift`, `no_of_eg_for_second_shift`, `no_of_eg_for_night_shift`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1, 'Dabhel Checkpost', 'Dabhel Check Post, Vapi - Daman Main Rd, Dabhel, Daman, Daman and Diu 396210, India', 2, 2, 2, 1, '2022-02-01 12:12:19', 1, '2022-02-01 13:09:24', 0),
(2, 'Pataliya Checkpost', 'Pataliya Checkpost', 2, 2, 2, 1, '2022-02-01 13:26:00', 0, '0000-00-00 00:00:00', 0),
(3, 'Kachigam Checkpost', 'Kachigam Checkpost', 1, 2, 2, 1, '2022-02-01 13:26:28', 0, '0000-00-00 00:00:00', 0),
(4, 'Atiyawad Checkpost', 'Atiyawad Checkpost', 2, 2, 2, 1, '2022-02-01 13:26:55', 0, '0000-00-00 00:00:00', 0),
(5, 'Zari Checkpost', 'Zari Checkpost', 1, 2, 2, 1, '2022-02-01 13:28:30', 0, '0000-00-00 00:00:00', 0),
(6, 'Bamanpuja Checkpost', 'Bamanpuja Checkpost', 1, 2, 2, 1, '2022-02-01 13:28:54', 0, '0000-00-00 00:00:00', 0),
(7, 'Blossom Industries', 'Blossom Industries', 1, 1, 1, 1, '2022-02-01 13:29:23', 0, '0000-00-00 00:00:00', 0),
(8, 'Khemani Distillery', 'Khemani Distillery', 1, 1, 1, 1, '2022-02-01 13:29:49', 0, '0000-00-00 00:00:00', 0),
(9, 'Royal Distillery', 'Royal Distillery', 1, 1, 1, 1, '2022-02-01 13:30:11', 0, '0000-00-00 00:00:00', 0),
(10, 'Silverstar Distillery', 'Silverstar Distillery', 1, 1, 0, 1, '2022-02-01 13:31:14', 0, '0000-00-00 00:00:00', 0),
(11, 'Jupiter Distillery', 'Jupiter Distillery', 1, 1, 0, 1, '2022-02-01 13:31:35', 0, '0000-00-00 00:00:00', 0),
(12, 'Krimpi Distillery', 'Krimpi Distillery', 1, 1, 0, 1, '2022-02-01 13:31:54', 0, '0000-00-00 00:00:00', 0),
(13, 'Dharmesh Distillery', 'Dharmesh Distillery', 1, 1, 0, 1, '2022-02-01 13:32:19', 0, '0000-00-00 00:00:00', 0),
(14, 'Jani Vankad Checkpost', 'Jani Vankad Checkpost', 2, 2, 2, 1, '2022-02-01 13:26:55', 0, '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkpost_distillery`
--
ALTER TABLE `checkpost_distillery`
  ADD PRIMARY KEY (`checkpost_distillery_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkpost_distillery`
--
ALTER TABLE `checkpost_distillery`
  MODIFY `checkpost_distillery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
