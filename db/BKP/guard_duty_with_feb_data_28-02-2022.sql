-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2022 at 12:11 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nicdexgx_eims`
--

-- --------------------------------------------------------

--
-- Table structure for table `guard_duty`
--

CREATE TABLE `guard_duty` (
  `guard_duty_id` int(11) NOT NULL,
  `checkpost_distillery_id` int(11) NOT NULL,
  `first_shift_guard_id` varchar(11) NOT NULL,
  `second_shift_guard_id` varchar(11) NOT NULL,
  `night_shift_guard_id` varchar(11) NOT NULL,
  `reserve_shift_guard_id` varchar(11) NOT NULL,
  `first_shift_guard_name` varchar(200) NOT NULL,
  `second_shift_guard_name` varchar(200) NOT NULL,
  `night_shift_guard_name` varchar(200) NOT NULL,
  `reserve_shift_guard_name` varchar(200) NOT NULL,
  `week_from` date NOT NULL,
  `week_to` date NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_time` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guard_duty`
--

INSERT INTO `guard_duty` (`guard_duty_id`, `checkpost_distillery_id`, `first_shift_guard_id`, `second_shift_guard_id`, `night_shift_guard_id`, `reserve_shift_guard_id`, `first_shift_guard_name`, `second_shift_guard_name`, `night_shift_guard_name`, `reserve_shift_guard_name`, `week_from`, `week_to`, `is_freeze`, `created_by`, `created_time`, `is_delete`) VALUES
(1, 1, '25,38', '12,26,', '17,51', '39', 'Shri. Dharmesh G Patel,Shri. Sagarkumar R Patel,', 'Shri Kamlesh Dhodi,Shri. Ronalkumar N Patel,', 'Shri. Pinal G Patel,Shri. Darpan R Bhandari,', 'Shri. Nitin R Patel', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(2, 2, '3,59,', '32,46,', '41,7,', '34', 'Shri Rajendra Solanki,Kum. Sanjana Mangela,', 'Shri. Rahul D Solanki,Shri. Rahul H. Patel,', 'Shri. Jaysukh D Patel,Shri Mahesh Natu', 'Shri. Vedang D Ramanuj', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(3, 3, '15,', '61,', '54,33,', '45', 'Shri. Prakash J Sikotariya,', 'Shri Jaykumar K. Dhodi,', 'Shri. Axay Kotiya,Shri. Vijay Pal', 'Shri. Akshay N Patel', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(4, 4, '13,16,', '30,12,', '18,42,', '6', 'Shri Pratap S Vaja,Shri. Pravinkumar R Baraiya,', 'Shri. Surajsingh S Tamta,Shri. Kamlesh N Patel,', 'Shri. Jignesh H Bhandari,Shri. Bhupendra Halpati,', 'Shri Pradip N Solanki', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(5, 5, '38,29,', '24,', '28,10,', '14', 'Shri. Tushar Laxman,Shri. Ronit K Patel ', 'Shri. Krinal P Patel,', 'Shri. Shivam Pandey,Shri Baudas Bica,', 'Smt. Ramila P Baria', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(6, 6, '37,', '43,23', '58,53', '8', 'Shri. Ravikumar Singh,', 'Shri. Divyesh R Halpati,Shri. Rohit Singh', 'Shri. Bhavesh Apandkar,Shri Hemant Patel,', 'Shri Vijay M Jadav', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(7, 7, '9,55,', '56,', '57,44', '', 'Shri. Premji S Solanki,Shri. Yatinkumar S Patel', 'Shri. Harishtey N Patel,', 'Shri. Amit Tiwari,Shri. Patel Yagneshkumar S', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(8, 8, '2,', '11,', '40,', '', 'Shri Bharat Patel,', 'Shri Pandu Chaudhary,', 'Shri. Bhavish R Patel,', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(9, 9, '63,', '31,', '47,', '', 'Shri Nimesh S. Ahir,', 'Shri. Swaraj Singh G,', 'Shri. Mayank J Patel,', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(10, 10, '1,', '49,', '50', '', 'Shri. Kantilal H Solanki,', 'Shri. Dhodiya Sauravkumar K,', 'Shri Shrinath Patel,', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(11, 11, '22,', '21,', '', '', 'Shri. Chetankumar Pavara,', 'Shri. Sunil P Dhodia ,', '-', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(12, 12, '62,', '5,', '', '', 'Shri Jayesh K. Bij,', 'Shri Ganesh Kurkutia,', '-', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(13, 13, '35,', '49,', '', '', 'Shri. Sanjaykumar V Solanki,', '', '-', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(14, 14, '52,', '48,', '38,', '', 'Shri. Bamaniya Sanjaykumar R,', 'Shri. Sumiten C Patel,', 'Shri. Sagarkumar R Patel,', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0),
(15, 15, '4,', '36', '', '', 'Shri Takhat Rathod,', 'Shri. Manish Kumar Yadav ', '-', '-', '2022-02-01', '2022-02-28', 1, 1, '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guard_duty`
--
ALTER TABLE `guard_duty`
  ADD PRIMARY KEY (`guard_duty_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guard_duty`
--
ALTER TABLE `guard_duty`
  MODIFY `guard_duty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
