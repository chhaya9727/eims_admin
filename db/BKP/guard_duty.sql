-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2022 at 08:57 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nicdexgx_eims`
--

-- --------------------------------------------------------

--
-- Table structure for table `guard_duty`
--

CREATE TABLE `guard_duty` (
  `guard_duty_id` int(11) NOT NULL,
  `checkpost_distillery_id` int(11) NOT NULL,
  `first_shift_guard_name` varchar(200) NOT NULL,
  `second_shift_guard_name` varchar(200) NOT NULL,
  `night_shift_guard_name` varchar(200) NOT NULL,
  `week_from` date NOT NULL,
  `week_to` date NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_time` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guard_duty`
--

INSERT INTO `guard_duty` (`guard_duty_id`, `checkpost_distillery_id`, `first_shift_guard_name`, `second_shift_guard_name`, `night_shift_guard_name`, `week_from`, `week_to`, `created_by`, `created_time`, `is_delete`) VALUES
(43, 1, 'Guard 6,Guard 63,', 'Guard 38,Guard 59,', 'Guard 32,Guard 28,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:46', 0),
(44, 2, 'Guard 42,Guard 7,', 'Guard 33,Guard 60,', 'Guard 40,Guard 46,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:46', 0),
(45, 3, 'Guard 54,', 'Guard 30,Guard 24,', 'Guard 9,Guard 69,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:46', 0),
(46, 4, 'Guard 50,Guard 66,', 'Guard 48,Guard 57,', 'Guard 55,Guard 4,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:46', 0),
(47, 5, 'Guard 36,', 'Guard 53,Guard 58,', 'Guard 10,Guard 1,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:46', 0),
(48, 6, 'Guard 34,', 'Guard 17,Guard 37,', 'Guard 26,Guard 35,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0),
(49, 7, 'Guard 65,', 'Guard 41,', 'Guard 27,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0),
(50, 8, 'Guard 11,', 'Guard 44,', 'Guard 14,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0),
(51, 9, 'Guard 51,', 'Guard 64,', 'Guard 62,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0),
(52, 10, 'Guard 19,', 'Guard 22,', '-', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0),
(53, 11, 'Guard 5,', 'Guard 18,', '-', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0),
(54, 12, 'Guard 29,', 'Guard 52,', '-', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0),
(55, 13, 'Guard 68,', 'Guard 2,', '-', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0),
(56, 14, 'Guard 49,Guard 21,', 'Guard 25,Guard 8,', 'Guard 3,Guard 45,', '2022-02-06', '2022-02-12', 1, '2022-02-04 11:14:47', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guard_duty`
--
ALTER TABLE `guard_duty`
  ADD PRIMARY KEY (`guard_duty_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guard_duty`
--
ALTER TABLE `guard_duty`
  MODIFY `guard_duty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
