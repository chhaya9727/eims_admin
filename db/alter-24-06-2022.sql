ALTER TABLE `guard`
ADD `is_fs_done` tinyint(1) NOT NULL AFTER `guard_email`,
ADD `is_ss_done` tinyint(1) NOT NULL AFTER `is_fs_done`,
ADD `is_ns_done` tinyint(1) NOT NULL AFTER `is_ss_done`,
ADD `is_rs_done` tinyint(1) NOT NULL AFTER `is_ns_done`;


ALTER TABLE `guard`
DROP `status`,
DROP `status_datetime`,
DROP `submitted_datetime`;

ALTER TABLE `checkpost_distillery`
ADD `no_of_eg_for_general` int(11) NOT NULL AFTER `no_of_eg_for_night_shift`;

ALTER TABLE `checkpost_distillery`
ADD `shifts_category` tinyint(1) NOT NULL AFTER `address`;

ALTER TABLE `checkpost_distillery`
CHANGE `no_of_eg_for_general` `no_of_eg_for_general_shift` int(11) NOT NULL AFTER `no_of_eg_for_night_shift`;

ALTER TABLE `guard_duty`
ADD `general_shift_guard_id` varchar(11) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `night_shift_guard_id`,
ADD `general_shift_guard_name` varchar(200) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `night_shift_guard_name`;

ALTER TABLE `guard`
ADD `is_gs_done` tinyint(1) NOT NULL AFTER `is_ns_done`;