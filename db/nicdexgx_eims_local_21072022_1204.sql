-- Adminer 4.8.1 MySQL 5.5.5-10.4.17-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `checkpost_distillery`;
CREATE TABLE `checkpost_distillery` (
  `checkpost_distillery_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `address` varchar(250) NOT NULL,
  `no_of_eg_for_first_shift` int(11) NOT NULL,
  `no_of_eg_for_second_shift` int(11) NOT NULL,
  `no_of_eg_for_night_shift` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  PRIMARY KEY (`checkpost_distillery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `checkpost_distillery` (`checkpost_distillery_id`, `name`, `address`, `no_of_eg_for_first_shift`, `no_of_eg_for_second_shift`, `no_of_eg_for_night_shift`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1,	'Dabhel Checkpost',	'Dabhel Check Post',	2,	2,	2,	1,	'2022-02-01 12:12:19',	2,	'2022-05-27 16:03:39',	0),
(2,	'Pataliya Checkpost',	'Pataliya Checkpost',	1,	1,	2,	1,	'2022-02-01 13:26:00',	2,	'2022-05-27 16:03:49',	0),
(3,	'Kachigam Checkpost',	'Kachigam Checkpost',	1,	1,	2,	1,	'2022-02-01 13:26:28',	2,	'2022-05-27 16:03:58',	0),
(4,	'Atiyawad Checkpost',	'Atiyawad Checkpost',	1,	1,	2,	1,	'2022-02-01 13:26:55',	2,	'2022-05-27 16:04:11',	0),
(5,	'Jani Vankad Checkpost',	'Jani Vankad Checkpost',	1,	1,	2,	1,	'2022-02-01 13:26:55',	2,	'2022-05-27 16:04:21',	0),
(6,	'Zari Checkpost',	'Zari Checkpost',	1,	1,	2,	1,	'2022-02-01 13:28:30',	2,	'2022-05-27 16:04:30',	0),
(7,	'Bamanpuja Checkpost',	'Bamanpuja Checkpost',	1,	1,	2,	1,	'2022-02-01 13:28:54',	2,	'2022-05-27 16:04:39',	0),
(8,	'Blossom Industries',	'Blossom Industries',	1,	1,	1,	1,	'2022-02-01 13:29:23',	2,	'2022-05-27 16:04:44',	0),
(9,	'Khemani Distillery',	'Khemani Distillery',	1,	1,	1,	1,	'2022-02-01 13:29:49',	2,	'2022-05-27 16:04:49',	0),
(10,	'Royal Distillery',	'Royal Distillery',	1,	1,	0,	1,	'2022-02-01 13:30:11',	2,	'2022-05-27 16:05:02',	0),
(11,	'Silverstar Distillery',	'Silverstar Distillery',	1,	1,	0,	1,	'2022-02-01 13:31:14',	2,	'2022-05-27 16:05:07',	0),
(12,	'Jupiter Distillery',	'Jupiter Distillery',	1,	1,	0,	1,	'2022-02-01 13:31:35',	2,	'2022-05-27 16:05:11',	0),
(13,	'Daman Distillery',	'Daman Distillery',	1,	0,	0,	1,	'2022-02-15 19:06:13',	2,	'2022-05-27 16:05:16',	0),
(14,	'Krimpi Distillery',	'Krimpi Distillery',	1,	1,	0,	1,	'2022-02-01 13:31:54',	2,	'2022-05-27 16:05:20',	0),
(15,	'Dharmesh Distillery',	'Dharmesh Distillery',	1,	1,	0,	1,	'2022-02-01 13:32:19',	2,	'2022-05-27 16:05:23',	0),
(16,	'Zone 1 & 2',	'Dabhel, Ringanwada, Atiyawad, Kalaria, Somnath, Kachigam, Varkund.',	1,	0,	0,	2,	'2022-05-27 16:03:21',	0,	'0000-00-00 00:00:00',	0),
(17,	'Zone 3',	'Bhimpore, Patalia, Jani Vankad',	1,	0,	0,	2,	'2022-05-27 16:05:52',	0,	'0000-00-00 00:00:00',	0),
(18,	'Zone 4 & 5',	'Marwad, Devka, Kadaiya, Dalwada, Nani Daman Muncipal Area and Dunetha',	1,	0,	0,	2,	'2022-05-27 16:06:52',	0,	'0000-00-00 00:00:00',	0),
(19,	'Zone 6',	'Moti Daman Muncipal Area, Jampore to Lighthouse (Sea Face)',	1,	0,	0,	2,	'2022-05-27 16:07:35',	0,	'0000-00-00 00:00:00',	0),
(20,	'Zone 7 & 8',	'Nani Daman Jetty (Sea Face), Devka, Marwad, Kadaiya (Sea Face), Moti Daman Rural Area, etc.',	1,	0,	0,	2,	'2022-05-27 16:10:13',	0,	'0000-00-00 00:00:00',	0),
(21,	'Jampore Beach',	'Jampore Beach',	1,	1,	0,	2,	'2022-05-27 16:10:45',	2,	'2022-05-27 16:11:27',	0),
(22,	'Devka Beach',	'Devka Beach',	1,	1,	0,	2,	'2022-05-27 16:11:21',	0,	'0000-00-00 00:00:00',	0);

DROP TABLE IF EXISTS `guard`;
CREATE TABLE `guard` (
  `guard_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `guard_name` varchar(200) NOT NULL,
  `guard_designation` varchar(200) NOT NULL,
  `guard_mob` varchar(10) NOT NULL,
  `guard_email` varchar(200) NOT NULL,
  `is_fs_done` tinyint(1) NOT NULL,
  `is_ss_done` tinyint(1) NOT NULL,
  `is_ns_done` tinyint(1) NOT NULL,
  `is_rs_done` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` int(11) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`guard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `guard` (`guard_id`, `user_id`, `guard_name`, `guard_designation`, `guard_mob`, `guard_email`, `is_fs_done`, `is_ss_done`, `is_ns_done`, `is_rs_done`, `is_active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1,	1,	'Shri Kantilal H. Solanki',	'Excise Guard',	'',	'',	0,	0,	0,	0,	1,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(2,	1,	'Shri Bharat Patel',	'Excise Guard',	'',	'',	0,	0,	0,	0,	1,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(3,	1,	'Shri Rajendra Solanki',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(4,	1,	'Shri Takhat Rathod',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(5,	1,	'Shri Ganesh Kurkutia',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(6,	1,	'Shri Pradip N Solanki',	'Excise Guard',	'',	'',	0,	0,	0,	0,	1,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(7,	1,	'Shri Mahesh Natu',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(8,	1,	'Shri Vijay M. Jadav',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(9,	1,	'Shri Premji S Solanki',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(10,	1,	'Shri Baudas Bica',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(11,	1,	'Shri Pandu Chaudhary',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(12,	1,	'Shri Kamlesh Dhodi',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(13,	1,	'Shri Pratap S. Vaja',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(14,	1,	'Smt Ramila P Baria',	'Excise Guard',	'',	'',	0,	0,	0,	0,	1,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(15,	1,	'Shri Sikotariya Prakash Jiva',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(16,	1,	'Shri Baraiya Pravinkumar Ramji',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(17,	1,	'Shri Pinalkumar G. Patel',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(18,	1,	'Shri Bhandari Jignesh Hiralal',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	1,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(19,	1,	'Shri Ankitkumar K. Patel',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(20,	1,	'Shri Tushar Laxman',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(21,	1,	'Shri Sunil Pravin Dhodia',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(22,	1,	'Shri Chetankumar Mohanbhai Pavara',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(23,	1,	'Shri Rohit Ramsiddh Singh',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(24,	1,	'Shri Patel Krinal Pramod ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(25,	1,	'Shri Patel Dharmesh Govind ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(26,	1,	'Shri Patel Ronalkumar Naginbhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(27,	1,	'Shri Patel Kamlesh Natu ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(28,	1,	'Shri Pande Shivam Sabhajit',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(29,	1,	'Shri Ronit Kamleshbhai Patel ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(30,	1,	'Shri Tamta Surajsingh Surendrasingh ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(31,	1,	'Shri Singh Swaraj Govind ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(32,	1,	'Shri Solanki Rahul Dinesh ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(33,	1,	'Shri Vijay Pal ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(34,	1,	'Shri Ramanuj Vedangkumar Dinkarbhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(35,	1,	'Shri Solanki Sanjaykumar Varjang ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(36,	1,	'Shri Manish Kumar Yadav',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(37,	1,	'Shri Ravi Kumar Singh',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(38,	1,	'Shri Patel Sagarkumar Ratilal ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(39,	1,	'Shri Patel Nitin Raman',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(40,	1,	'Shri Patel Bhavish Raviabhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(41,	1,	'Shri Patel Jaysukhbhai Dhirubhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(42,	1,	'Shri Halpati Bhupendrakumar Mukeshbhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(43,	1,	'Shri Halpati Divyesh Radakabhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(44,	1,	'Shri Patel Yagneshkumar Surendra',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(45,	1,	'Shri Patel Akshay Naresh',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(46,	1,	'Shri Patel Rahul Hasmukh',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(47,	1,	'Shri Mayank Kumar Jasvant Bhai Patel ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(48,	1,	'Shri Sumiten Chhana Patel ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(49,	1,	'Shri Dhodiya Sauravkumar Kalidas',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(50,	1,	'Shri Patel Shrinath Khalpabhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(51,	1,	'Shri Bhandari Darpan Rajesh ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(52,	1,	'Shri Bamaniya Sanjaykumar Ramji',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(53,	1,	'Shri Patel Hemant Dhirubhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(54,	1,	'Shri Kotiya Axaykumar Karsan ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(55,	1,	'Shri Patel Yatinkumar Shantilal ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(56,	1,	'Shri Patel Harishtey Naginbhai ',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(57,	1,	'Shri Tiwari Amitkumar Shivkumar',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(58,	1,	'Shri Bhavesh Jaydeep Apandkar',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(59,	1,	'Kum Mangela Sanjanaben Ramkumar',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(60,	1,	'Shri Rajbhar  Amit Rambachan',	'Excise Guard ',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(61,	1,	'Shri Jaykumar K. Dhodi',	'Excise Guard',	'',	'',	0,	0,	0,	0,	1,	1,	'2022-02-28 15:38:00',	2,	2022,	0),
(62,	1,	'Shri Jayesh K. Bij',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0),
(63,	1,	'Shri Nimesh S. Ahir',	'Excise Guard',	'',	'',	0,	0,	0,	0,	0,	1,	'2022-02-28 15:38:00',	0,	0,	0);

DROP TABLE IF EXISTS `guard_duty`;
CREATE TABLE `guard_duty` (
  `guard_duty_id` int(11) NOT NULL AUTO_INCREMENT,
  `checkpost_distillery_id` int(11) NOT NULL,
  `first_shift_guard_id` varchar(11) NOT NULL,
  `second_shift_guard_id` varchar(11) NOT NULL,
  `night_shift_guard_id` varchar(11) NOT NULL,
  `reserve_shift_guard_id` varchar(11) NOT NULL,
  `first_shift_guard_name` varchar(200) NOT NULL,
  `second_shift_guard_name` varchar(200) NOT NULL,
  `night_shift_guard_name` varchar(200) NOT NULL,
  `reserve_shift_guard_name` varchar(200) NOT NULL,
  `week_from` date NOT NULL,
  `week_to` date NOT NULL,
  `is_freeze` tinyint(4) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_time` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  PRIMARY KEY (`guard_duty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `guard_duty` (`guard_duty_id`, `checkpost_distillery_id`, `first_shift_guard_id`, `second_shift_guard_id`, `night_shift_guard_id`, `reserve_shift_guard_id`, `first_shift_guard_name`, `second_shift_guard_name`, `night_shift_guard_name`, `reserve_shift_guard_name`, `week_from`, `week_to`, `is_freeze`, `created_by`, `created_time`, `is_delete`) VALUES
(1,	1,	'25,38',	'12,26,',	'17,51',	'39',	'Shri. Dharmesh G Patel,Shri. Sagarkumar R Patel,',	'Shri Kamlesh Dhodi,Shri. Ronalkumar N Patel,',	'Shri. Pinal G Patel,Shri. Darpan R Bhandari,',	'Shri. Nitin R Patel',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(2,	2,	'3,59,',	'32,46,',	'41,7,',	'34',	'Shri Rajendra Solanki,Kum. Sanjana Mangela,',	'Shri. Rahul D Solanki,Shri. Rahul H. Patel,',	'Shri. Jaysukh D Patel,Shri Mahesh Natu',	'Shri. Vedang D Ramanuj',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(3,	3,	'15,',	'61,',	'54,33,',	'45',	'Shri. Prakash J Sikotariya,',	'Shri Jaykumar K. Dhodi,',	'Shri. Axay Kotiya,Shri. Vijay Pal',	'Shri. Akshay N Patel',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(4,	4,	'13,16,',	'30,12,',	'18,42,',	'6',	'Shri Pratap S Vaja,Shri. Pravinkumar R Baraiya,',	'Shri. Surajsingh S Tamta,Shri. Kamlesh N Patel,',	'Shri. Jignesh H Bhandari,Shri. Bhupendra Halpati,',	'Shri Pradip N Solanki',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(5,	5,	'38,29,',	'24,',	'28,10,',	'14',	'Shri. Tushar Laxman,Shri. Ronit K Patel ',	'Shri. Krinal P Patel,',	'Shri. Shivam Pandey,Shri Baudas Bica,',	'Smt. Ramila P Baria',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(6,	6,	'37,',	'43,23',	'58,53',	'8',	'Shri. Ravikumar Singh,',	'Shri. Divyesh R Halpati,Shri. Rohit Singh',	'Shri. Bhavesh Apandkar,Shri Hemant Patel,',	'Shri Vijay M Jadav',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(7,	7,	'9,55,',	'56,',	'57,44',	'',	'Shri. Premji S Solanki,Shri. Yatinkumar S Patel',	'Shri. Harishtey N Patel,',	'Shri. Amit Tiwari,Shri. Patel Yagneshkumar S',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(8,	8,	'2,',	'11,',	'40,',	'',	'Shri Bharat Patel,',	'Shri Pandu Chaudhary,',	'Shri. Bhavish R Patel,',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(9,	9,	'63,',	'31,',	'47,',	'',	'Shri Nimesh S. Ahir,',	'Shri. Swaraj Singh G,',	'Shri. Mayank J Patel,',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(10,	10,	'1,',	'49,',	'50',	'',	'Shri. Kantilal H Solanki,',	'Shri. Dhodiya Sauravkumar K,',	'Shri Shrinath Patel,',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(11,	11,	'22,',	'21,',	'',	'',	'Shri. Chetankumar Pavara,',	'Shri. Sunil P Dhodia ,',	'-',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(12,	12,	'62,',	'5,',	'',	'',	'Shri Jayesh K. Bij,',	'Shri Ganesh Kurkutia,',	'-',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(13,	13,	'35,',	'49,',	'',	'',	'Shri. Sanjaykumar V Solanki,',	'',	'-',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(14,	14,	'52,',	'48,',	'38,',	'',	'Shri. Bamaniya Sanjaykumar R,',	'Shri. Sumiten C Patel,',	'Shri. Sagarkumar R Patel,',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(15,	15,	'4,',	'36',	'',	'',	'Shri Takhat Rathod,',	'Shri. Manish Kumar Yadav ',	'-',	'-',	'2022-02-01',	'2022-02-28',	1,	1,	'0000-00-00 00:00:00',	0),
(16,	1,	'33,27,',	'57,45,',	'55,32,',	'62',	'Shri Vijay Pal ,Shri Patel Kamlesh Natu ,',	'Shri Tiwari Amitkumar Shivkumar,Shri Patel Akshay Naresh,',	'Shri Patel Yatinkumar Shantilal ,Shri Solanki Rahul Dinesh ,',	'Shri Jayesh K. Bij',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(17,	2,	'26,43,',	'47,53,',	'1,3,',	'13',	'Shri Patel Ronalkumar Naginbhai ,Shri Halpati Divyesh Radakabhai ,',	'Shri Mayank Kumar Jasvant Bhai Patel ,Shri Patel Hemant Dhirubhai ,',	'Shri Kantilal H. Solanki,Shri Rajendra Solanki,',	'Shri Pratap S. Vaja',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(18,	3,	'6,21,',	'37,50,',	'5,49,',	'9',	'Shri Pradip N Solanki,Shri Sunil Pravin Dhodia,',	'Shri Ravi Kumar Singh,Shri Patel Shrinath Khalpabhai ,',	'Shri Ganesh Kurkutia,Shri Dhodiya Sauravkumar Kalidas,',	'Shri Premji S Solanki',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(19,	4,	'24,41,',	'35,44,',	'46,30,',	'52',	'Shri Patel Krinal Pramod ,Shri Patel Jaysukhbhai Dhirubhai ,',	'Shri Solanki Sanjaykumar Varjang ,Shri Patel Yagneshkumar Surendra,',	'Shri Patel Rahul Hasmukh,Shri Tamta Surajsingh Surendrasingh ,',	'Shri Bamaniya Sanjaykumar Ramji',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(20,	5,	'11,58,',	'38,',	'56,',	'-',	'Shri Pandu Chaudhary,Shri Bhavesh Jaydeep Apandkar,',	'Shri Patel Sagarkumar Ratilal ,',	'Shri Patel Harishtey Naginbhai ,',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(21,	6,	'19,7,',	'14,29,',	'15,12,',	'-',	'Shri Ankitkumar K. Patel,Shri Mahesh Natu,',	'Smt Ramila P Baria,Shri Ronit Kamleshbhai Patel ,',	'Shri Sikotariya Prakash Jiva,Shri Kamlesh Dhodi,',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(22,	7,	'39,42,',	'10,60,',	'48,22,',	'-',	'Shri Patel Nitin Raman,Shri Halpati Bhupendrakumar Mukeshbhai ,',	'Shri Baudas Bica,Shri Rajbhar  Amit Rambachan,',	'Shri Sumiten Chhana Patel ,Shri Chetankumar Mohanbhai Pavara,',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(23,	8,	'40,',	'2,',	'20,',	'-',	'Shri Patel Bhavish Raviabhai ,',	'Shri Bharat Patel,',	'Shri Tushar Laxman,',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(24,	9,	'28,',	'59,',	'16,',	'-',	'Shri Pande Shivam Sabhajit,',	'Kum Mangela Sanjanaben Ramkumar,',	'Shri Baraiya Pravinkumar Ramji,',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(25,	10,	'61,',	'63,',	'36,',	'-',	'Shri Jaykumar K. Dhodi,',	'Shri Nimesh S. Ahir,',	'Shri Manish Kumar Yadav,',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(26,	11,	'8,',	'51,',	'-',	'-',	'Shri Vijay M. Jadav,',	'Shri Bhandari Darpan Rajesh ,',	'-',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(27,	12,	'17,',	'4,',	'-',	'-',	'Shri Pinalkumar G. Patel,',	'Shri Takhat Rathod,',	'-',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(28,	13,	'31,',	'-',	'-',	'-',	'Shri Singh Swaraj Govind ,',	'-',	'-',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(29,	14,	'23,',	'25,',	'-',	'-',	'Shri Rohit Ramsiddh Singh,',	'Shri Patel Dharmesh Govind ,',	'-',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(30,	15,	'54,',	'18,',	'-',	'-',	'Shri Kotiya Axaykumar Karsan ,',	'Shri Bhandari Jignesh Hiralal,',	'-',	'-',	'2022-03-01',	'2022-03-31',	1,	2,	'2022-02-28 17:24:45',	0),
(31,	1,	'48,56,',	'40,49,',	'42,21,',	'17',	'Shri Sumiten Chhana Patel ,Shri Patel Harishtey Naginbhai ,',	'Shri Patel Bhavish Raviabhai ,Shri Dhodiya Sauravkumar Kalidas,',	'Shri Halpati Bhupendrakumar Mukeshbhai ,Shri Sunil Pravin Dhodia,',	'Shri Pinalkumar G. Patel',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(32,	2,	'20,15,',	'12,23,',	'7,18,',	'57',	'Shri Tushar Laxman,Shri Sikotariya Prakash Jiva,',	'Shri Kamlesh Dhodi,Shri Rohit Ramsiddh Singh,',	'Shri Mahesh Natu,Shri Bhandari Jignesh Hiralal,',	'Shri Tiwari Amitkumar Shivkumar',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(33,	3,	'29,',	'16,',	'35,47,',	'-',	'Shri Ronit Kamleshbhai Patel ,',	'Shri Baraiya Pravinkumar Ramji,',	'Shri Solanki Sanjaykumar Varjang ,Shri Mayank Kumar Jasvant Bhai Patel ,',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(34,	4,	'63,22,',	'1,3,',	'50,26,',	'-',	'Shri Nimesh S. Ahir,Shri Chetankumar Mohanbhai Pavara,',	'Shri Kantilal H. Solanki,Shri Rajendra Solanki,',	'Shri Patel Shrinath Khalpabhai ,Shri Patel Ronalkumar Naginbhai ,',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(35,	5,	'52,9,',	'27,55,',	'51,4,',	'-',	'Shri Bamaniya Sanjaykumar Ramji,Shri Premji S Solanki,',	'Shri Patel Kamlesh Natu ,Shri Patel Yatinkumar Shantilal ,',	'Shri Bhandari Darpan Rajesh ,Shri Takhat Rathod,',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(36,	6,	'10,',	'13,',	'31,61,',	'-',	'Shri Baudas Bica,',	'Shri Pratap S. Vaja,',	'Shri Singh Swaraj Govind ,Shri Jaykumar K. Dhodi,',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(37,	7,	'59,32,',	'8,28,',	'33,34,',	'-',	'Kum Mangela Sanjanaben Ramkumar,Shri Solanki Rahul Dinesh ,',	'Shri Vijay M. Jadav,Shri Pande Shivam Sabhajit,',	'Shri Vijay Pal ,Shri Ramanuj Vedangkumar Dinkarbhai ,',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(38,	8,	'36,',	'24,',	'39,',	'-',	'Shri Manish Kumar Yadav,',	'Shri Patel Krinal Pramod ,',	'Shri Patel Nitin Raman,',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(39,	9,	'38,',	'11,',	'54,',	'-',	'Shri Patel Sagarkumar Ratilal ,',	'Shri Pandu Chaudhary,',	'Shri Kotiya Axaykumar Karsan ,',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(40,	10,	'45,',	'58,',	'25,',	'-',	'Shri Patel Akshay Naresh,',	'Shri Bhavesh Jaydeep Apandkar,',	'Shri Patel Dharmesh Govind ,',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(41,	11,	'37,',	'5,',	'-',	'-',	'Shri Ravi Kumar Singh,',	'Shri Ganesh Kurkutia,',	'-',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(42,	12,	'53,',	'46,',	'-',	'-',	'Shri Patel Hemant Dhirubhai ,',	'Shri Patel Rahul Hasmukh,',	'-',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(43,	13,	'62,',	'-',	'-',	'-',	'Shri Jayesh K. Bij,',	'-',	'-',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(44,	14,	'30,',	'41,',	'-',	'-',	'Shri Tamta Surajsingh Surendrasingh ,',	'Shri Patel Jaysukhbhai Dhirubhai ,',	'-',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(45,	15,	'44,',	'43,',	'-',	'-',	'Shri Patel Yagneshkumar Surendra,',	'Shri Halpati Divyesh Radakabhai ,',	'-',	'-',	'2022-04-01',	'2022-04-30',	1,	2,	'2022-03-29 17:12:30',	0),
(46,	1,	'50,35,',	'31,52,',	'24,37,',	'32',	'Shri Patel Shrinath Khalpabhai ,Shri Solanki Sanjaykumar Varjang ,',	'Shri Singh Swaraj Govind ,Shri Bamaniya Sanjaykumar Ramji,',	'Shri Patel Krinal Pramod ,Shri Ravi Kumar Singh,',	'Shri Solanki Rahul Dinesh ',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(47,	2,	'13,55,',	'22,9,',	'10,16,',	'51',	'Shri Pratap S. Vaja,Shri Patel Yatinkumar Shantilal ,',	'Shri Chetankumar Mohanbhai Pavara,Shri Premji S Solanki,',	'Shri Baudas Bica,Shri Baraiya Pravinkumar Ramji,',	'Shri Bhandari Darpan Rajesh ',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(48,	3,	'12,',	'20,',	'43,36,',	'-',	'Shri Kamlesh Dhodi,',	'Shri Tushar Laxman,',	'Shri Halpati Divyesh Radakabhai ,Shri Manish Kumar Yadav,',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(49,	4,	'40,46,',	'25,39,',	'5,11,',	'-',	'Shri Patel Bhavish Raviabhai ,Shri Patel Rahul Hasmukh,',	'Shri Patel Dharmesh Govind ,Shri Patel Nitin Raman,',	'Shri Ganesh Kurkutia,Shri Pandu Chaudhary,',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(50,	5,	'61,49,',	'33,30,',	'3,29,',	'-',	'Shri Jaykumar K. Dhodi,Shri Dhodiya Sauravkumar Kalidas,',	'Shri Vijay Pal ,Shri Tamta Surajsingh Surendrasingh ,',	'Shri Rajendra Solanki,Shri Ronit Kamleshbhai Patel ,',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(51,	6,	'27,',	'7,',	'23,53,',	'-',	'Shri Patel Kamlesh Natu ,',	'Shri Mahesh Natu,',	'Shri Rohit Ramsiddh Singh,Shri Patel Hemant Dhirubhai ,',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(52,	7,	'17,',	'42,54,',	'44,38,',	'-',	'Shri Pinalkumar G. Patel,',	'Shri Halpati Bhupendrakumar Mukeshbhai ,Shri Kotiya Axaykumar Karsan ,',	'Shri Patel Yagneshkumar Surendra,Shri Patel Sagarkumar Ratilal ,',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(53,	8,	'21,',	'63,',	'56,',	'-',	'Shri Sunil Pravin Dhodia,',	'Shri Nimesh S. Ahir,',	'Shri Patel Harishtey Naginbhai ,',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(54,	9,	'60,',	'62,',	'57,',	'-',	'Shri Rajbhar  Amit Rambachan,',	'Shri Jayesh K. Bij,',	'Shri Tiwari Amitkumar Shivkumar,',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(55,	10,	'26,',	'15,',	'28,',	'-',	'Shri Patel Ronalkumar Naginbhai ,',	'Shri Sikotariya Prakash Jiva,',	'Shri Pande Shivam Sabhajit,',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(56,	11,	'58,',	'48,',	'-',	'-',	'Shri Bhavesh Jaydeep Apandkar,',	'Shri Sumiten Chhana Patel ,',	'-',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(57,	12,	'4,',	'45,',	'-',	'-',	'Shri Takhat Rathod,',	'Shri Patel Akshay Naresh,',	'-',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(58,	13,	'34,',	'-',	'-',	'-',	'Shri Ramanuj Vedangkumar Dinkarbhai ,',	'-',	'-',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(59,	14,	'19,',	'47,',	'-',	'-',	'Shri Ankitkumar K. Patel,',	'Shri Mayank Kumar Jasvant Bhai Patel ,',	'-',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(60,	15,	'8,',	'59,',	'-',	'-',	'Shri Vijay M. Jadav,',	'Kum Mangela Sanjanaben Ramkumar,',	'-',	'-',	'2022-05-01',	'2022-05-31',	1,	2,	'2022-04-27 10:27:44',	0),
(61,	1,	'5,51,',	'10,8,',	'55,40,',	'4',	'Shri Ganesh Kurkutia,Shri Bhandari Darpan Rajesh ,',	'Shri Baudas Bica,Shri Vijay M. Jadav,',	'Shri Patel Yatinkumar Shantilal ,Shri Patel Bhavish Raviabhai ,',	'Shri Takhat Rathod',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(62,	2,	'25,',	'58,',	'42,49,',	'-',	'Shri Patel Dharmesh Govind ,',	'Shri Bhavesh Jaydeep Apandkar,',	'Shri Halpati Bhupendrakumar Mukeshbhai ,Shri Dhodiya Sauravkumar Kalidas,',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(63,	3,	'39,',	'37,',	'34,9,',	'-',	'Shri Patel Nitin Raman,',	'Shri Ravi Kumar Singh,',	'Shri Ramanuj Vedangkumar Dinkarbhai ,Shri Premji S Solanki,',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(64,	4,	'48,',	'12,',	'13,62,',	'-',	'Shri Sumiten Chhana Patel ,',	'Shri Kamlesh Dhodi,',	'Shri Pratap S. Vaja,Shri Jayesh K. Bij,',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(65,	5,	'44,',	'17,',	'60,63,',	'-',	'Shri Patel Yagneshkumar Surendra,',	'Shri Pinalkumar G. Patel,',	'Shri Rajbhar  Amit Rambachan,Shri Nimesh S. Ahir,',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(66,	6,	'59,',	'32,',	'45,47,',	'-',	'Kum Mangela Sanjanaben Ramkumar,',	'Shri Solanki Rahul Dinesh ,',	'Shri Patel Akshay Naresh,Shri Mayank Kumar Jasvant Bhai Patel ,',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(67,	7,	'22,',	'19,',	'33,15,',	'-',	'Shri Chetankumar Mohanbhai Pavara,',	'Shri Ankitkumar K. Patel,',	'Shri Vijay Pal ,Shri Sikotariya Prakash Jiva,',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(68,	8,	'38,',	'35,',	'27,',	'-',	'Shri Patel Sagarkumar Ratilal ,',	'Shri Solanki Sanjaykumar Varjang ,',	'Shri Patel Kamlesh Natu ,',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(69,	9,	'20,',	'23,',	'26,',	'-',	'Shri Tushar Laxman,',	'Shri Rohit Ramsiddh Singh,',	'Shri Patel Ronalkumar Naginbhai ,',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(70,	10,	'7,',	'46,',	'-',	'-',	'Shri Mahesh Natu,',	'Shri Patel Rahul Hasmukh,',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(71,	11,	'31,',	'29,',	'-',	'-',	'Shri Singh Swaraj Govind ,',	'Shri Ronit Kamleshbhai Patel ,',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(72,	12,	'43,',	'56,',	'-',	'-',	'Shri Halpati Divyesh Radakabhai ,',	'Shri Patel Harishtey Naginbhai ,',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(73,	13,	'3,',	'-',	'-',	'-',	'Shri Rajendra Solanki,',	'-',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(74,	14,	'30,',	'21,',	'-',	'-',	'Shri Tamta Surajsingh Surendrasingh ,',	'Shri Sunil Pravin Dhodia,',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(75,	15,	'16,',	'24,',	'-',	'-',	'Shri Baraiya Pravinkumar Ramji,',	'Shri Patel Krinal Pramod ,',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(76,	16,	'52,',	'-',	'-',	'-',	'Shri Bamaniya Sanjaykumar Ramji,',	'-',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(77,	17,	'54,',	'-',	'-',	'-',	'Shri Kotiya Axaykumar Karsan ,',	'-',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(78,	18,	'28,',	'-',	'-',	'-',	'Shri Pande Shivam Sabhajit,',	'-',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(79,	19,	'53,',	'-',	'-',	'-',	'Shri Patel Hemant Dhirubhai ,',	'-',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(80,	20,	'11,',	'-',	'-',	'-',	'Shri Pandu Chaudhary,',	'-',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(81,	21,	'41,',	'57,',	'-',	'-',	'Shri Patel Jaysukhbhai Dhirubhai ,',	'Shri Tiwari Amitkumar Shivkumar,',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(82,	22,	'36,',	'50,',	'-',	'-',	'Shri Manish Kumar Yadav,',	'Shri Patel Shrinath Khalpabhai ,',	'-',	'-',	'2022-06-01',	'2022-06-30',	1,	2,	'2022-05-28 12:08:27',	0),
(83,	1,	'5,51,',	'10,8,',	'55,40,',	'4',	'Shri Ganesh Kurkutia,Shri Bhandari Darpan Rajesh ,',	'Shri Baudas Bica,Shri Vijay M. Jadav,',	'Shri Patel Yatinkumar Shantilal ,Shri Patel Bhavish Raviabhai ,',	'Shri Takhat Rathod',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(84,	2,	'25,',	'58,',	'42,49,',	'-',	'Shri Patel Dharmesh Govind ,',	'Shri Bhavesh Jaydeep Apandkar,',	'Shri Halpati Bhupendrakumar Mukeshbhai ,Shri Dhodiya Sauravkumar Kalidas,',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(85,	3,	'39,',	'37,',	'34,9,',	'-',	'Shri Patel Nitin Raman,',	'Shri Ravi Kumar Singh,',	'Shri Ramanuj Vedangkumar Dinkarbhai ,Shri Premji S Solanki,',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(86,	4,	'48,',	'12,',	'13,62,',	'-',	'Shri Sumiten Chhana Patel ,',	'Shri Kamlesh Dhodi,',	'Shri Pratap S. Vaja,Shri Jayesh K. Bij,',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(87,	5,	'44,',	'17,',	'60,63,',	'-',	'Shri Patel Yagneshkumar Surendra,',	'Shri Pinalkumar G. Patel,',	'Shri Rajbhar  Amit Rambachan,Shri Nimesh S. Ahir,',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(88,	6,	'59,',	'32,',	'45,47,',	'-',	'Kum Mangela Sanjanaben Ramkumar,',	'Shri Solanki Rahul Dinesh ,',	'Shri Patel Akshay Naresh,Shri Mayank Kumar Jasvant Bhai Patel ,',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(89,	7,	'22,',	'19,',	'33,15,',	'-',	'Shri Chetankumar Mohanbhai Pavara,',	'Shri Ankitkumar K. Patel,',	'Shri Vijay Pal ,Shri Sikotariya Prakash Jiva,',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(90,	8,	'38,',	'35,',	'27,',	'-',	'Shri Patel Sagarkumar Ratilal ,',	'Shri Solanki Sanjaykumar Varjang ,',	'Shri Patel Kamlesh Natu ,',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(91,	9,	'20,',	'23,',	'26,',	'-',	'Shri Tushar Laxman,',	'Shri Rohit Ramsiddh Singh,',	'Shri Patel Ronalkumar Naginbhai ,',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(92,	10,	'7,',	'46,',	'-',	'-',	'Shri Mahesh Natu,',	'Shri Patel Rahul Hasmukh,',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(93,	11,	'31,',	'29,',	'-',	'-',	'Shri Singh Swaraj Govind ,',	'Shri Ronit Kamleshbhai Patel ,',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(94,	12,	'43,',	'56,',	'-',	'-',	'Shri Halpati Divyesh Radakabhai ,',	'Shri Patel Harishtey Naginbhai ,',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(95,	13,	'3,',	'-',	'-',	'-',	'Shri Rajendra Solanki,',	'-',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(96,	14,	'31,',	'21,',	'-',	'-',	'Shri Tamta Surajsingh Surendrasingh ,',	'Shri Sunil Pravin Dhodia,',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(97,	15,	'16,',	'24,',	'-',	'-',	'Shri Baraiya Pravinkumar Ramji,',	'Shri Patel Krinal Pramod ,',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(98,	16,	'52,',	'-',	'-',	'-',	'Shri Bamaniya Sanjaykumar Ramji,',	'-',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(99,	17,	'54,',	'-',	'-',	'-',	'Shri Kotiya Axaykumar Karsan ,',	'-',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(100,	18,	'28,',	'-',	'-',	'-',	'Shri Pande Shivam Sabhajit,',	'-',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(101,	19,	'53,',	'-',	'-',	'-',	'Shri Patel Hemant Dhirubhai ,',	'-',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(102,	20,	'11,',	'-',	'-',	'-',	'Shri Pandu Chaudhary,',	'-',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(103,	21,	'41,',	'57,',	'-',	'-',	'Shri Patel Jaysukhbhai Dhirubhai ,',	'Shri Tiwari Amitkumar Shivkumar,',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0),
(104,	22,	'36,',	'50,',	'-',	'-',	'Shri Manish Kumar Yadav,',	'Shri Patel Shrinath Khalpabhai ,',	'-',	'-',	'2022-07-01',	'2022-07-31',	1,	2,	'2022-05-28 12:08:27',	0);

DROP TABLE IF EXISTS `logs_change_pin`;
CREATE TABLE `logs_change_pin` (
  `logs_change_pin_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `old_pin` text NOT NULL,
  `new_pin` text NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`logs_change_pin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `logs_email`;
CREATE TABLE `logs_email` (
  `email_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `email_type` tinyint(1) NOT NULL,
  `module_type` tinyint(4) NOT NULL,
  `module_id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`email_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `logs_login_details`;
CREATE TABLE `logs_login_details` (
  `logs_login_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `login_timestamp` int(11) NOT NULL,
  `logout_timestamp` int(11) NOT NULL,
  `logs_data` text NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` datetime NOT NULL,
  PRIMARY KEY (`logs_login_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `logs_sms`;
CREATE TABLE `logs_sms` (
  `logs_sms_id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_type` tinyint(1) NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `ErrorCode` tinyint(4) NOT NULL,
  `ErrorMessage` text NOT NULL,
  `JobId` varchar(100) NOT NULL,
  `MessageData` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`logs_sms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `otp`;
CREATE TABLE `otp` (
  `otp_id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_number` varchar(10) NOT NULL,
  `otp` varchar(10) NOT NULL,
  `otp_type` tinyint(1) NOT NULL,
  `created_time` datetime NOT NULL,
  `is_expired` tinyint(1) NOT NULL,
  PRIMARY KEY (`otp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sa_logs_change_password`;
CREATE TABLE `sa_logs_change_password` (
  `sa_logs_change_password_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sa_user_id` bigint(20) NOT NULL,
  `old_password` text NOT NULL,
  `new_password` text NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`sa_logs_change_password_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sa_logs_change_password` (`sa_logs_change_password_id`, `sa_user_id`, `old_password`, `new_password`, `created_time`) VALUES
(1,	2,	'457f734d2010c82726cd137993565c396231343161613631666435336330383135643934323331326332383164626237313731623539356565316533343564623265346261363864613934323365386126fc3088ec664a28ed7d',	'cc3b2a512b2d25cd69bee3888d85d2f0623635663030616339613033623638323434626364373235663366643064633962613633306330343136666239326335396466336635396532646435386431393d4d22f057298ba962d343',	'2022-03-03 15:22:40');

DROP TABLE IF EXISTS `sa_logs_login_details`;
CREATE TABLE `sa_logs_login_details` (
  `sa_logs_login_details_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sa_user_id` bigint(20) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `login_timestamp` int(11) NOT NULL,
  `logout_timestamp` int(11) NOT NULL,
  `logs_data` text NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` datetime NOT NULL,
  PRIMARY KEY (`sa_logs_login_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sa_logs_login_details` (`sa_logs_login_details_id`, `sa_user_id`, `ip_address`, `login_timestamp`, `logout_timestamp`, `logs_data`, `created_time`, `updated_time`) VALUES
(1,	1,	'150.107.241.220',	1645508687,	1645510701,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"150.107.241.220\"}',	'2022-02-22 11:14:47',	'2022-02-22 11:48:21'),
(2,	1,	'157.32.253.164',	1645508707,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.82 Safari\\/537.36\",\"REMOTE_ADDR\":\"157.32.253.164\"}',	'2022-02-22 11:15:07',	'0000-00-00 00:00:00'),
(3,	1,	'164.100.212.187',	1645680044,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"164.100.212.187\"}',	'2022-02-24 10:50:44',	'0000-00-00 00:00:00'),
(4,	1,	'164.100.212.187',	1645688454,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"164.100.212.187\"}',	'2022-02-24 13:10:54',	'0000-00-00 00:00:00'),
(5,	2,	'164.100.212.187',	1645689571,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"164.100.212.187\"}',	'2022-02-24 13:29:31',	'0000-00-00 00:00:00'),
(6,	1,	'150.107.241.220',	1645689971,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"150.107.241.220\"}',	'2022-02-24 13:36:11',	'0000-00-00 00:00:00'),
(7,	2,	'118.185.36.82',	1645691885,	1645692028,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36 Edg\\/98.0.1108.56\",\"REMOTE_ADDR\":\"118.185.36.82\"}',	'2022-02-24 14:08:05',	'2022-02-24 14:10:28'),
(8,	2,	'118.185.36.82',	1645694704,	1645696486,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36 Edg\\/98.0.1108.56\",\"REMOTE_ADDR\":\"118.185.36.82\"}',	'2022-02-24 14:55:04',	'2022-02-24 15:24:46'),
(9,	2,	'59.95.34.41',	1645699582,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36 Edg\\/98.0.1108.56\",\"REMOTE_ADDR\":\"59.95.34.41\"}',	'2022-02-24 16:16:22',	'0000-00-00 00:00:00'),
(10,	2,	'157.32.100.53',	1645772338,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (iPhone; CPU iPhone OS 15_3 like Mac OS X) AppleWebKit\\/605.1.15 (KHTML, like Gecko) Version\\/15.3 Mobile\\/15E148 Safari\\/604.1\",\"REMOTE_ADDR\":\"157.32.100.53\"}',	'2022-02-25 12:28:58',	'0000-00-00 00:00:00'),
(11,	2,	'118.185.36.82',	1645775445,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36 Edg\\/98.0.1108.56\",\"REMOTE_ADDR\":\"118.185.36.82\"}',	'2022-02-25 13:20:45',	'0000-00-00 00:00:00'),
(12,	2,	'157.32.100.53',	1645801961,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (iPhone; CPU iPhone OS 15_3 like Mac OS X) AppleWebKit\\/605.1.15 (KHTML, like Gecko) Version\\/15.3 Mobile\\/15E148 Safari\\/604.1\",\"REMOTE_ADDR\":\"157.32.100.53\"}',	'2022-02-25 20:42:41',	'0000-00-00 00:00:00'),
(13,	2,	'61.0.170.146',	1646029286,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36 Edg\\/98.0.1108.62\",\"REMOTE_ADDR\":\"61.0.170.146\"}',	'2022-02-28 11:51:26',	'0000-00-00 00:00:00'),
(14,	1,	'164.100.212.187',	1646031701,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"164.100.212.187\"}',	'2022-02-28 12:31:41',	'0000-00-00 00:00:00'),
(15,	1,	'103.251.59.47',	1646031711,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"103.251.59.47\"}',	'2022-02-28 12:31:51',	'0000-00-00 00:00:00'),
(16,	1,	'::1',	1646036253,	1646036402,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-02-28 13:47:33',	'2022-02-28 13:50:02'),
(17,	2,	'::1',	1646036412,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-02-28 13:50:12',	'0000-00-00 00:00:00'),
(18,	2,	'::1',	1646036979,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-02-28 13:59:39',	'0000-00-00 00:00:00'),
(19,	2,	'::1',	1646301091,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-03 15:21:31',	'0000-00-00 00:00:00'),
(20,	2,	'::1',	1646370582,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-04 10:39:42',	'0000-00-00 00:00:00'),
(21,	2,	'::1',	1646389254,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-04 15:50:54',	'0000-00-00 00:00:00'),
(22,	2,	'::1',	1646455838,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/98.0.4758.102 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-05 10:20:38',	'0000-00-00 00:00:00'),
(23,	2,	'::1',	1646736259,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.51 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-08 16:14:19',	'0000-00-00 00:00:00'),
(24,	2,	'::1',	1646812343,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.51 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-09 13:22:23',	'0000-00-00 00:00:00'),
(25,	2,	'::1',	1646906350,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.51 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-10 15:29:10',	'0000-00-00 00:00:00'),
(26,	2,	'::1',	1648104252,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.82 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-24 12:14:12',	'0000-00-00 00:00:00'),
(27,	2,	'::1',	1648187713,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.82 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-25 11:25:13',	'0000-00-00 00:00:00'),
(28,	2,	'::1',	1648206411,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.82 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-25 16:36:51',	'0000-00-00 00:00:00'),
(29,	2,	'::1',	1648449441,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.84 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-28 12:07:21',	'0000-00-00 00:00:00'),
(30,	2,	'::1',	1648463651,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.84 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-28 16:04:11',	'0000-00-00 00:00:00'),
(31,	2,	'::1',	1648529151,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.84 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-29 10:15:51',	'0000-00-00 00:00:00'),
(32,	2,	'::1',	1648547279,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.84 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-29 15:17:59',	'0000-00-00 00:00:00'),
(33,	2,	'::1',	1648701232,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/99.0.4844.84 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-03-31 10:03:52',	'0000-00-00 00:00:00'),
(34,	2,	'::1',	1650689513,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/100.0.4896.127 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-04-23 10:21:53',	'0000-00-00 00:00:00'),
(35,	2,	'::1',	1650891235,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/100.0.4896.127 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-04-25 18:23:55',	'0000-00-00 00:00:00'),
(36,	2,	'::1',	1650950830,	1650951443,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/100.0.4896.127 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-04-26 10:57:10',	'2022-04-26 11:07:23'),
(37,	2,	'::1',	1650970542,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/100.0.4896.127 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-04-26 16:25:42',	'0000-00-00 00:00:00'),
(38,	2,	'::1',	1651035219,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/100.0.4896.127 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-04-27 10:23:39',	'0000-00-00 00:00:00'),
(39,	2,	'::1',	1653304672,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/101.0.4951.67 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-05-23 16:47:52',	'0000-00-00 00:00:00'),
(40,	2,	'::1',	1653634749,	1653635872,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/101.0.4951.67 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-05-27 12:29:09',	'2022-05-27 12:47:52'),
(41,	2,	'::1',	1653640217,	1653653219,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/101.0.4951.67 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-05-27 14:00:17',	'2022-05-27 17:36:59'),
(42,	2,	'::1',	1653718755,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/101.0.4951.67 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-05-28 11:49:15',	'0000-00-00 00:00:00'),
(43,	2,	'::1',	1653732946,	0,	'{\"HTTP_USER_AGENT\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/101.0.4951.67 Safari\\/537.36\",\"REMOTE_ADDR\":\"::1\"}',	'2022-05-28 15:45:46',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `sa_users`;
CREATE TABLE `sa_users` (
  `sa_user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_type` tinyint(1) NOT NULL,
  `district` tinyint(1) NOT NULL,
  `is_deactive` tinyint(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`sa_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sa_users` (`sa_user_id`, `name`, `username`, `password`, `user_type`, `district`, `is_deactive`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1,	'Admin',	'admin',	'f6e1823488b1bd4e72127c4b3005cf0862396666366436393539326636656236353437636264623065383461323334653363316339653864303632666264613634333965346439623931383036366334400b37037b94f4452462',	1,	0,	0,	1,	'2020-03-25 17:20:00',	1,	'2020-10-20 20:08:47',	0),
(2,	'EXCISE DEPARTMENT DAMAN',	'excise.daman',	'cc3b2a512b2d25cd69bee3888d85d2f0623635663030616339613033623638323434626364373235663366643064633962613633306330343136666239326335396466336635396532646435386431393d4d22f057298ba962d343',	2,	1,	0,	1,	'2020-08-28 13:44:06',	2,	'2022-03-03 15:22:40',	0);

DROP TABLE IF EXISTS `sa_user_type`;
CREATE TABLE `sa_user_type` (
  `sa_user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`sa_user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sa_user_type` (`sa_user_type_id`, `type`, `created_by`, `created_time`, `updated_by`, `updated_time`, `is_delete`) VALUES
(1,	'Admin',	1,	'2020-10-16 10:41:00',	1,	'2020-10-16 10:41:00',	0),
(2,	'Department',	1,	'2020-10-16 10:41:00',	0,	'0000-00-00 00:00:00',	0);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(1) NOT NULL,
  `applicant_name` varchar(100) NOT NULL,
  `applicant_address` varchar(200) NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `pin` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `is_verify_mobile` tinyint(1) NOT NULL,
  `verify_mobile_datetime` datetime NOT NULL,
  `is_verify_email` tinyint(1) NOT NULL,
  `verify_email_datetime` datetime NOT NULL,
  `temp_access_token` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` datetime NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2022-07-21 06:33:18