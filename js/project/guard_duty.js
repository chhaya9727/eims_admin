var guardDutyListTemplate = Handlebars.compile($('#guard_duty_list_template').html());
var guardDutyTableTemplate = Handlebars.compile($('#guard_duty_table_template').html());
var GuardDuty = {
    run: function () {
        this.router = new this.Router();
        this.listview = new this.listView();
    }
};
GuardDuty.Router = Backbone.Router.extend({
    routes: {
        'guardduty': 'renderList',
        'guardduty_form': 'renderListForForm',
        'edit_guardduty_form': 'renderList',
        'view_guardduty_form': 'renderList',
    },
    renderList: function () {
        GuardDuty.listview.listPage();
    },
    renderListForForm: function () {
        GuardDuty.listview.listPageGuardDutyForm();
    }
});
GuardDuty.listView = Backbone.View.extend({
    el: 'div#main_container',
    listPage: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }

        activeLink('menu_guard_duty');
        addClass('menu_guardduty', 'active');
        GuardDuty.router.navigate('guardduty');
        var templateData = {};
        this.$el.html(guardDutyListTemplate(templateData));
        this.loadGuardDutyData();
    },
    listPageGuardDutyForm: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        activeLink('menu_guard_duty');
        this.$el.html(guardDutyListTemplate);
    },
    actionRenderer: function (rowData) {
        if ((tempTypeInSession == TEMP_TYPE_A || tempTypeInSession == TEMP_TYPE_EXCISE_DEPT_USER) && rowData.status != VALUE_TWO) {
            rowData.show_edit_btn = true;
        }
        if (rowData.status != VALUE_ZERO && rowData.status != VALUE_ONE) {
            rowData.show_form_one_btn = true;
        }
        return guardDutyActionTemplate(rowData);
    },
    loadGuardDutyData: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }

        var templateData = {};
        showTableContainer('guard_duty');
        GuardDuty.router.navigate('guardduty');
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var date = new Date();
        var prev_month_from = dateTo_DD_MM_YYYY(new Date(date.getFullYear() - (date.getMonth() > 0 ? 0 : 1), (date.getMonth() - 1 + 12) % 12, 1));
        var prev_month_to = dateTo_DD_MM_YYYY(new Date(date.getFullYear(), date.getMonth(), 0));
        var curr_month_from = dateTo_DD_MM_YYYY(new Date(date.getFullYear(), date.getMonth(), 1));
        var curr_month_to = dateTo_DD_MM_YYYY(new Date(date.getFullYear(), date.getMonth() + 1, 0));
        var temp_next_month_from = new Date(date.getFullYear(), date.getMonth() + 1, 1);
        var next_month_from = dateTo_DD_MM_YYYY(new Date(date.getFullYear(), date.getMonth() + 1, 1));
        var next_month_to = dateTo_DD_MM_YYYY(new Date(temp_next_month_from.getFullYear(), temp_next_month_from.getMonth() + 1, 0));
        templateData.prev_month_from = prev_month_from;
        templateData.prev_month_to = prev_month_to;
        templateData.curr_month_from = curr_month_from;
        templateData.curr_month_to = curr_month_to;
        templateData.next_month_from = next_month_from;
        templateData.next_month_to = next_month_to;
        templateData.prevMonthName = months[date.getMonth() - 1];
        templateData.currMonthName = months[date.getMonth()];
        templateData.nextMonthName = months[date.getMonth() + 1];
        templateData.VALUE_ONE = VALUE_ONE;
        templateData.VALUE_TWO = VALUE_TWO;
        templateData.VALUE_THREE = VALUE_THREE;
        templateData.prev_month_frizee_data_status = 'display : none';
        templateData.curr_month_frizee_data_status = 'display : none';
        templateData.next_month_frizee_data_status = 'display : none';
        templateData.prev_month_report_data_status = 'display : none';
        templateData.curr_month_report_data_status = 'display : none';
        templateData.next_month_report_data_status = 'display : none';
        var prev_month_status;
        var curr_month_status;
        var next_month_status;
        var btn_status;
        $.post("guard_duty/get_all_frizee_data_status", {'prev_month_from': templateData.prev_month_from, 'prev_month_to': templateData.prev_month_to, 'curr_month_from': templateData.curr_month_from, 'curr_month_to': templateData.curr_month_to, 'next_month_from': templateData.next_month_from, 'next_month_to': templateData.next_month_to})
                .done(function (data) {
                    var parseData = JSON.parse(data);
                    var prev_month = parseData.frizee_data_status_prev_month.total_guards;
                    var curr_month = parseData.frizee_data_status_curr_month.total_guards;
                    var next_month = parseData.frizee_data_status_next_month.total_guards;
                    prev_month_status = parseData.frizee_data_status_prev_month.is_freeze;
                    curr_month_status = parseData.frizee_data_status_curr_month.is_freeze;
                    next_month_status = parseData.frizee_data_status_next_month.is_freeze;
                    btn_status_prev_month = parseData.btn_status_prev_month;
                    btn_status_curr_month = parseData.btn_status_curr_month;
                    btn_status_next_month = parseData.btn_status_next_month;
                    // if (prev_month > VALUE_ZERO) {
                    //     $(".cardOne").show();
                    //     $(".cardTwo").show();
                    //     $(".cardThree").hide();
                    // }
                    // if (curr_month > VALUE_ZERO) {
                    //     $(".cardOne").show();
                    //     $(".cardTwo").show();
                    //     $(".cardThree").show();
                    // }
                    //console.log(prev_month_status + curr_month_status + next_month_status + btn_status);
                    if (prev_month == VALUE_ZERO && curr_month == VALUE_ZERO && next_month == VALUE_ZERO) {
                        $(".cardOne").show();
                        $(".cardTwo").hide();
                        $(".cardThree").hide();
                    }
                    if (prev_month > VALUE_ZERO) {
                        $(".cardOne").show();
                        $(".cardTwo").show();
                        $(".cardThree").hide();
                    }
                    if (curr_month > VALUE_ZERO) {
                        $(".cardOne").show();
                        $(".cardTwo").show();
                        $(".cardThree").show();
                    }
                    //console.log(prev_month);
                    if (prev_month_status == VALUE_ONE && btn_status_prev_month == true) {
                        $('.r1').show();
                    } else if (btn_status_prev_month == true) {
                        $('.f1').show();
                    }
                    if (curr_month_status == VALUE_ONE && btn_status_curr_month == true) {
                        $('.r2').show();
                    } else if (btn_status_curr_month == true) {
                        $('.f2').show();
                    }
                    if (next_month_status == VALUE_ONE && btn_status_next_month == true) {
                        $('.r3').show();
                    } else if (btn_status_next_month == true) {
                        $('.f3').show();
                    }
                });
        //console.log(prev_month_status);

        $('#guard_duty_datatable_container').html(guardDutyTableTemplate(templateData));
    },
    guardDutyDataWithStatus: function (btnObj, monthFrom, monthTo, tblID) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }

        var firstShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.first_shift_guard_name != undefined && full.first_shift_guard_name != null) {
                if (full.first_shift_guard_name == VALUE_ZERO) {
                    full.first_shift_guard_name = '-';
                }
                var first_shift_guard_name = full.first_shift_guard_name.replace(/,\s*$/, "");
                return first_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        var secondShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.second_shift_guard_name != undefined && full.second_shift_guard_name != null) {
                if (full.second_shift_guard_name == VALUE_ZERO) {
                    full.second_shift_guard_name = '-';
                }
                var second_shift_guard_name = full.second_shift_guard_name.replace(/,\s*$/, "");
                return  second_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        var nightShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.night_shift_guard_name != undefined && full.night_shift_guard_name != null) {
                if (full.night_shift_guard_name == VALUE_ZERO) {
                    full.night_shift_guard_name = '-';
                }
                var night_shift_guard_name = full.night_shift_guard_name.replace(/,\s*$/, "");
                return night_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        var generalShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.general_shift_guard_name != undefined && full.general_shift_guard_name != null) {
                if (full.general_shift_guard_name == VALUE_ZERO) {
                    full.general_shift_guard_name = '-';
                }
                var general_shift_guard_name = full.general_shift_guard_name.replace(/,\s*$/, "");
                return general_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        if (tempTypeInSession == TEMP_TYPE_A || tempTypeInSession == TEMP_TYPE_EXCISE_DEPT_USER) {
            var tempStringDuty = '';
            var tempGuardName = '';
//            var office_one = 'Shri. Ankit Patel:<br/>1. Inward/ Outward of letters<br/>2. All permit related process<br/>3. Brand approval process<br/>Any other assigned work';
//            var office_two = 'Shri. Amit Rajbhar:<br/>1. Maintenance of Crime Register<br/>2. Process of cases<br/>3. Record of seized vehicles and Liquor<br/>Any other assigned work';
            guarddutyDataTable = $('#guard_duty_datatable_' + tblID).DataTable({
                ajax: {url: 'guard_duty/get_guard_duty_data', dataSrc: "guard_duty_data", type: "post", data: $.extend({}, {'month_from': monthFrom, 'month_to': monthTo}, getTokenData())},
                bAutoWidth: false,
                ordering: false,
                processing: true,
                paging: false,
                bInfo: false,
                bDestroy: true,
                language: {
                    "zeroRecords": "<span class='color-nic-red f-w-b'>You Required Maximum no. of Guards for Randomization Process.</span>"
                },
                serverSide: true,
                columns: [
                    {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
                    {data: 'checkpost_distillery_id', 'class': 'text-center', 'orderable': false, 'visible': false},
                    {data: 'first_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'second_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'night_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'reserve_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'general_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'name', 'class': 'v-a-m'},
                    {data: 'first_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': firstShiftGuardNameRenderer},
                    {data: 'second_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': secondShiftGuardNameRenderer},
                    {data: 'night_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': nightShiftGuardNameRenderer},
                    {data: 'general_shift_guard_name', 'class': 'text-center', 'orderable': false, 'visible': false, 'render': generalShiftGuardNameRenderer},
                ],
                createdRow: function (row, data, dataIndex) {
                    // If shift is "General Shift"
                    if (data['general_shift_guard_id'] != '' && data['general_shift_guard_id'] != '-' && data['general_shift_guard_id'] != 'undefined' && data['general_shift_guard_id'] != null) {
                        // Add COLSPAN attribute
                        $('td:eq(2)', row).attr('colspan', 3);
                        // Center horizontally
                        $('td:eq(2)', row).attr('align', 'center');
                        // Hide required number of columns
                        // next to the cell with COLSPAN attribute
                        $('td:eq(3)', row).css('display', 'none');
                        $('td:eq(4)', row).css('display', 'none');
                        // Update cell data
                        this.api().cell($('td:eq(2)', row)).data(data['general_shift_guard_name']);
                    }
                },
                "rowCallback": function (row, data) {
                    if (data.reserve_shift_guard_name != VALUE_ZERO && data.reserve_shift_guard_name != '-' && data.reserve_shift_guard_name != null) {
                        tempGuardName += data.reserve_shift_guard_name + '<br/>';
                    }
                    tempStringDuty = '<tr><td class="text-center">' + tempGuardName + '</td></tr>';
                },
                "initComplete": function (settings, json) {
                    if (tblID == VALUE_ONE) {
                        $('#previous_reserve_guard_list').html(tempStringDuty);
                    }
                    if (tblID == VALUE_TWO) {
                        $('#current_reserve_guard_list').html(tempStringDuty);
                    }
                    if (tblID == VALUE_THREE) {
                        $('#next_reserve_guard_list').html(tempStringDuty);
                    }

                    this.api().columns().every(function () {
                        var that = this;
                        $('input', this.header()).on('keyup change clear', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                        $('select', this.header()).on('change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                }
            });
            $('#guard_duty_datatable_' + tblID + '_filter').remove();
        }
    },
    frizeeGuardDutyData: function (btnObj, weekFrom, weekTo, tblId) {
        var checkpostId = [];
        var firstShiftGuardName = [];
        var secondShiftGuardName = [];
        var nightShiftGuardName = [];
        var generalShiftGuardName = [];
        var reserveShiftGuardName = [];
        var firstShiftGuardId = [];
        var secondShiftGuardId = [];
        var nightShiftGuardId = [];
        var generalShiftGuardId = [];
        var reserveShiftGuardId = [];
        var table = $('#guard_duty_datatable_' + tblId).DataTable();
        var data = table
                .rows()
                .data();
        var jsonStr = '[';
        i = 0;
        data.each(function () {
            checkpostId[i] = table.rows(i).data()[0]['checkpost_distillery_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            firstShiftGuardName[i] = table.rows(i).data()[0]['first_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            secondShiftGuardName[i] = table.rows(i).data()[0]['second_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            nightShiftGuardName[i] = table.rows(i).data()[0]['night_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            generalShiftGuardName[i] = table.rows(i).data()[0]['general_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            reserveShiftGuardName[i] = table.rows(i).data()[0]['reserve_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            firstShiftGuardId[i] = table.rows(i).data()[0]['first_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            secondShiftGuardId[i] = table.rows(i).data()[0]['second_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            nightShiftGuardId[i] = table.rows(i).data()[0]['night_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            generalShiftGuardId[i] = table.rows(i).data()[0]['general_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            reserveShiftGuardId[i] = table.rows(i).data()[0]['reserve_shift_guard_id'];
            i++;
        });
        for (i = 0; i < checkpostId.length; i++) {
            jsonStr += "{";
            jsonStr += '"checkpost_distillery_id":"' + checkpostId[i] + '" ,"first_shift_guard_name" : "' + firstShiftGuardName[i] + '" ,"second_shift_guard_name" : "' + secondShiftGuardName[i] + '" ,"night_shift_guard_name" : "' + nightShiftGuardName[i] + '","general_shift_guard_name" : "' + generalShiftGuardName[i] + '" ,"reserve_shift_guard_name" : "' + reserveShiftGuardName[i] + '" ,"week_from" : "' + weekFrom + '" ,"week_to" : "' + weekTo + '" ,"first_shift_guard_id" : "' + firstShiftGuardId[i] + '" ,"second_shift_guard_id" : "' + secondShiftGuardId[i] + '" ,"night_shift_guard_id" : "' + nightShiftGuardId[i] + '","general_shift_guard_id" : "' + generalShiftGuardId[i] + '"  ,"reserve_shift_guard_id" : "' + reserveShiftGuardId[i] + '"';
            jsonStr += "}";
            if (i < checkpostId.length - 1)
                jsonStr += ",";
        }
        jsonStr += ']';
        var guardData = JSON.parse(jsonStr);
        var that = this;
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        $.ajax({
            url: 'guard_duty/submit_guard_duty',
            type: 'post',
            data: $.extend({}, {'guardData': guardData}, getTokenData()),
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                validationMessageShow('guardduty', textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (data) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(data);
                setNewToken(parseData.temp_token);
                if (parseData.success == false) {
                    validationMessageShow('guardduty', parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                showSuccess(parseData.message);
                $('#frizee_btn_' + tblId).remove();
                $('#referesh_btn_' + tblId).remove();
                $('#report_btn_' + tblId).show();
                //GuardDuty.router.navigate('guardduty', {'trigger': true});
                that.loadGuardDutyData();
            }
        });
    },
    generateReport: function (btnObj, weekFrom, weekTo, tblID) {
        if (!weekFrom || !weekTo || !tblID) {
            showError(invalidAccessValidationMessage);
            return false;
        }
        $('#week_from_for_report').val(weekFrom);
        $('#week_to_for_report').val(weekTo);
        $('#guard_duty_report_pdf_form').submit();
    },
});