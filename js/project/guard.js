var guardListTemplate = Handlebars.compile($('#guard_list_template').html());
var guardTableTemplate = Handlebars.compile($('#guard_table_template').html());
var guardActionTemplate = Handlebars.compile($('#guard_action_template').html());
var guardFormTemplate = Handlebars.compile($('#guard_form_template').html());
var guardViewTemplate = Handlebars.compile($('#guard_view_template').html());
var guardApproveTemplate = Handlebars.compile($('#guard_approve_template').html());
var guardRejectTemplate = Handlebars.compile($('#guard_reject_template').html());

var tempPersonCnt = 1;

var GUARD = {
    run: function () {
        this.router = new this.Router();
        this.listview = new this.listView();
    }
};
GUARD.Router = Backbone.Router.extend({
    routes: {
        'guard': 'renderList',
        'guard_form': 'renderListForForm',
        'edit_guard_form': 'renderList',
        'view_guard_form': 'renderList',
    },
    renderList: function () {
        GUARD.listview.listPage();
    },
    renderListForForm: function () {
        GUARD.listview.listPageForm();
    }
});
GUARD.listView = Backbone.View.extend({
    el: 'div#main_container',
    listPage: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }

        activeLink('menu_gurad');
        addClass('menu_guard', 'active');
        GUARD.router.navigate('guard');
        var templateData = {};
        this.$el.html(guardListTemplate(templateData));
        this.loadData();

    },
    listPageForm: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        activeLink('menu_gurad');
        addClass('menu_guard', 'active');
        this.$el.html(guardListTemplate);
        this.newGUARDForm(false, {});
    },
    loadData: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        var actionRenderer = function (data, type, full, meta) {
            var templateData = {
                'guard_id': data,
            };
            if (full.is_active == IS_ACTIVE) {
                templateData.active_deactive_type = 'toggle-on';
                templateData.new_active_deactive_type = 'active';
                templateData.new_active_deactive_view_type = 'Deactived';
                templateData.active_deactive_class = 'danger';
                templateData.active_deactive_title = 'Deactive Account.';
            } else if (full.is_active == IS_DEACTIVE) {
                templateData.active_deactive_type = 'toggle-off';
                templateData.new_active_deactive_type = 'deactive';
                templateData.new_active_deactive_view_type = 'Actived';
                templateData.active_deactive_class = 'success';
                templateData.active_deactive_title = 'Activate Account.';
            }
            return guardActionTemplate(templateData);
        };
        var userStatusRenderer = function (data, type, full, meta) {
            if (data == IS_DEACTIVE) {
                return '<span class="badge bg-danger" id="active_deactive_container_' + full.guard_id + '">Deactive</span>';
            }
            return '<span class="badge bg-success" id="active_deactive_container_' + full.guard_id + '">Active</span>';
        };
        var tempRegNoRenderer = function (data, type, full, meta) {
            return regNoRenderer(VALUE_TWO, data);
        };
        var that = this;
        showTableContainer('guard');
        GUARD.router.navigate('guard');
        $('#guard_datatable_container').html(guardTableTemplate);
        allowOnlyIntegerValue('mobile_number_for_guard_list');
        if (tempTypeInSession == TEMP_TYPE_A || tempTypeInSession == TEMP_TYPE_EXCISE_DEPT_USER) {
            guardDataTable = $('#guard_datatable').DataTable({
                ajax: {url: 'guard/get_guard_data', dataSrc: "guard_data", type: "post"},
                bAutoWidth: false,
                ordering: false,
                processing: true,
                language: dataTableProcessingAndNoDataMsg,
                serverSide: true,
                columns: [
                    {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
                    {data: 'guard_id', 'class': 'v-a-m text-center f-w-b', 'render': tempRegNoRenderer},
                    {data: 'guard_name', 'class': 'v-a-m'},
                    {data: 'guard_mob', 'class': 'v-a-m'},
                    {data: 'is_active', 'class': 'text-center', 'render': userStatusRenderer, 'orderable': false},
                    {'class': 'text-center', 'orderable': false, 'data': 'guard_id', "render": actionRenderer},
                ],
                "initComplete": searchableDatatable
            });
        }
        $('#guard_datatable_filter').remove();
    },
    newGUARDForm: function (isEdit, parseData) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        var that = this;
        if (isEdit) {
            var formData = parseData.guard_data;
            GUARD.router.navigate('edit_guard_form');
        } else {
            var formData = {};
            GUARD.router.navigate('guard_form');
        }
        eeDist = [];
        var templateData = {};
        templateData.VALUE_ONE = VALUE_ONE;
        templateData.VALUE_TWO = VALUE_TWO;
        templateData.IS_CHECKED_YES = IS_CHECKED_YES;
        templateData.IS_CHECKED_NO = IS_CHECKED_NO;
        templateData.VIEW_UPLODED_DOCUMENT = VIEW_UPLODED_DOCUMENT;
        templateData.guard_data = parseData.guard_data;
        showFormContainer('guard');
        $('#guard_form_container').html(guardFormTemplate((templateData)));

        generateSelect2();
        datePicker();
        $('#guard_form').find('input').keypress(function (e) {
            if (e.which == 13) {
                that.submitGUARD($('#submit_btn_for_guard'));
            }
        });
    },
    editOrViewGUARD: function (btnObj, guardId, isEdit) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        if (!guardId) {
            showError(invalidUserValidationMessage);
            return;
        }
        tempVillageData = [];
        var that = this;
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        $.ajax({
            url: 'guard/get_guard_data_by_id',
            type: 'post',
            data: $.extend({}, {'guard_id': guardId}, getTokenData()),
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                showError(textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (response) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(response);
                setNewToken(parseData.temp_token);
                if (parseData.success === false) {
                    showError(parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                tempVillageData = parseData.village_data;
                if (isEdit) {
                    that.newGUARDForm(isEdit, parseData);
                } else {
                    that.viewGUARDForm(parseData);
                }
            }
        });
    },
    viewGUARDForm: function (parseData) {
        var that = this;
        var templateData = {};
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        eeDist = [];
        var formData = parseData.guard_data;
        formData.VIEW_UPLODED_DOCUMENT = VIEW_UPLODED_DOCUMENT;
        GUARD.router.navigate('view_guard_form');
        showFormContainer('guard');
        $('#guard_form_container').html(guardViewTemplate(formData));
        $('.form-control').prop('readonly', true);
        generateSelect2();
    },
    checkValidationFor: function (guardData) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }

        if (!guardData.guard_name) {
            return getBasicMessageAndFieldJSONArray('guard_name', nameValidationMessage);
        }
        if (!guardData.guard_designation) {
            return getBasicMessageAndFieldJSONArray('guard_designation', designationValidationMessage);
        }
        if (!guardData.guard_mob) {
            return getBasicMessageAndFieldJSONArray('guard_mob', mobileValidationMessage);
        }
        var mobileMessage = mobileNumberValidation(guardData.guard_mob);
        if (mobileMessage != '') {
            return getBasicMessageAndFieldJSONArray('guard_mob', invalidMobileValidationMessage);
        }
        if (!guardData.guard_email) {
            return getBasicMessageAndFieldJSONArray('guard_email', emailValidationMessage);
        }
        return '';
    },
    askForSubmitGUARD: function (moduleType) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        validationMessageHide();
        if (moduleType != VALUE_ONE && moduleType != VALUE_TWO) {
            showError(invalidAccessValidationMessage);
            return false;
        }
        var yesEvent = 'GUARD.listview.submitGUARD(\'' + moduleType + '\')';
        showConfirmation(yesEvent, 'Submit');
    },
    submitGUARD: function (moduleType) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        var that = this;
        validationMessageHide();
        var guardData = $('#guard_form').serializeFormJSON();
        var validationData = that.checkValidationFor(guardData);
        if (validationData != '') {
            $('#' + validationData.field).focus();
            validationMessageShow('guard-' + validationData.field, validationData.message);
            return false;
        }

        var btnObj = moduleType == VALUE_ONE ? $('#draft_btn_for_guard') : $('#submit_btn_for_guard');
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        var guardData = new FormData($('#guard_form')[0]);
        guardData.append("csrf_token_eims_admin", getTokenData()['csrf_token_eims_admin']);
        guardData.append("module_type", moduleType);
        $.ajax({
            type: 'POST',
            url: 'guard/submit_guard',
            data: guardData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                validationMessageShow('guard', textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (data) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(data);
                setNewToken(parseData.temp_token);
                if (parseData.success == false) {
                    validationMessageShow('guard', parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                showSuccess(parseData.message);
                GUARD.router.navigate('guard', {'trigger': true});
            }
        });
    },
    activeDeactiveUser: function (btnObj, guardId, activeDeactiveType) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        if (!guardId) {
            showError('Please select any Guard.');
            return false;
        }
        var that = this;
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        var onclick = 'User.listview.activeDeactiveUser($(this),"' + guardId + '","' + activeDeactiveType + '")';
        var template = '<label class="fa fa-' + activeDeactiveType + ' label-btn-icon"></label>';
        var ogType = activeDeactiveType == 'active' ? IS_DEACTIVE : IS_ACTIVE;
        var guardData = new FormData($('#guard_form')[0]);
        guardData.append("csrf_token_eims_admin", getTokenData()['csrf_token_eims_admin']);
        guardData.append("onclick", onclick);
        guardData.append("template", template);
        guardData.append("type", ogType);
        guardData.append("guard_id", guardId);
        $.ajax({
            url: 'guard/active_deactive_guard',
            type: 'POST',
            data: guardData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                validationMessageShow('guard', textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (data) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(data);
                setNewToken(parseData.temp_token);
                if (parseData.success == false) {
                    validationMessageShow('guard', parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                showSuccess(parseData.message);
                GUARD.router.navigate('guard', {'trigger': true});
                that.loadData();
            }
        });
    }
});
