var guardDutyListTemplate = Handlebars.compile($('#guard_duty_list_template').html());
var guardDutyTableTemplate = Handlebars.compile($('#guard_duty_table_template').html());
// var guardDutyActionTemplate = Handlebars.compile($('#guard_duty_action_template').html());
// var guardDutyFormTemplate = Handlebars.compile($('#guard_duty_form_template').html());
// var guardDutyViewTemplate = Handlebars.compile($('#guard_duty_view_template').html());


var GuardDuty = {
    run: function () {
        this.router = new this.Router();
        this.listview = new this.listView();
    }
};
GuardDuty.Router = Backbone.Router.extend({
    routes: {
        'guardduty': 'renderList',
        'guardduty_form': 'renderListForForm',
        'edit_guardduty_form': 'renderList',
        'view_guardduty_form': 'renderList',
    },
    renderList: function () {
        GuardDuty.listview.listPage();
    },
    renderListForForm: function () {
        GuardDuty.listview.listPageGuardDutyForm();
    }
});
GuardDuty.listView = Backbone.View.extend({
    el: 'div#main_container',
    listPage: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }

        activeLink('menu_guard_duty');
        addClass('menu_guardduty', 'active');
        GuardDuty.router.navigate('guardduty');
        var templateData = {};
        var that = this;
        this.$el.html(guardDutyListTemplate(templateData));
        this.loadGuardDutyData();

    },
    listPageGuardDutyForm: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        activeLink('menu_guard_duty');
        this.$el.html(guardDutyListTemplate);
        this.newGuardDutyForm(false, {});
    },
    actionRenderer: function (rowData) {
        if ((tempTypeInSession == TEMP_TYPE_A) && rowData.status != VALUE_TWO) {
            rowData.show_edit_btn = true;
        }
        if (rowData.status != VALUE_ZERO && rowData.status != VALUE_ONE) {
            rowData.show_form_one_btn = true;
        }
        return guardDutyActionTemplate(rowData);
    },
    loadGuardDutyData: function (btnObj, weekFrom, weekTo) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        var templateData = {};

        var today = new Date();

        var curr_week_from = new Date(today.setDate(today.getDate() - today.getDay())).toUTCString();
        var curr_week_to = new Date(today.setDate((today.getDate() - today.getDay()) + 6)).toUTCString();

        //var today = new Date();
        var week_one_from = new Date(today.setDate(today.getDate() + (7 - 1 - today.getDay() + 7) % 7 + 1));
        var week_one_to = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 6);

        var week_two_from = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7);
        var week_two_to = new Date(week_two_from.getFullYear(), week_two_from.getMonth(), week_two_from.getDate() + 6);

        var week_three_from = new Date(week_two_to.getFullYear(), week_two_to.getMonth(), week_two_to.getDate() + 1);
        var week_three_to = new Date(week_three_from.getFullYear(), week_three_from.getMonth(), week_three_from.getDate() + 6);

        var week_four_from = new Date(week_three_to.getFullYear(), week_three_to.getMonth(), week_three_to.getDate() + 1);
        var week_four_to = new Date(week_four_from.getFullYear(), week_four_from.getMonth(), week_four_from.getDate() + 6);

        templateData.curr_week_from = dateTo_DD_MM_YYYY(curr_week_from);
        templateData.curr_week_to = dateTo_DD_MM_YYYY(curr_week_to);
        templateData.week_one_from = dateTo_DD_MM_YYYY(week_one_from);
        templateData.week_one_to = dateTo_DD_MM_YYYY(week_one_to);
        templateData.week_two_from = dateTo_DD_MM_YYYY(week_two_from);
        templateData.week_two_to = dateTo_DD_MM_YYYY(week_two_to);
        templateData.week_three_from = dateTo_DD_MM_YYYY(week_three_from);
        templateData.week_three_to = dateTo_DD_MM_YYYY(week_three_to);
        templateData.week_four_from = dateTo_DD_MM_YYYY(week_four_from);
        templateData.week_four_to = dateTo_DD_MM_YYYY(week_four_to);

        var that = this;
        showTableContainer('guard_duty');
        GuardDuty.router.navigate('guardduty');
        $('#guard_duty_datatable_container').html(guardDutyTableTemplate(templateData));
        if (tempTypeInSession == TEMP_TYPE_A) {
            guarddutyDataTable = $('#guard_duty_datatable').DataTable({
                ajax: {url: 'guard_duty/get_guard_duty_data', dataSrc: "guard_duty_data", type: "post", data: $.extend({}, {'week_from': weekFrom, 'week_to': weekTo}, getTokenData())},
                bAutoWidth: false,
                ordering: false,
                processing: true,
                paging: false,
                language: dataTableProcessingAndNoDataMsg,
                serverSide: true,
                columns: [
                    {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
                    {data: 'checkpost_distillery_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'first_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'second_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'night_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'reserve_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'name', 'class': 'v-a-m', "defaultContent": '-'},
                    {data: 'first_shift_guard_name', 'class': 'v-a-m', "defaultContent": '-'},
                    {data: 'second_shift_guard_name', 'class': 'text-center', "defaultContent": '-'},
                    {data: 'night_shift_guard_name', 'class': 'text-center', "defaultContent": '-'},
                    {data: 'reserve_guard_name', 'class': 'text-center', "defaultContent": '-'},
                    {'class': 'details-control text-center', 'orderable': false, 'data': null, "defaultContent": ''}
                ],
                columnDefs: [{
                        targets: -1,
                        visible: false,
                    }],
                "initComplete": searchableDatatable
            });
            // guardReserveDataTable = $('#guard_reserve_duty_datatable').DataTable({
            //     ajax: {url: 'guard_duty/get_guard_duty_data', dataSrc: "guard_reserve_duty_data", type: "post"},
            //     bAutoWidth: false,
            //     ordering: false,
            //     processing: true,
            //     paging: false,
            //     language: dataTableProcessingAndNoDataMsg,
            //     serverSide: true,
            //     columns: [
            //         {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
            //         {data: 'reserve_guard_name', 'class': 'text-center', "defaultContent": ''},
            //         {'class': 'details-control text-center', 'orderable': false, 'data': null, "defaultContent": ''}
            //     ],
            //     columnDefs: [{
            //             targets: -1,
            //             visible: false
            //         }],
            //     "initComplete": searchableDatatable
            // });
        }
        if (tempTypeInSession == TEMP_TYPE_A) {
            currGuarddutyDataTable = $('#curr_guard_duty_datatable').DataTable({
                ajax: {url: 'guard_duty/get_curr_week_guard_duty_data', dataSrc: "curr_week_guard_duty_data", type: "post"},
                bAutoWidth: false,
                ordering: false,
                processing: true,
                paging: false,
                language: dataTableProcessingAndNoDataMsg,
                serverSide: true,
                columns: [
                    {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
                    {data: 'name', 'class': 'v-a-m'},
                    {data: 'first_shift_guard_name', 'class': 'v-a-m', "defaultContent": '-'},
                    {data: 'second_shift_guard_name', 'class': 'text-center', "defaultContent": '-'},
                    {data: 'night_shift_guard_name', 'class': 'text-center', "defaultContent": '-'},
                    {data: 'reserve_shift_guard_name', 'class': 'text-center', "defaultContent": '-'},
                    {'class': 'details-control text-center', 'orderable': false, 'data': null, "defaultContent": ''}
                ],
                columnDefs: [{
                        targets: -1,
                        visible: false
                    }],
                "initComplete": searchableDatatable
            });
        }
        $('#guard_duty_datatable_filter').remove();
        $('#curr_guard_duty_datatable_filter').remove();
    },
    frizeeGuardDutyData: function (btnObj, weekFrom, weekTo) {
        var checkpostId = [];
        var firstShiftGuardName = [];
        var secondShiftGuardName = [];
        var nightShiftGuardName = [];
        var reserveShiftGuardName = [];

        var table = $('#guard_duty_datatable').DataTable();

        var data = table
                .rows()
                .data();

        var jsonStr = '[';
        i = 0;
        data.each(function () {
            checkpostId[i] = table.rows(i).data()[0]['checkpost_distillery_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            firstShiftGuardName[i] = table.rows(i).data()[0]['first_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            secondShiftGuardName[i] = table.rows(i).data()[0]['second_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            nightShiftGuardName[i] = table.rows(i).data()[0]['night_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            reserveShiftGuardName[i] = table.rows(i).data()[0]['reserve_guard_id'];
            i++;
        });

        for (i = 0; i < checkpostId.length; i++) {
            jsonStr += "{";
            jsonStr += '"checkpostId":"' + checkpostId[i] + '" ,"firstShiftGuardName" : "' + firstShiftGuardName[i] + '" ,"secondShiftGuardName" : "' + secondShiftGuardName[i] + '" ,"nightShiftGuardName" : "' + nightShiftGuardName[i] + '" ,"reserveShiftGuardName" : "' + reserveShiftGuardName[i] + '" ,"weekFrom" : "' + weekFrom + '" ,"weekTo" : "' + weekTo + '"';
            jsonStr += "}";
            if (i < checkpostId.length - 1)
                jsonStr += ",";
        }
        jsonStr += ']';
        //  alert(jsonStr); die();
        var guardData = JSON.parse(jsonStr);
        var that = this;
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        $.ajax({
            url: 'guard_duty/submit_guard_duty',
            type: 'post',
            data: $.extend({}, {'guardData': guardData}, getTokenData()),
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                validationMessageShow('guardduty', textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (data) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(data);
                setNewToken(parseData.temp_token);
                if (parseData.success == false) {
                    validationMessageShow('guardduty', parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                showSuccess(parseData.message);
                GuardDuty.router.navigate('guardduty', {'trigger': true});
            }
        });
    },
});
