var checkpostDistilleryListTemplate = Handlebars.compile($('#checkpost_distillery_list_template').html());
var checkpostDistilleryTableTemplate = Handlebars.compile($('#checkpost_distillery_table_template').html());
var checkpostDistilleryActionTemplate = Handlebars.compile($('#checkpost_distillery_action_template').html());
var checkpostDistilleryFormTemplate = Handlebars.compile($('#checkpost_distillery_form_template').html());
var checkpostDistilleryViewTemplate = Handlebars.compile($('#checkpost_distillery_view_template').html());

var CheckpostDistillery = {
    run: function () {
        this.router = new this.Router();
        this.listview = new this.listView();
    }
};
CheckpostDistillery.Router = Backbone.Router.extend({
    routes: {
        'checkpostdistillery': 'renderList',
        'checkpostdistillery_form': 'renderListForForm',
        'edit_checkpostdistillery_form': 'renderList',
        'view_checkpostdistillery_form': 'renderList',
    },
    renderList: function () {
        CheckpostDistillery.listview.listPage();
    },
    renderListForForm: function () {
        CheckpostDistillery.listview.listPageCheckpostDistilleryForm();
    }
});
CheckpostDistillery.listView = Backbone.View.extend({
    el: 'div#main_container',
    listPage: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }

        activeLink('menu_checkpost_distillery');
        addClass('menu_checkpostdistillery', 'active');
        CheckpostDistillery.router.navigate('checkpostdistillery');
        var templateData = {};
        this.$el.html(checkpostDistilleryListTemplate(templateData));
        this.loadCheckpostDistilleryData();

    },
    listPageCheckpostDistilleryForm: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        activeLink('menu_checkpost_distillery');
        this.$el.html(checkpostDistilleryListTemplate);
        this.newCheckpostDistilleryForm(false, {});
    },
    loadCheckpostDistilleryData: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        var actionRenderer = function (data, type, full, meta) {
            return checkpostDistilleryActionTemplate({'checkpost_distillery_id': data});
        };
        var that = this;
        showTableContainer('checkpost_distillery');
        CheckpostDistillery.router.navigate('checkpostdistillery');
        $('#checkpost_distillery_datatable_container').html(checkpostDistilleryTableTemplate);
        allowOnlyIntegerValue('mobile_number_for_checkpostdistillery_list');
        if (tempTypeInSession == TEMP_TYPE_A || tempTypeInSession == TEMP_TYPE_EXCISE_DEPT_USER) {
            checkpostdistilleryDataTable = $('#checkpost_distillery_datatable').DataTable({
                ajax: {url: 'checkpost_distillery/get_checkpost_distillery_data', dataSrc: "checkpost_distillery_data", type: "post"},
                bAutoWidth: false,
                ordering: false,
                processing: true,
                language: dataTableProcessingAndNoDataMsg,
                serverSide: true,
                columns: [
                    {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
                    {data: 'name', 'class': 'v-a-m'},
                    {data: 'address', 'class': 'v-a-m'},
                    {'class': 'text-center', 'orderable': false, 'data': 'checkpost_distillery_id', "render": actionRenderer},
                ],
                "initComplete": searchableDatatable
            });
        }
        $('#checkpost_distillery_datatable_filter').remove();
    },
    newCheckpostDistilleryForm: function (isEdit, parseData) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        var that = this;
        if (isEdit) {
            var formData = parseData.checkpost_distillery_data;
            CheckpostDistillery.router.navigate('edit_checkpostdistillery_form');
        } else {
            var formData = {};
            CheckpostDistillery.router.navigate('checkpostdistillery_form');
        }
        eeDist = [];
        var templateData = {};
        templateData.checkpost_distillery_data = parseData.checkpost_distillery_data;
        showFormContainer('checkpost_distillery');
        $('#checkpost_distillery_form_container').html(checkpostDistilleryFormTemplate((templateData)));
        allowOnlyIntegerValue('no_of_eg_for_first_shift');
        allowOnlyIntegerValue('no_of_eg_for_second_shift');
        allowOnlyIntegerValue('no_of_eg_for_night_shift');
        generateBoxes('radio', shiftsCategoryArray, 'shifts_category', 'checkpost_distillery', formData.shifts_category, false, false);
        showSubContainer('shifts_category', 'checkpost_distillery', 'shifts_category_item', VALUE_ZERO, 'radio');
        showSubContainer('shifts_category', 'checkpost_distillery', 'shifts_category_general_item', VALUE_ONE, 'radio');
        $('#checkpost_distillery_form').find('input').keypress(function (e) {
            if (e.which == 13) {
                that.submitCheckpostDistillery($('#submit_btn_for_checkpost_distillery'));
            }
        });
    },
    editOrViewCheckpostDistillery: function (btnObj, checkpostdistilleryId, isEdit) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        if (!checkpostdistilleryId) {
            showError(invalidUserValidationMessage);
            return;
        }
        tempVillageData = [];
        var that = this;
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        $.ajax({
            url: 'checkpost_distillery/get_checkpost_distillery_data_by_id',
            type: 'post',
            data: $.extend({}, {'checkpost_distillery_id': checkpostdistilleryId}, getTokenData()),
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                showError(textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (response) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(response);
                setNewToken(parseData.temp_token);
                if (parseData.success === false) {
                    showError(parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                if (isEdit) {
                    that.newCheckpostDistilleryForm(isEdit, parseData);
                } else {
                    that.viewCheckpostDistilleryForm(parseData);
                }
            }
        });
    },
    viewCheckpostDistilleryForm: function (parseData) {
        var that = this;
        var templateData = {};
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        eeDist = [];
        var formData = parseData.checkpost_distillery_data;
        CheckpostDistillery.router.navigate('view_checkpostdistillery_form');
        showFormContainer('checkpost_distillery');
        $('#checkpost_distillery_form_container').html(checkpostDistilleryViewTemplate(formData));
        generateBoxes('radio', shiftsCategoryArray, 'shifts_category', 'checkpost_distillery', formData.shifts_category, false, false);
        showSubContainer('shifts_category', 'checkpost_distillery', 'shifts_category_item', VALUE_ZERO, 'radio');
        showSubContainer('shifts_category', 'checkpost_distillery', 'shifts_category_general_item', VALUE_ONE, 'radio');

    },
    checkValidationForCheckpostDistillery: function (checkpostDistilleryData) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }

        if (!checkpostDistilleryData.name) {
            return getBasicMessageAndFieldJSONArray('name', checkpostNameValidationMessage);
        }
        if (!checkpostDistilleryData.address) {
            return getBasicMessageAndFieldJSONArray('address', checkpostAddressValidationMessage);
        }
        if (!checkpostDistilleryData.shifts_category_for_checkpost_distillery) {
            $('#shifts_category_for_checkpost_distillery_1').focus();
            return getBasicMessageAndFieldJSONArray('shifts_category', selectOptionValidationMessage);
        }
        if (checkpostDistilleryData.shifts_category_for_checkpost_distillery == VALUE_ZERO) {
            if (!checkpostDistilleryData.no_of_eg_for_first_shift && checkpostDistilleryData.no_of_eg_for_first_shift != VALUE_ZERO) {
                return getBasicMessageAndFieldJSONArray('no_of_eg_for_first_shift', exciseGuardNoValidationMessage);
            }
            if (!checkpostDistilleryData.no_of_eg_for_second_shift && checkpostDistilleryData.no_of_eg_for_second_shift != VALUE_ZERO) {
                return getBasicMessageAndFieldJSONArray('no_of_eg_for_second_shift', exciseGuardNoValidationMessage);
            }
            if (!checkpostDistilleryData.no_of_eg_for_night_shift && checkpostDistilleryData.no_of_eg_for_night_shift != VALUE_ZERO) {
                return getBasicMessageAndFieldJSONArray('no_of_eg_for_night_shift', exciseGuardNoValidationMessage);
            }
        }
        if (checkpostDistilleryData.shifts_category_for_checkpost_distillery == VALUE_ONE) {
            if (!checkpostDistilleryData.no_of_eg_for_general_shift) {
                return getBasicMessageAndFieldJSONArray('no_of_eg_for_general_shift', exciseGuardNoValidationMessage);
            }
        }

        return '';
    },
    askForSubmitCheckpostDistillery: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        validationMessageHide();
        var yesEvent = 'CheckpostDistillery.listview.submitCheckpostDistillery()';
        showConfirmation(yesEvent, 'Submit');
    },
    submitCheckpostDistillery: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        var that = this;
        validationMessageHide();
        var checkpostDistilleryData = $('#checkpost_distillery_form').serializeFormJSON();
        var validationData = that.checkValidationForCheckpostDistillery(checkpostDistilleryData);
        if (validationData != '') {
            $('#' + validationData.field).focus();
            validationMessageShow('checkpost_distillery-' + validationData.field, validationData.message);
            return false;
        }

        var btnObj = $('#submit_btn_for_checkpost_distillery');
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        var checkpostDistilleryData = new FormData($('#checkpost_distillery_form')[0]);
        checkpostDistilleryData.append("csrf_token_eims_admin", getTokenData()['csrf_token_eims_admin']);
        //checkpostDistilleryData.append("module_type", moduleType);
        $.ajax({
            type: 'POST',
            url: 'checkpost_distillery/submit_checkpost_distillery',
            data: checkpostDistilleryData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                validationMessageShow('checkpost_distillery', textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (data) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(data);
                setNewToken(parseData.temp_token);
                if (parseData.success == false) {
                    validationMessageShow('checkpost_distillery', parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                showSuccess(parseData.message);
                CheckpostDistillery.router.navigate('checkpostdistillery', {'trigger': true});
            }
        });
    },
});
