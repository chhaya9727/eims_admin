var guardDutyListTemplate = Handlebars.compile($('#guard_duty_list_template').html());
var guardDutyTableTemplate = Handlebars.compile($('#guard_duty_table_template').html());

var GuardDuty = {
    run: function () {
        this.router = new this.Router();
        this.listview = new this.listView();
    }
};
GuardDuty.Router = Backbone.Router.extend({
    routes: {
        'guardduty': 'renderList',
        'guardduty_form': 'renderListForForm',
        'edit_guardduty_form': 'renderList',
        'view_guardduty_form': 'renderList',
    },
    renderList: function () {
        GuardDuty.listview.listPage();
    },
    renderListForForm: function () {
        GuardDuty.listview.listPageGuardDutyForm();
    }
});
GuardDuty.listView = Backbone.View.extend({
    el: 'div#main_container',
    listPage: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }

        activeLink('menu_guard_duty');
        addClass('menu_guardduty', 'active');
        GuardDuty.router.navigate('guardduty');
        var templateData = {};
        this.$el.html(guardDutyListTemplate(templateData));
        this.loadGuardDutyData();

    },
    listPageGuardDutyForm: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        activeLink('menu_guard_duty');
        this.$el.html(guardDutyListTemplate);
    },
    actionRenderer: function (rowData) {
        if ((tempTypeInSession == TEMP_TYPE_A || tempTypeInSession == TEMP_TYPE_EXCISE_DEPT_USER) && rowData.status != VALUE_TWO) {
            rowData.show_edit_btn = true;
        }
        if (rowData.status != VALUE_ZERO && rowData.status != VALUE_ONE) {
            rowData.show_form_one_btn = true;
        }
        return guardDutyActionTemplate(rowData);
    },
    guardDutyDataWithStatus: function (btnObj, weekFrom, weekTo, tblID) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }

        var that = this;
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        $.ajax({
            url: 'guard_duty/get_frizee_data_status',
            type: 'post',
            data: $.extend({}, {'week_from': weekFrom, 'week_to': weekTo}, getTokenData()),
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                showError(textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (response) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(response);
                setNewToken(parseData.temp_token);
                if (parseData.success === false) {
                    showError(parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                var frizeeDataStatus = parseData.frizee_data_status;
                var lastWeekFrizeeData = parseData.last_week_frizee_data;
                var btn_status = parseData.btn_status;
                that.loadGuardDutyData(btnObj, weekFrom, weekTo, tblID, frizeeDataStatus, lastWeekFrizeeData, btn_status)
            }
        });
    },
    loadGuardDutyData: function (btnObj, weekFrom, weekTo, tblID, frizeeDataStatus, lastWeekFrizeeData, btnStatus) {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }

//        $.post("guard_duty/get_frizee_data_status", function (data) {
//            alert(data);
////            $(".result").html(data);
//        });


        var templateData = {};

        // var today = new Date();

        // var curr_week_from = new Date(today.setDate(today.getDate() - today.getDay())).toUTCString();
        // var curr_week_to = new Date(today.setDate((today.getDate() - today.getDay()) + 6)).toUTCString();

        // //var today = new Date();
        // var week_one_from = new Date(today.setDate(today.getDate() + (7 - 1 - today.getDay() + 7) % 7 + 1));
        // var week_one_to = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 6);

        // var week_two_from = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7);
        // var week_two_to = new Date(week_two_from.getFullYear(), week_two_from.getMonth(), week_two_from.getDate() + 6);

        // var week_three_from = new Date(week_two_to.getFullYear(), week_two_to.getMonth(), week_two_to.getDate() + 1);
        // var week_three_to = new Date(week_three_from.getFullYear(), week_three_from.getMonth(), week_three_from.getDate() + 6);

        // var week_four_from = new Date(week_three_to.getFullYear(), week_three_to.getMonth(), week_three_to.getDate() + 1);
        // var week_four_to = new Date(week_four_from.getFullYear(), week_four_from.getMonth(), week_four_from.getDate() + 6);

        // templateData.curr_week_from = dateTo_DD_MM_YYYY(curr_week_from);
        // templateData.curr_week_to = dateTo_DD_MM_YYYY(curr_week_to);
        // templateData.week_one_from = dateTo_DD_MM_YYYY(week_one_from);
        // templateData.week_one_to = dateTo_DD_MM_YYYY(week_one_to);
        // templateData.week_two_from = dateTo_DD_MM_YYYY(week_two_from);
        // templateData.week_two_to = dateTo_DD_MM_YYYY(week_two_to);
        // templateData.week_three_from = dateTo_DD_MM_YYYY(week_three_from);
        // templateData.week_three_to = dateTo_DD_MM_YYYY(week_three_to);
        // templateData.week_four_from = dateTo_DD_MM_YYYY(week_four_from);
        // templateData.week_four_to = dateTo_DD_MM_YYYY(week_four_to);

        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        console.log(dateTo_DD_MM_YYYY(firstDay) + dateTo_DD_MM_YYYY(lastDay));

        $.post("guard_duty/get_current_month_data", {'current_month_from': firstDay, 'current_month_to': templateData.lastDay})
                .done(function (data) {
                    var parseData = JSON.parse(data);
                    var week_curr = JSON.parse(parseData.frizee_data_status_week_curr);
                    var week_one = JSON.parse(parseData.frizee_data_status_week_one);
                    var week_two = JSON.parse(parseData.frizee_data_status_week_two);
                    var week_three = JSON.parse(parseData.frizee_data_status_week_three);
                    
                });

        templateData.VALUE_ONE = VALUE_ONE;
        templateData.VALUE_TWO = VALUE_TWO;
        templateData.VALUE_THREE = VALUE_THREE;
        templateData.VALUE_FOUR = VALUE_FOUR;

        var firstShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.first_shift_guard_name != undefined) {
                if (full.first_shift_guard_name == VALUE_ZERO) {
                    full.first_shift_guard_name = '-';
                }
                var first_shift_guard_name = full.first_shift_guard_name.replace(/,\s*$/, "");
                return first_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        var secondShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.second_shift_guard_name != undefined) {
                if (full.second_shift_guard_name == VALUE_ZERO) {
                    full.second_shift_guard_name = '-';
                }
                var second_shift_guard_name = full.second_shift_guard_name.replace(/,\s*$/, "");
                return  second_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        var nightShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.night_shift_guard_name != undefined) {
                if (full.night_shift_guard_name == VALUE_ZERO) {
                    full.night_shift_guard_name = '-';
                }
                var night_shift_guard_name = full.night_shift_guard_name.replace(/,\s*$/, "");
                return night_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };

        templateData.week_one_frizee_data_status = 'display : none';
        templateData.week_two_frizee_data_status = 'display : none';
        templateData.week_three_frizee_data_status = 'display : none';
        templateData.week_four_frizee_data_status = 'display : none';

        templateData.week_one_report_data_status = 'display : none';
        templateData.week_two_report_data_status = 'display : none';
        templateData.week_three_report_data_status = 'display : none';
        templateData.week_four_report_data_status = 'display : none';

//        templateData.week_one_accordion = 'display : none';
//        templateData.week_two_accordion = 'display : none';
//        templateData.week_three_accordion = 'display : none';
//        templateData.week_four_accordion = 'display : none';

        if (lastWeekFrizeeData != undefined) {
            var max_date_from = lastWeekFrizeeData.week_from;
            var max_date_to = lastWeekFrizeeData.week_to;

            var dtf = new Date(max_date_from);
            var max_dt_from = dtf.setDate(dtf.getDate() + 7);
            var dtt = new Date(max_date_to);
            var max_dt_to = dtt.setDate(dtt.getDate() + 7);


            if (dateTo_DD_MM_YYYY(max_dt_from) == dateTo_DD_MM_YYYY(week_one_from) && dateTo_DD_MM_YYYY(max_dt_to) == dateTo_DD_MM_YYYY(week_one_to) && btnStatus == true) {
                templateData.week_one_frizee_data_status = '';
            } else {
                templateData.week_one_frizee_data_status = 'display : none';
            }
            if (dateTo_DD_MM_YYYY(max_dt_from) == dateTo_DD_MM_YYYY(week_two_from) && dateTo_DD_MM_YYYY(max_dt_to) == dateTo_DD_MM_YYYY(week_two_to) && btnStatus == true) {
                templateData.week_two_frizee_data_status = '';
            } else {
                templateData.week_two_frizee_data_status = 'display : none';
            }
            if (dateTo_DD_MM_YYYY(max_dt_from) == dateTo_DD_MM_YYYY(week_three_from) && dateTo_DD_MM_YYYY(max_dt_to) == dateTo_DD_MM_YYYY(week_three_to) && btnStatus == true) {
                templateData.week_three_frizee_data_status = '';
            } else {
                templateData.week_three_frizee_data_status = 'display : none';
            }
            if (dateTo_DD_MM_YYYY(max_dt_from) == dateTo_DD_MM_YYYY(week_four_from) && dateTo_DD_MM_YYYY(max_dt_to) == dateTo_DD_MM_YYYY(week_four_to) && btnStatus == true) {
                templateData.week_four_frizee_data_status = '';
            } else {
                templateData.week_four_frizee_data_status = 'display : none';
            }
        }

        if (weekFrom == dateTo_DD_MM_YYYY(week_one_from) && weekTo == dateTo_DD_MM_YYYY(week_one_to) && frizeeDataStatus > VALUE_ZERO) {
            templateData.week_one_report_data_status = '';
        } else {
            templateData.week_one_report_data_status = 'display : none;';
        }
        if (weekFrom == dateTo_DD_MM_YYYY(week_two_from) && weekTo == dateTo_DD_MM_YYYY(week_two_to) && frizeeDataStatus > VALUE_ZERO) {
            templateData.week_two_report_data_status = '';
        } else {
            templateData.week_two_report_data_status = 'display : none;';
        }
        if (weekFrom == dateTo_DD_MM_YYYY(week_three_from) && weekTo == dateTo_DD_MM_YYYY(week_three_to) && frizeeDataStatus > VALUE_ZERO) {
            templateData.week_three_report_data_status = '';
        } else {
            templateData.week_three_report_data_status = 'display : none;';
        }
        if (weekFrom == dateTo_DD_MM_YYYY(week_four_from) && weekTo == dateTo_DD_MM_YYYY(week_four_to) && frizeeDataStatus > VALUE_ZERO) {
            templateData.week_four_report_data_status = '';
        } else {
            templateData.week_four_report_data_status = 'display : none;';
        }

        $.post("guard_duty/get_all_frizee_data_status", {'week_curr_from': templateData.curr_week_from, 'week_curr_to': templateData.curr_week_to,
            'week_one_from': templateData.week_one_from, 'week_one_to': templateData.week_one_to,
            'week_two_from': templateData.week_two_from, 'week_two_to': templateData.week_two_to,
            'week_three_from': templateData.week_three_from, 'week_three_to': templateData.week_three_to})
                .done(function (data) {
                    var parseData = JSON.parse(data);
                    var week_curr = JSON.parse(parseData.frizee_data_status_week_curr);
                    var week_one = JSON.parse(parseData.frizee_data_status_week_one);
                    var week_two = JSON.parse(parseData.frizee_data_status_week_two);
                    var week_three = JSON.parse(parseData.frizee_data_status_week_three);
                    if (week_curr > VALUE_ZERO) {
                        $(".cardOne").show();
                        $(".cardTwo").hide();
                        $(".cardThree").hide();
                        $(".cardFour").hide();
                    }
                    if (week_one > VALUE_ZERO) {
                        $(".cardOne").show();
                        $(".cardTwo").show();
                        $(".cardThree").hide();
                        $(".cardFour").hide();
                    }
                    if (week_two > VALUE_ZERO) {
                        $(".cardOne").show();
                        $(".cardTwo").show();
                        $(".cardThree").show();
                        $(".cardFour").hide();
                    }
                    if (week_three > VALUE_ZERO) {
                        $(".cardOne").show();
                        $(".cardTwo").show();
                        $(".cardThree").show();
                        $(".cardFour").show();
                    }
                });

        var that = this;
        showTableContainer('guard_duty');
        GuardDuty.router.navigate('guardduty');
        $('#guard_duty_datatable_container').html(guardDutyTableTemplate(templateData));

        if (tblID != '') {
            if (tblID == VALUE_ONE)
                $('.week_one_data').collapse().show();
            if (tblID == VALUE_TWO)
                $('.week_two_data').collapse().show();
            if (tblID == VALUE_THREE)
                $('.week_three_data').collapse().show();
            if (tblID == VALUE_FOUR)
                $('.week_four_data').collapse().show();
        }
        if (tempTypeInSession == TEMP_TYPE_A || tempTypeInSession == TEMP_TYPE_EXCISE_DEPT_USER) {
            var tempStringDuty = '';
            guarddutyDataTable = $('#guard_duty_datatable_' + tblID).DataTable({
                ajax: {url: 'guard_duty/get_guard_duty_data', dataSrc: "guard_duty_data", type: "post", data: $.extend({}, {'week_from': weekFrom, 'week_to': weekTo}, getTokenData())},
                bAutoWidth: false,
                ordering: false,
                processing: true,
                paging: false,
                //language: dataTableProcessingAndNoDataMsg,
                language: {
                    "zeroRecords": "<span class='color-nic-red f-w-b'>You Required Maximum no. of Guards for Randomization Process.</span>"
                },
                serverSide: true,
                columns: [
                    {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
                    {data: 'checkpost_distillery_id', 'class': 'text-center', 'orderable': false, 'visible': false},
                    {data: 'first_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'second_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'night_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'reserve_shift_guard_id', 'class': 'text-center', 'orderable': false, 'visible': false, "defaultContent": '-'},
                    {data: 'name', 'class': 'v-a-m'},
                    {data: 'first_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': firstShiftGuardNameRenderer},
                    {data: 'second_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': secondShiftGuardNameRenderer},
                    {data: 'night_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': nightShiftGuardNameRenderer},
                    {'class': 'details-control text-center', 'orderable': false, 'data': null, "defaultContent": ''}
                ],
                "rowCallback": function (row, data) {
                    if (data.reserve_shift_guard_name != VALUE_ZERO && data.reserve_shift_guard_name != '-' && data.reserve_shift_guard_name != null) {
                        tempStringDuty += data.reserve_shift_guard_name + '<hr/>';
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: false
                    }],
                "initComplete": function (settings, json) {
                    if (tblID == VALUE_ONE) {
                        $('#week1_reserve_guard_list').html(tempStringDuty);
                    }
                    if (tblID == VALUE_TWO) {
                        $('#week2_reserve_guard_list').html(tempStringDuty);
                    }
                    if (tblID == VALUE_THREE) {
                        $('#week3_reserve_guard_list').html(tempStringDuty);
                    }
                    if (tblID == VALUE_FOUR) {
                        $('#week4_reserve_guard_list').html(tempStringDuty);
                    }

                    this.api().columns().every(function () {
                        var that = this;
                        $('input', this.header()).on('keyup change clear', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                        $('select', this.header()).on('change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                }
            });
            // if ( ! guarddutyDataTable.data().any() ) {
            //     alert( 'Empty table' );
            // }
            $('#guard_duty_datatable_' + tblID + '_filter').remove();

        }
        if (tempTypeInSession == TEMP_TYPE_A || tempTypeInSession == TEMP_TYPE_EXCISE_DEPT_USER) {
            var tempString = '';
            currGuarddutyDataTable = $('#curr_guard_duty_datatable').DataTable({
                ajax: {url: 'guard_duty/get_curr_week_guard_duty_data', dataSrc: "curr_week_guard_duty_data", type: "post"},
                bAutoWidth: false,
                ordering: false,
                processing: true,
                paging: false,
                language: dataTableProcessingAndNoDataMsg,
                serverSide: true,
                columns: [
                    {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
                    {data: 'name', 'class': 'v-a-m'},
                    {data: 'first_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': firstShiftGuardNameRenderer},
                    {data: 'second_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': secondShiftGuardNameRenderer},
                    {data: 'night_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': nightShiftGuardNameRenderer},
                    {'class': 'details-control text-center', 'orderable': false, 'data': null, "defaultContent": ''}
                ],
                "rowCallback": function (row, data) {
                    if (data.reserve_shift_guard_name != VALUE_ZERO && data.reserve_shift_guard_name != '-' && data.reserve_shift_guard_name != null) {
                        tempString += data.reserve_shift_guard_name + '<hr/>';
                    }
                },
                columnDefs: [{
                        targets: -1,
                        visible: false
                    }],
                "initComplete": function (settings, json) {
                    $('#current_reserve_guard_list').html(tempString);
                    this.api().columns().every(function () {
                        var that = this;
                        $('input', this.header()).on('keyup change clear', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                        $('select', this.header()).on('change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                }
            });
        }
        $('#curr_guard_duty_datatable_filter').remove();
    },
    frizeeGuardDutyData: function (btnObj, weekFrom, weekTo, tblId) {
        var checkpostId = [];
        var firstShiftGuardName = [];
        var secondShiftGuardName = [];
        var nightShiftGuardName = [];
        var reserveShiftGuardName = [];
        var firstShiftGuardId = [];
        var secondShiftGuardId = [];
        var nightShiftGuardId = [];
        var reserveShiftGuardId = [];

        var table = $('#guard_duty_datatable_' + tblId).DataTable();

        var data = table
                .rows()
                .data();

        var jsonStr = '[';
        i = 0;
        data.each(function () {
            checkpostId[i] = table.rows(i).data()[0]['checkpost_distillery_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            firstShiftGuardName[i] = table.rows(i).data()[0]['first_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            secondShiftGuardName[i] = table.rows(i).data()[0]['second_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            nightShiftGuardName[i] = table.rows(i).data()[0]['night_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            reserveShiftGuardName[i] = table.rows(i).data()[0]['reserve_shift_guard_name'];
            i++;
        });
        i = 0;
        data.each(function () {
            firstShiftGuardId[i] = table.rows(i).data()[0]['first_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            secondShiftGuardId[i] = table.rows(i).data()[0]['second_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            nightShiftGuardId[i] = table.rows(i).data()[0]['night_shift_guard_id'];
            i++;
        });
        i = 0;
        data.each(function () {
            reserveShiftGuardId[i] = table.rows(i).data()[0]['reserve_shift_guard_id'];
            i++;
        });
        for (i = 0; i < checkpostId.length; i++) {
            jsonStr += "{";
            jsonStr += '"checkpost_distillery_id":"' + checkpostId[i] + '" ,"first_shift_guard_name" : "' + firstShiftGuardName[i] + '" ,"second_shift_guard_name" : "' + secondShiftGuardName[i] + '" ,"night_shift_guard_name" : "' + nightShiftGuardName[i] + '" ,"reserve_shift_guard_name" : "' + reserveShiftGuardName[i] + '" ,"week_from" : "' + weekFrom + '" ,"week_to" : "' + weekTo + '" ,"first_shift_guard_id" : "' + firstShiftGuardId[i] + '" ,"second_shift_guard_id" : "' + secondShiftGuardId[i] + '" ,"night_shift_guard_id" : "' + nightShiftGuardId[i] + '" ,"reserve_shift_guard_id" : "' + reserveShiftGuardId[i] + '"';
            jsonStr += "}";
            if (i < checkpostId.length - 1)
                jsonStr += ",";
        }
        jsonStr += ']';

        var guardData = JSON.parse(jsonStr);
        var that = this;
        var ogBtnHTML = btnObj.html();
        var ogBtnOnclick = btnObj.attr('onclick');
        btnObj.html(iconSpinnerTemplate);
        btnObj.attr('onclick', '');
        $.ajax({
            url: 'guard_duty/submit_guard_duty',
            type: 'post',
            data: $.extend({}, {'guardData': guardData}, getTokenData()),
            error: function (textStatus, errorThrown) {
                generateNewCSRFToken();
                if (!textStatus.statusText) {
                    loginPage();
                    return false;
                }
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                validationMessageShow('guardduty', textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (data) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                var parseData = JSON.parse(data);
                setNewToken(parseData.temp_token);
                if (parseData.success == false) {
                    validationMessageShow('guardduty', parseData.message);
                    $('html, body').animate({scrollTop: '0px'}, 0);
                    return false;
                }
                showSuccess(parseData.message);
                $('#frizee_btn_' + tblId).remove();
                $('#referesh_btn_' + tblId).remove();
                $('#report_btn_' + tblId).show();
                GuardDuty.router.navigate('guardduty', {'trigger': true});
            }
        });
    },
    generateReport: function (btnObj, weekFrom, weekTo, tblID) {
        if (!weekFrom || !weekTo || !tblID) {
            showError(invalidAccessValidationMessage);
            return false;
        }
        $('#week_from_for_report').val(weekFrom);
        $('#week_to_for_report').val(weekTo);
        $('#guard_duty_report_pdf_form').submit();
    },
});