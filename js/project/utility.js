(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push($.trim(this.value) || '');
            } else {
                o[this.name] = $.trim(this.value) || '';
            }
        });
        return o;
    };

})(jQuery);

function loginPage() {
    window.location = baseUrl + 'login';
}

function checkValidation(moduleName, fieldName, messageName) {
    var val = $('#' + fieldName).val();
    var newFieldName = moduleName + '-' + fieldName;
    validationMessageHide(newFieldName);
    if (!val || !val.trim()) {
        validationMessageShow(newFieldName, messageName);
    }
}

function validationMessageHide(moduleName) {
    if (typeof moduleName === "undefined") {
        $('.error-message').hide();
        $('.error-message').html('');
    } else {
        $('.error-message-' + moduleName).hide();
        $('.error-message-' + moduleName).html('');
    }
}

function validationMessageShow(moduleName, messageName) {
    $('.error-message-' + moduleName).html(messageName);
    $('.error-message-' + moduleName).show();
}

function getBasicMessageAndFieldJSONArray(field, message) {
    var returnData = {};
    returnData['message'] = message;
    returnData['field'] = field;
    return returnData;
}

function resetForm(formId) {
    validationMessageHide();
    $('#' + formId).trigger("reset");
}

function checkPasswordValidation(moduleName, id) {
    var password = $('#' + id).val();
    if (!password) {
        validationMessageShow(moduleName + '-' + id, passwordValidationMessage);
        return;
    }
    var msg = passwordValidation(password);
    if (msg != '') {
        validationMessageShow(moduleName + '-' + id, msg);
        return;
    }
    validationMessageHide(moduleName + '-' + id);
}

function passwordValidation(password) {
    var regex = new RegExp(passwordRegex);
    if (!regex.test(password)) {
        return passwordPolicyValidationMessage;
    }
    return '';
}

function checkPasswordValidationForRetypePassword(moduleName, compareId, id) {
    var retypePassword = $('#' + compareId).val();
    if (!retypePassword) {
        validationMessageHide(moduleName + '-' + compareId);
        return;
    }
    var password = $('#' + id).val();
    if (password != retypePassword) {
        validationMessageShow(moduleName + '-' + compareId, passwordAndRetypePasswordValidationMessage);
        return;
    }
    validationMessageHide(moduleName + '-' + compareId);
}

function passwordValidation(password) {
    if (!passwordRegex.test(password)) {
        return passwordPolicyValidationMessage;
    }
    return '';
}

function generateSelect2() {
    $('.select2').select2({"allowClear": true});
}

function generateSelect2WithId(id) {
    $('#' + id).select2({"allowClear": true});
}

function renderOptionsForTwoDimensionalArray(dataArray, comboId, addBlankOption) {
    if (!dataArray) {
        return false;
    }
    if (typeof addBlankOption === "undefined") {
        addBlankOption = true;
    }
    if (addBlankOption) {
        $('#' + comboId).html('<option value="">&nbsp;</option>');
    }
    var data = {};
    var optionResult = "";
    $.each(dataArray, function (index, dataObject) {
        data = {"value_field": index, 'text_field': dataObject};
        optionResult = optionTemplate(data);
        $("#" + comboId).append(optionResult);
    });
}

function renderOptionsForTwoDimensionalArrayWithKeyValueWithCombinationFor(dataArray, comboId, keyId, valueId, message) {
    if (!dataArray) {
        return false;
    }
    $('#' + comboId).html('<option value="">Select ' + message + '</option>');
    var data = {};
    var optionResult = "";
    $.each(dataArray, function (index, dataObject) {
        if (dataObject != undefined && dataObject[keyId] != 0) {
            data = {"value_field": dataObject[keyId], 'text_field': dataObject[valueId]};
            optionResult = optionTemplate(data);
            $("#" + comboId).append(optionResult);
        }
    });
}
function renderOptionsForStateAndDistrict(dataArray, comboId, keyId, valueId, message) {
    if (!dataArray) {
        return false;
    }
    $('#' + comboId).html('<option value="0">' + message + '</option>');
    var data = {};
    var optionResult = "";
    $.each(dataArray, function (index, dataObject) {
        if (dataObject != undefined && dataObject[keyId] != 0) {
            data = {"value_field": dataObject[keyId], 'text_field': dataObject[valueId]};
            optionResult = optionTemplate(data);
            $("#" + comboId).append(optionResult);
        }
    });
}

function renderOptionsForTwoDimensionalArrayWithKeyValueWithCombination(dataArray, comboId, keyId, valueId, valueId2, addBlankOption) {
    if (!dataArray) {
        return false;
    }
    if (typeof addBlankOption === "undefined") {
        addBlankOption = true;
    }
    if (addBlankOption) {
        $('#' + comboId).html('<option value="">&nbsp;</option>');
    }
    var data = {};
    var optionResult = "";
    var textField = "";
    $.each(dataArray, function (index, dataObject) {
        if (dataObject != undefined && dataObject[keyId] != 0) {
            if (dataObject[valueId2]) {
                textField = dataObject[valueId] + (dataObject[valueId2] != null ? '( ' + dataObject[valueId2] + ' )' : '');
            } else {
                textField = dataObject[valueId];
            }
            data = {"value_field": dataObject[keyId], 'text_field': textField};
            optionResult = optionTemplate(data);
            $("#" + comboId).append(optionResult);
        }
    });
}

function dateTo_DD_MM_YYYY(date, delimeter) {
    var delim = delimeter ? delimeter : '-';
    var d = new Date(date || Date.now()),
            month = d.getMonth() + 1,
            day = '' + d.getDate(),
            year = d.getFullYear();
    if (month < 10)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    return [day, month, year].join(delim);
}

function dateTo_MM_YYYY(date, delimeter) {
    var delim = delimeter ? delimeter : '-';
    var d = new Date(date || Date.now()),
            month = d.getMonth() + 1,
            day = '' + d.getDate(),
            year = d.getFullYear();
    if (month < 10)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    return [month, year].join(delim);
}

function getPerviousDateTo_DD_MM_YYYY(days, date) {
    var d = new Date(date || Date.now());
    d.setDate(d.getDate() - days);
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    if (month < 10)
        month = '0' + month;
    if (day < 10)
        day = '0' + day;
    return [day, month, year].join('-');
}

function getNextDateTo_DD_MM_YYYY(days, date) {
    var ndate = date.split("-").reverse().join("-");
    var d = new Date(ndate || Date.now());
    d.setDate(d.getDate() + days);
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    if (month < 10)
        month = '0' + month;
    if (day < 10)
        day = '0' + day;
    return [day, month, year].join('-');
}

function dateTo_YYYY_MM_DD(date, delimeter) {
    return date.split('-').reverse().join('-');
}

function getCurrentTime() {
    var date = new Date();
    var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 12 ? "PM" : "AM";
    hours = hours < 10 ? "0" + hours : hours;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    return hours + ":" + minutes + ":" + " " + am_pm;
}

function checkPincode(obj) {
    var pincode = obj.val();
    var pincodeValidationMessage = pincodeValidation(pincode);
    if (pincodeValidationMessage != '') {
        showError(pincodeValidationMessage);
        return false;
    }
}


function pincodeValidation(pincode) {
    if (!pincode) {
        return '';
    }
    var regex = /^[1-9][0-9]{5}$/;
    if (!regex.test(pincode)) {
        return 'Invalid Pincode';
    }
    return '';
}

function checkNumeric(obj) {
    if (!$.isNumeric(obj.val())) {
        obj.val("");
    }
}

function allowOnlyIntegerValue(id) {
    $('#' + id).keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
}

function roundOff(obj) {
    var amount = obj.val();
    if ($.isNumeric(amount)) {
        obj.val(parseFloat(Math.abs(amount)).toFixed(2));
    }
}


var serialNumberRenderer = function (data, type, full, meta) {
    return meta.row + meta.settings._iDisplayStart + 1;
};

var districtRenderer = function (data, type, full, meta) {
    return talukaArray[data] ? talukaArray[data] : '-';
};
var yesNoRenderer = function (data, type, full, meta) {
    return yesNoTypeArray[data] ? yesNoTypeArray[data] : '-';
};

var appStatusRenderer = function (data, type, full, meta) {
    return '<div id="status_' + data + '">' + (appStatusArray[full.status] ? appStatusArray[full.status] : appStatusArray[VALUE_ZERO]) + '</div>';
};

var queryStatusRenderer = function (data, type, full, meta) {
    return '<div id="query_status_' + data + '">' + (queryStatusArray[full.query_status] ? queryStatusArray[full.query_status] : queryStatusArray[VALUE_ZERO]) + '</div>';
};

var dateRenderer = function (data, type, full, meta) {
    return dateTo_DD_MM_YYYY(data);
};

function checkAlphabets(obj) {
    obj.val(obj.val().replace(/[^a-z A-Z.]/g, ""));
    if ((event.which >= 48 && event.which <= 57)) {
        event.preventDefault();
    }
}

function checkAlphabetsBlur(obj) {
    obj.val(obj.val().replace(/[^a-z A-Z.]/g, ''));
}

function datePicker() {
    $('.date_picker').datetimepicker({
        icons:
                {
                    up: 'fa fa-angle-up',
                    down: 'fa fa-angle-down'
                },
                format: "mm-yyyy",
    });
    $('.date_picker').keyup(function (e) {
        e = e || window.event; //for pre-IE9 browsers, where the event isn't passed to the handler function
        if (e.keyCode == '37' || e.which == '37' || e.keyCode == '39' || e.which == '39') {
            var message = ' ' + $('.ui-state-hover').html() + ' ' + $('.ui-datepicker-month').html() + ' ' + $('.ui-datepicker-year').html();
            if ($(this).attr('id') == 'startDate') {
                $(".date_picker").val(message);
            }
        }
    });
}

function checkValidationForMobileNumber(moduleName, id) {
    var mobileNumber = $('#' + id).val();
    if (!mobileNumber) {
        validationMessageShow(moduleName + '-' + id, mobileValidationMessage);
        return;
    }
    var validate = mobileNumberValidation(mobileNumber);
    if (validate != '') {
        validationMessageShow(moduleName + '-' + id, validate);
        return false;
    }
    validationMessageHide(moduleName + '-' + id);
}

function mobileNumberValidation(mobileNumber) {
    var filter = /^[0-9-+]+$/;
    if (mobileNumber.length != 10 || !filter.test(mobileNumber)) {
        return invalidMobileValidationMessage;
    }
    return '';
}

function checkValidationForEmail(moduleName, id) {
    var emailId = $('#' + id).val();
    if (!emailId) {
        validationMessageShow(moduleName + '-' + id, emailValidationMessage);
        return false;
    }
    var validate = emailIdValidation(emailId);
    if (validate != '') {
        validationMessageShow(moduleName + '-' + id, validate);
        return false;
    }
    validationMessageHide(moduleName + '-' + id);
}

function emailIdValidation(emailId) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(emailId)) {
        return invalidEmailValidationMessage;
    }
    return '';
}

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 10000
});

function toastFire(type, message) {
    Toast.fire({
        type: type,
        title: '<span style="padding-left: 10px; padding-right: 10px;">' + message + '</span>',
        showCloseButton: true,
    });
}

function showConfirmation(yesEvent, message) {
    $('.swal2-popup').removeClass('p-5px');
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
        title: 'Are you sure You want to ' + message + ' ?',
        type: 'warning',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: 'Yes, ' + message + ' it !',
        cancelButtonText: 'No, Cancel !',
    }).then((result) => {
        if (result.value) {
            $('#temp_btn').attr('onclick', yesEvent);
            $('#temp_btn').click();
            $('#temp_btn').attr('onclick', '');
        }
    });
}

function showPopup() {
    const swalWithBootstrapButtons = Swal.mixin({});
    swalWithBootstrapButtons.fire({
        showCancelButton: false,
        showConfirmButton: false,
        html: '<div id="popup_container"></div>',
    });
    $('.swal2-popup').addClass('p-5px');
}

function showSuccess(message) {
    toastFire('success', message);
}

function showError(message) {
    toastFire('error', message);
}

function activeLink(id) {
    $('.nav-link').removeClass('active');
    addClass(id, 'active');
}

function addClass(id, className) {
    $('#' + id).addClass(className);
}

function addTagSpinner(id) {
    $('#' + id).parent().find('.error-message').before(tagSpinnerTemplate);
}

function removeTagSpinner() {
    $('#tag_spinner').remove();
}

function resetModel() {
    $('#popup_modal').modal('hide');
    $('#model_title').html('');
    $('#model_body').html('');
}

function activeSelectedBtn(obj) {
    $('.small-btn').removeClass('btn-success');
    $('.small-btn').addClass('btn-primary');
    if (obj) {
        obj.removeClass('btn-primary');
        obj.addClass('btn-success');
    }
}

function selectOrDeselectRow(obj, id) {
    if (obj.hasClass('bg-white')) {
        obj.removeClass('bg-white');
        obj.addClass('bg-active');
        $('#' + id).prop('checked', true);
    } else {
        obj.removeClass('bg-active');
        obj.addClass('bg-white');
        $('#' + id).prop('checked', false);
    }
}

function getTotalSelectedRows(id) {
    $('#' + id).html($('.bg-active').length);
}

var trimColumnValueRenderer = function (data, type, full, meta) {
    return (data).trim();
};

function generateRandomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function getDistrictData(stateId, districtId) {
    renderOptionsForTwoDimensionalArrayWithKeyValueWithCombinationFor([], districtId, 'district_code', 'district_name', 'District');
    $('#' + districtId).val('');
    var stateCode = $('#' + stateId).val();
    if (!stateCode) {
        return;
    }
    var districtData = tempDistrictData[stateCode] ? tempDistrictData[stateCode] : [];
    renderOptionsForTwoDimensionalArrayWithKeyValueWithCombinationFor(districtData, districtId, 'district_code', 'district_name', 'District');
    $('#' + districtId).val('');
}

function fileUploadValidation(imageUploadAttrId, maxFileSize) {
    var allowedFiles = ['jpg', 'png', 'jpeg', 'jfif', 'pdf'];
    var fileName = $('#' + imageUploadAttrId).val();
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    if ($.inArray(ext, allowedFiles) == -1) {
        $('#' + imageUploadAttrId).val('');
        $('#' + imageUploadAttrId).focus();
        return 'Please upload File having extensions: <b>' + allowedFiles.join(', ') + '</b> only.';
    }
    if (($('#' + imageUploadAttrId)[0].files[0].size / 1024) > maxFileSize) {
        return 'Maximum upload size ' + (maxFileSize / 1024) + ' MB only.';
    }
    return false;
}

var dataTableProcessingAndNoDataMsg = {
    'loadingRecords': '<span class="color-nic-blue"><i class="fas fa-spinner fa-spin fa-2x"></i></span>',
    'processing': '<span class="color-nic-blue"><i class="fas fa-spinner fa-spin fa-3x"></i></span>',
    'emptyTable': 'No Data Available !'
};

var searchableDatatable = function (settings, json) {
    this.api().columns().every(function () {
        var that = this;
        $('input', this.header()).on('keyup change clear', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
        $('select', this.header()).on('change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });
}

var fontRenderer = function (data, type, full, meta) {
    return '<span class="table-bold-data">' + data + '</span>';
};

function getSubCategoryData(categoryIdText, subCategoryIdText) {
    renderOptionsForTwoDimensionalArrayWithKeyValueWithCombinationFor([], subCategoryIdText, 'sub_category_id', 'sub_category_name', 'Sub Category');
    var categoryId = $('#' + categoryIdText).val();
    if (!categoryId) {
        return;
    }
    $.ajax({
        url: 'pmanage/get_sub_category_data_for_product',
        type: 'post',
        data: $.extend({}, {'category_id': categoryId}, getTokenData()),
        error: function (textStatus, errorThrown) {
            generateNewCSRFToken();
            showError(textStatus.statusText);
            $('html, body').animate({scrollTop: '0px'}, 0);
        },
        success: function (response) {
            var parseData = JSON.parse(response);
            setNewToken(parseData.temp_token);
            if (parseData.success === false) {
                showError(parseData.message);
                $('html, body').animate({scrollTop: '0px'}, 0);
                return false;
            }
            renderOptionsForTwoDimensionalArrayWithKeyValueWithCombinationFor(parseData.sub_category_data, subCategoryIdText, 'sub_category_id', 'sub_category_name', 'Sub Category');
            $('#' + subCategoryIdText).val('');
        }
    });
}

function generateBoxes(type, data, id, moduleName, existingArray, isBr) {
    $.each(data, function (index, value) {
        var template = '<label class="' + type + '-inline f-w-n m-b-0px m-r-10px"><input type="' + type + '" id="' + id + '_for_' + moduleName + '_' + index + '" name="' + id + '_for_' + moduleName + '" value="' + index + '">&nbsp;&nbsp;' + value + '</label>';
        if (isBr) {
            template += '<br>';
        }
        $('#' + id + '_container_for_' + moduleName).append(template);
    });
    if (existingArray) {
        if (type == 'checkbox') {
            var existingData = (existingArray).split(',');
            $.each(existingData, function (index, value) {
                $('input[name=' + id + '_for_' + moduleName + '][value="' + value + '"]').click();
            });
        } else {
            $('input[name=' + id + '_for_' + moduleName + '][value="' + existingArray + '"]').click();
        }
    } else {
        $('input[name=' + id + '_for_' + moduleName + '][value="' + existingArray + '"]').click();
    }
}

function getLocation() {
    tempLocationData = {};
    tempLocationData.latitude = '';
    tempLocationData.longitude = '';
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getCurrentLatLong);
    }
}

function getCurrentLatLong(position) {
    $('#latitude_for_road_details').val(position.coords.latitude);
    $('#longitude_for_road_details').val(position.coords.longitude);
}

function showSubContainer(id, moduleName, showId, showValue, type) {
    var otherId = '';
    if (type == 'radio') {
        otherId = $('input[name=' + id + '_for_' + moduleName + ']:checked').val();
    } else {
        otherId = $('#' + id + '_for_' + moduleName).val();
    }
    if (otherId == showValue) {
        $('#' + showId + '_container_for_' + moduleName + '').show();
    }
    $('input[name=' + id + '_for_' + moduleName + ']').change(function () {
        var other = $(this).val();
        $('#' + showId + '_container_for_' + moduleName + '').hide();
        if (other == showValue) {
            $('#' + showId + '_container_for_' + moduleName + '').show();
            return false;
        }
    });
}

function showSubContainerForPaymentDetails(id, moduleName, showId, type) {
    var otherId = '';
    if (type == 'radio') {
        otherId = $('input[name=' + id + '_for_' + moduleName + ']:checked').val();
    } else {
        otherId = $('#' + id + '_for_' + moduleName).val();
    }
    if (otherId == VALUE_ONE || otherId == VALUE_TWO) {
        $('#' + showId + '_container_for_' + moduleName + '').show();
    }
    $('input[name=' + id + '_for_' + moduleName + ']').change(function () {
        validationMessageHide('wmregistration-uc-' + id + '_for_' + moduleName);
        var other = $(this).val();
        $('#' + showId + '_container_for_' + moduleName + '').hide();
        if (other == VALUE_ONE || other == VALUE_TWO) {
            if (other == VALUE_ONE) {
                $('.utitle_for_' + moduleName).html('Challan Copy');
            } else {
                $('.utitle_for_' + moduleName).html('Payment Details');
            }
            $('#' + showId + '_container_for_' + moduleName + '').show();
            return false;
        }
    });
}

function getEncryptedId(id) {
    return generateRandomString(3) + window.btoa(id) + generateRandomString(3);
}

function getDescryptedId(encryptedId) {
    var tempString = encryptedId.substr(3);
    var tempString2 = tempString.substr(0, -3);
    return window.atob(tempString2);
}

function resetCounter(className) {
    var cnt = 1;
    $('.' + className).each(function () {
        $(this).html(cnt);
        cnt++;
    });
}

function getTextOfId(dataArray, value, compareValue, otherValue) {
    var data = dataArray[value] ? dataArray[value] : '';
    if (compareValue != '' && otherValue != '') {
        if (value == compareValue) {
            data = data + '(' + otherValue + ')';
        }
    }
    return data;
}

var emailRenderer = function (data, type, full, meta) {
    return data.replace('@', '<br>@');
};

function removeDocument(id, moduleName) {
    $('#' + id + '_name_container_for_' + moduleName).hide();
    $('#' + id + '_container_for_' + moduleName).show();
    $('#' + id + '_name_href_for_' + moduleName).attr('href', '');
    $('#' + id + '_name_for_' + moduleName).html('');
    $('#' + id + '_remove_btn_for_' + moduleName).attr('onclick', '');
}

function removeRowDetails(moduleName, cnt) {
    $('#' + moduleName + '_row_' + cnt).remove();
    resetCounter(moduleName + '-cnt');
}

var _validFileExtensions = [".jpg", ".jpeg", ".png"];
//var _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf"];
var _imageFileExtensions = [".jpg", ".jpeg", ".png"];
function imagePdfValidation(oInput, message, imagePdfUploadAttrId) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                showError(message + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
            if (jQuery.inArray(sCurExtension, _imageFileExtensions) != -1) {
                if (($('#' + imagePdfUploadAttrId)[0].files[0].size / 1204) > maxFileSizeInKb) {
                    showError('Maximum upload size ' + maxFileSizeInKb + ' mb only in ' + message);
                    return false;
                }
            } else {
                if ((($('#' + imagePdfUploadAttrId)[0].files[0].size / 1024) / 1024) > maxFileSizeInMb) {
                    showError('Maximum upload size ' + maxFileSizeInMb + ' mb only in ' + message);
                    return true;
                }
            }
        }
    }
    return true;
}

function imagePdfUploadValidation(imageUploadAttrId, message, isValidateFileSize) {
    var allowedFiles = ['.jpg', '.png', '.jpeg'];
//    var allowedFiles = ['.jpg', '.png', '.jpeg', '.pdf'];
    var allowedFilesImage = ['.jpg', '.png', '.jpeg'];
    var imageName = $('#' + imageUploadAttrId).val();
    var fileExtension = imageName.replace(/^.*\./, '');
//    if (imageName.length > 0) {
    var regex = new RegExp('([a-zA-Z0-9\s_\\.\-:])+(' + allowedFiles.join('|') + ')$');

    if (!regex.test(imageName.toLowerCase())) {
        showError(message + ' <b>' + allowedFiles.join(', ') + '</b> only.');
        return true;
    }

    if (jQuery.inArray('.' + fileExtension, allowedFilesImage) != -1) {
        if (isValidateFileSize) {
            if (($('#' + imageUploadAttrId)[0].files[0].size / 1204) > maxFileSizeInKb) {
                showError('Maximum upload size ' + maxFileSizeInKb + 'kb only.');
                return true;
            }
        }
    } else {
        if (isValidateFileSize) {
            if ((($('#' + imageUploadAttrId)[0].files[0].size / 1024) / 1024) > maxFileSizeInMb) {
                showError('Maximum upload size ' + maxFileSizeInMb + ' mb only.');
                return true;
            }
        }
    }
//    }
    return false;
}

function getTotalEmployee(btnObj) {

    var a = $('#direct_unskilled').val() == "" ? 0 : $('#direct_unskilled').val();
    var b = $('#direct_semiskilled').val() == "" ? 0 : $('#direct_semiskilled').val();
    var c = $('#direct_skilled').val() == "" ? 0 : $('#direct_skilled').val();

    var res = parseFloat(a) + parseFloat(b) + parseFloat(c);
    $('#direct_total').val(res);


    var d = $('#contractor_unskilled').val() == "" ? 0 : $('#contractor_unskilled').val();
    var e = $('#contractor_semiskilled').val() == "" ? 0 : $('#contractor_semiskilled').val();
    var f = $('#contractor_skilled').val() == "" ? 0 : $('#contractor_skilled').val();

    var res1 = parseFloat(d) + parseFloat(e) + parseFloat(f);
    $('#contractor_total').val(res1);

    var res2 = parseFloat(a) + parseFloat(d);
    $('#total_unskilled').val(res2);

    var res3 = parseFloat(b) + parseFloat(e);
    $('#total_semiskilled').val(res3);

    var res4 = parseFloat(c) + parseFloat(f);
    $('#total_skilled').val(res4);

    var res5 = parseFloat(res) + parseFloat(res1);
    $('#total_total').val(res5);


    var g = $('#direct_male').val() == "" ? 0 : $('#direct_male').val();
    var h = $('#contractor_male').val() == "" ? 0 : $('#contractor_male').val();

    var res6 = parseFloat(g) + parseFloat(h);
    $('#total_male').val(res6);

    var i = $('#direct_female').val() == "" ? 0 : $('#direct_female').val();
    var j = $('#contractor_female').val() == "" ? 0 : $('#contractor_female').val();

    var res7 = parseFloat(i) + parseFloat(j);
    $('#total_female').val(res7);

}

function getTotal(btnObj) {

    var a = $('#direct_unskilled').val() == "" ? 0 : $('#direct_unskilled').val();
    var b = $('#direct_semiskilled').val() == "" ? 0 : $('#direct_semiskilled').val();
    var c = $('#direct_skilled').val() == "" ? 0 : $('#direct_skilled').val();

    var res = parseFloat(a) + parseFloat(b) + parseFloat(c);
    $('#direct_total').val(res);


    var d = $('#contractor_unskilled').val() == "" ? 0 : $('#contractor_unskilled').val();
    var e = $('#contractor_semiskilled').val() == "" ? 0 : $('#contractor_semiskilled').val();
    var f = $('#contractor_skilled').val() == "" ? 0 : $('#contractor_skilled').val();

    var res1 = parseFloat(d) + parseFloat(e) + parseFloat(f);
    $('#contractor_total').val(res1);

    var res2 = parseFloat(a) + parseFloat(d);
    $('#total_unskilled').val(res2);

    var res3 = parseFloat(b) + parseFloat(e);
    $('#total_semiskilled').val(res3);

    var res4 = parseFloat(c) + parseFloat(f);
    $('#total_skilled').val(res4);

    var res5 = parseFloat(res) + parseFloat(res1);
    $('#total_total').val(res5);


    var g = $('#direct_male').val() == "" ? 0 : $('#direct_male').val();
    var h = $('#contractor_male').val() == "" ? 0 : $('#contractor_male').val();

    var res6 = parseFloat(g) + parseFloat(h);
    $('#total_male').val(res6);

    var i = $('#direct_female').val() == "" ? 0 : $('#direct_female').val();
    var j = $('#contractor_female').val() == "" ? 0 : $('#contractor_female').val();

    var res7 = parseFloat(i) + parseFloat(j);
    $('#total_female').val(res7);

}

function getTotalWorker(id1, id2, id3) {
    var value1 = $('#' + id1).val();
    var value2 = $('#' + id2).val();
    var value3 = $('#' + id3);

    var a = value1 == "" ? 0 : value1;
    var b = value2 == "" ? 0 : value2;

    var res = parseFloat(a) + parseFloat(b);
    $(value3).val(res);
}

function regNoRenderer(moduleType, moduleId) {
    var pre = prefixModuleArray[moduleType] ? prefixModuleArray[moduleType] : '';
    return pre + ('00000' + moduleId).slice(-5);
}

function loadQueryDocItemForViewQuestion(queryDocumentData, mainCnt) {
    var tempCnt = 1;
    $.each(queryDocumentData, function (index, docData) {
        docData.cnt = tempCnt;
        $('#document_item_container_for_query_view_' + mainCnt).append(documentItemViewTemplate(docData));
        if (docData.document) {
            $('#document_name_href_for_query_answer_view_' + tempCnt).attr('href', 'documents/query/' + docData['document']);
            $('#document_name_for_query_answer_view_' + tempCnt).html(docData['document']);
        }
        tempCnt++;
    });
}

function loadQueryDocItemForView(queryDocumentData, mainCnt) {
    var tempCnt = 1;
    $.each(queryDocumentData, function (index, docData) {
        docData.cnt = tempCnt;
        $('#document_item_container_for_query_answer_view_' + mainCnt).append(documentItemViewTemplate(docData));
        if (docData.document) {
            $('#document_name_href_for_query_answer_view_' + tempCnt).attr('href', QUERY_PATH + docData['document']);
            $('#document_name_for_query_answer_view_' + tempCnt).html(docData['document']);
        }
        tempCnt++;
    });
}

function checkValidationForSubmitQueryDetails() {
    validationMessageHide();
    var moduleType = $('#module_type_for_query').val();
    if (moduleType != VALUE_ONE) {
        return invalidAccessValidationMessage;
    }
    var moduleId = $('#module_id_for_query').val();
    if (!moduleId) {
        return invalidAccessValidationMessage;
    }
    var queryType = $('#query_type_for_query').val();
    if (queryType != VALUE_ONE && queryType != VALUE_TWO) {
        return invalidAccessValidationMessage;
    }
    var remarks = $('#remarks_for_query').val();
    if (!remarks) {
        return remarksValidationMessage;
    }
    return '';
}

function askForSubmitQueryDetails() {
    if (!tempIdInSession || tempIdInSession == null) {
        loginPage();
        return false;
    }
    var validationMessage = checkValidationForSubmitQueryDetails();
    if (validationMessage != '') {
        $('#remarks_for_query').focus();
        validationMessageShow('query-remarks_for_query', validationMessage);
        return false;
    }
    var yesEvent = 'submitQueryDetails()';
    showConfirmation(yesEvent, 'Submit');
}

function getQDItems() {
    var newQDItems = [];
    var exiQDItems = [];
    var isQDItemValidation;
    $('.query_document_row').each(function () {
        var that = $(this);
        var tempCnt = that.find('.og_query_document_cnt').val();
        if (tempCnt == '' || tempCnt == null) {
            showError(invalidAccessMsg);
            isQDItemValidation = true;
            return false;
        }
        var qdItem = {};
        var docName = $('#doc_name_for_query_' + tempCnt).val();
        if (docName == '' || docName == null) {
            $('#doc_name_for_query_' + tempCnt).focus();
            validationMessageShow('query-doc_name_for_query_' + tempCnt, documentNameValidationMessage);
            isQDItemValidation = true;
            return false;
        }
        qdItem.doc_name = docName;
        if ($('#document_container_for_query_' + tempCnt).is(':visible')) {
            var uploadDoc = $('#document_for_query_' + tempCnt).val();
            if (!uploadDoc) {
                validationMessageShow('query-document_for_query_' + tempCnt, uploadDocValidationMessage);
                isQDItemValidation = true;
            }
            var uploadDocMessage = fileUploadValidation('document_for_query_' + tempCnt, 2048);
            if (uploadDocMessage != '') {
                validationMessageShow('query-document_for_query_' + tempCnt, uploadDocMessage);
                isQDItemValidation = true;
            }
        }

        var queryDocumentId = $('#query_document_id_for_query_' + tempCnt).val();
        if (!queryDocumentId || queryDocumentId == null) {
            newQDItems.push(qdItem);
        } else {
            qdItem.query_document_id = queryDocumentId;
            exiQDItems.push(qdItem);
        }
    });
    if (isQDItemValidation) {
        return false;
    }
    return {'new_qd_items': newQDItems, 'exi_qd_items': exiQDItems};
}

function submitQueryDetails() {
    if (!tempIdInSession || tempIdInSession == null) {
        loginPage();
        return false;
    }
    var validationMessage = checkValidationForSubmitQueryDetails();
    if (validationMessage != '') {
        $('#remarks_for_query').focus();
        validationMessageShow('query-remarks_for_query', validationMessage);
        return false;
    }
    var formData = {};
    formData.query_id_for_query = $('#query_id_for_query').val();
    formData.module_type_for_query = $('#module_type_for_query').val();
    formData.module_id_for_query = $('#module_id_for_query').val();
    formData.query_type_for_query = $('#query_type_for_query').val();
    formData.remarks_for_query = $('#remarks_for_query').val();
    formData.new_qd_items = [];
    formData.exi_qd_items = [];
    var qdItems = getQDItems();
    if (!qdItems) {
        return false;
    }
    formData.new_qd_items = qdItems.new_qd_items;
    formData.exi_qd_items = qdItems.exi_qd_items;
    var btnObj = $('#submit_btn_for_query');
    var ogBtnHTML = btnObj.html();
    var ogBtnOnclick = btnObj.attr('onclick');
    btnObj.html(iconSpinnerTemplate);
    btnObj.attr('onclick', '');
    $.ajax({
        type: 'POST',
        url: 'utility/raise_a_query',
        data: $.extend({}, formData, getTokenData()),
        error: function (textStatus, errorThrown) {
            generateNewCSRFToken();
            if (!textStatus.statusText) {
                loginPage();
                return false;
            }
            btnObj.html(ogBtnHTML);
            btnObj.attr('onclick', ogBtnOnclick);
            showError(textStatus.statusText);
            $('html, body').animate({scrollTop: '0px'}, 0);
        },
        success: function (response) {
            var parseData = JSON.parse(response);
            setNewToken(parseData.temp_token);
            if (parseData.success === false) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                showError(parseData.message);
                $('html, body').animate({scrollTop: '0px'}, 0);
                return false;
            }
            showSuccess(parseData.message);
            var tempData = {};
            tempData.remarks = formData.remarks_for_query;
            tempData.datetime_text = parseData.query_datetime;
            if (!jQuery.isEmptyObject(parseData.query_document_data)) {
                tempData.show_document_container = true;
            }
            tempData.cnt = 1;
            $('#query_container').html(queryQuestionViewTemplate(tempData));
            $('#query_status_' + formData.module_id_for_query).html(queryStatusArray[parseData.query_status]);
            loadQueryDocItemForViewQuestion(parseData.query_document_data, tempData.cnt);

            $('#reject_btn_for_app_' + formData.module_id_for_query).hide();
            $('#approve_btn_for_app_' + formData.module_id_for_query).hide();
        }
    });
}

function askForResolvedQuery(moduleType, moduleId) {
    if (!tempIdInSession || tempIdInSession == null) {
        loginPage();
        return false;
    }
    if (moduleType != VALUE_ONE) {
        showError(invalidAccessValidationMessage);
        return false;
    }
    if (!moduleId) {
        showError(invalidAccessValidationMessage);
        return false;
    }

    var yesEvent = 'resolvedQuery(' + moduleType + ',' + moduleId + ')';
    showConfirmation(yesEvent, 'Resolved Query');
}

function resolvedQuery(moduleType, moduleId) {
    if (!tempIdInSession || tempIdInSession == null) {
        loginPage();
        return false;
    }
    if (moduleType != VALUE_ONE) {
        showError(invalidAccessValidationMessage);
        return false;
    }
    if (!moduleId) {
        showError(invalidAccessValidationMessage);
        return false;
    }
    var btnObj = $('#resolved_btn_for_query');
    var ogBtnHTML = btnObj.html();
    var ogBtnOnclick = btnObj.attr('onclick');
    btnObj.html(iconSpinnerTemplate);
    btnObj.attr('onclick', '');
    $.ajax({
        type: 'POST',
        url: 'utility/resolved_query',
        data: $.extend({}, {'module_type': moduleType, 'module_id': moduleId}, getTokenData()),
        error: function (textStatus, errorThrown) {
            generateNewCSRFToken();
            if (!textStatus.statusText) {
                loginPage();
                return false;
            }
            btnObj.html(ogBtnHTML);
            btnObj.attr('onclick', ogBtnOnclick);
            showError(textStatus.statusText);
            $('html, body').animate({scrollTop: '0px'}, 0);
        },
        success: function (response) {
            var parseData = JSON.parse(response);
            setNewToken(parseData.temp_token);
            if (parseData.success === false) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                showError(parseData.message);
                $('html, body').animate({scrollTop: '0px'}, 0);
                return false;
            }
            showSuccess(parseData.message);
            $('#query_status_' + moduleId).html(queryStatusArray[parseData.query_status]);
            $('#popup_modal').modal('hide');
            if (parseData.status != VALUE_FIVE && parseData.status != VALUE_SIX &&
                    (parseData.query_status == VALUE_ZERO || parseData.query_status == VALUE_THREE)) {
                $('#reject_btn_for_app_' + moduleId).show();
            }
//            if (parseData.status == VALUE_SEVEN && (parseData.query_status == VALUE_ZERO || parseData.query_status == VALUE_THREE)) {
            if (moduleType == VALUE_TWENTYTHREE) {
                if (parseData.status == VALUE_SEVEN && (parseData.query_status == VALUE_ZERO || parseData.query_status == VALUE_THREE)) {
                    $('#approve_btn_for_app_' + moduleId).show();
                }
            } else {
                if (parseData.status != VALUE_FIVE && parseData.status != VALUE_SIX && (parseData.query_status == VALUE_ZERO || parseData.query_status == VALUE_THREE)) {
//                    $('#approve_btn_for_app_' + moduleId).show();
                    $('#approve_btn_for_app_' + moduleId).hide();
                }
            }
            $('#resolved_btn_for_query').remove();
        }
    });
}

function raiseAnotherQuery(moduleType, moduleId) {
    var templateData = {};
    templateData.datetime_text = '00-00-0000 00:00:00';
    templateData.query_type = VALUE_ONE;
    templateData.module_type = moduleType;
    templateData.module_id = moduleId;
    $('#query_item_container').prepend(queryQuestionTemplate(templateData));
    $('#raise_query_btn_for_query').hide();
}

function loadQueryManagementModule(parseData, templateData, tmpData) {
    documentRowCnt = 1;
    var moduleData = parseData.module_data;
    if ((moduleData.status != VALUE_FIVE && moduleData.status != VALUE_SIX) && (moduleData.query_status == VALUE_ONE || moduleData.query_status == VALUE_TWO)) {
        tmpData.show_resolve_query_btn = true;
    }
    if ((moduleData.status != VALUE_FIVE && moduleData.status != VALUE_SIX) && (moduleData.query_status == VALUE_TWO || moduleData.query_status == VALUE_THREE)) {
        tmpData.show_raise_query_btn = true;
    }
    $('#model_title').html('Query Management');
    $('#model_body').html(queryFormTemplate(tmpData));
    var cnt = 1;
    $.each(parseData.query_data, function (index, qd) {
        qd.cnt = cnt;
        qd.show_extra_div = true;
        qd.datetime_text = qd.display_datetime;
        if (qd.query_type == VALUE_ONE) {
            if (qd.status == VALUE_ONE) {
                if (!jQuery.isEmptyObject(qd.query_documents)) {
                    qd.show_document_container = true;
                }
                $('#query_item_container').prepend(queryQuestionViewTemplate(qd));
                loadQueryDocItemForViewQuestion(qd.query_documents, cnt);
            } else {
                qd.datetime_text = '00-00-0000 00:00:00';
                $('#query_item_container').prepend(queryQuestionTemplate(qd));
                $.each(qd.query_documents, function (index, docData) {
                    addDocumentRow(docData);
                });
                $('#raise_query_btn_for_query').hide();
            }
        }
        if (qd.query_type == VALUE_TWO) {
            if (qd.status == VALUE_ONE) {
                if (!jQuery.isEmptyObject(qd.query_documents)) {
                    qd.show_document_container = true;
                }
                $('#query_item_container').prepend(queryAnswerViewTemplate(qd));
                loadQueryDocItemForView(qd.query_documents, cnt);
            }
        }
        cnt++;
    });
    if (moduleData.status == VALUE_TWO || moduleData.status == VALUE_THREE || moduleData.status == VALUE_FOUR || moduleData.status == VALUE_SEVEN) {
        if (cnt == 1) {
            raiseAnotherQuery(templateData.module_type, templateData.module_id);
        }
    }
    $('#popup_modal').modal('show');
}

function addDocumentRow(templateData) {
    templateData.cnt = documentRowCnt;
    $('#document_item_container_for_query').append(documentItemTemplate(templateData));
    if (templateData.document) {
        loadQueryDocument('document', documentRowCnt, templateData);
    }
    resetCounter('query-document-cnt');
    documentRowCnt++;
}

function checkValidationForDocUpload() {
    validationMessageHide();
    var moduleType = $('#module_type_for_query').val();
    if (moduleType != VALUE_ONE) {
        return invalidAccessValidationMessage;
    }
    var moduleId = $('#module_id_for_query').val();
    if (!moduleId) {
        return invalidAccessValidationMessage;
    }
    var queryType = $('#query_type_for_query').val();
    if (moduleType != VALUE_ONE) {
        return invalidAccessValidationMessage;
    }
    return '';
}

function uploadDocumentForQuery(tempCnt) {
    var validationMessage = checkValidationForDocUpload();
    if (validationMessage != '') {
        showError(validationMessage);
        return false;
    }
    var id = 'document_for_query_' + tempCnt;
    var doc = $('#' + id).val();
    if (doc == '') {
        return false;
    }
    var materialslipMessage = fileUploadValidation(id, 2048);
    if (materialslipMessage != '') {
        showError(materialslipMessage);
        return false;
    }
    $('#document_container_for_query_' + tempCnt).hide();
    $('#document_name_container_for_query_' + tempCnt).hide();
    $('#spinner_template_for_query_' + tempCnt).show();
    var formData = new FormData();
    formData.append('query_id_for_query', $('#query_id_for_query').val());
    formData.append('module_type_for_query', $('#module_type_for_query').val());
    formData.append('module_id_for_query', $('#module_id_for_query').val());
    formData.append('query_type_for_query', $('#query_type_for_query').val());
    formData.append('query_document_id_for_query', $('#query_document_id_for_query_' + tempCnt).val());
    formData.append('document_for_query', $('#' + id)[0].files[0]);
    $.ajax({
        type: 'POST',
        url: 'utility/upload_query_document',
        data: formData,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        error: function (textStatus, errorThrown) {
            if (!textStatus.statusText) {
                loginPage();
                return false;
            }
            $('#spinner_template_for_query_' + tempCnt).hide();
            $('#document_container_for_query_' + tempCnt).show();
            $('#document_name_container_for_query_' + tempCnt).hide();
            $('#' + id).val('');
            showError(textStatus.statusText);
        },
        success: function (data) {
            var parseData = JSON.parse(data);
            if (parseData.success == false) {
                $('#spinner_template_for_query_' + tempCnt).hide();
                $('#document_container_for_query_' + tempCnt).show();
                $('#document_name_container_for_query_' + tempCnt).hide();
                $('#' + id).val('');
                showError(parseData.message);
                return false;
            }
            $('#spinner_template_for_query_' + tempCnt).hide();
            $('#document_name_container_for_query_' + tempCnt).hide();
            $('#' + id).val('');
            $('#query_id_for_query').val(parseData.query_id);
            $('#query_document_id_for_query_' + tempCnt).val(parseData.query_document_id);
            var docItemData = {};
            docItemData.query_document_id = parseData.query_document_id;
            docItemData.query_id = parseData.query_id;
            docItemData.document = parseData.document_name;
            loadQueryDocument('document', tempCnt, docItemData);
        }
    });
}

function loadQueryDocument(documentFieldName, cnt, docItemData) {
    $('#' + documentFieldName + '_container_for_query_' + cnt).hide();
    $('#' + documentFieldName + '_name_container_for_query_' + cnt).show();
    $('#' + documentFieldName + '_name_href_for_query_' + cnt).attr('href', 'documents/query/' + docItemData[documentFieldName]);
    $('#' + documentFieldName + '_name_for_query_' + cnt).html(docItemData[documentFieldName]);
    $('#' + documentFieldName + '_remove_btn_for_query_' + cnt).attr('onclick', 'askForRemoveDocumentRow("' + cnt + '")');
}


function askForRemoveDocumentRow(cnt) {
    var queryDocumentId = $('#query_document_id_for_query_' + cnt).val();
    if (!queryDocumentId || queryDocumentId == 0 || queryDocumentId == null) {
        removeDocumentItemRow(cnt);
        return false;
    }
    var yesEvent = 'removeDocumentRow(' + cnt + ')';
    showConfirmation(yesEvent, 'Remove');
}

function removeDocumentItemRow(cnt) {
    $('#query_document_row_' + cnt).remove();
    resetCounter('query-document-cnt');
}

function removeDocumentRow(cnt) {
    var queryDocumentId = $('#query_document_id_for_query_' + cnt).val();
    if (!queryDocumentId || queryDocumentId == 0 || queryDocumentId == null) {
        showError(invalidAccessValidationMessage);
        return false;
    }
    $.ajax({
        type: 'POST',
        url: 'utility/remove_query_document_item',
        data: $.extend({}, {'query_document_id': queryDocumentId}, getTokenData()),
        error: function (textStatus, errorThrown) {
            generateNewCSRFToken();
            showError(textStatus.statusText);
        },
        success: function (response) {
            var parseData = JSON.parse(response);
            setNewToken(parseData.temp_token);
            if (parseData.success === false) {
                showError(parseData.message);
                return false;
            }
            showSuccess(parseData.message);
            removeDocumentItemRow(cnt);
        }
    });
}

function showTableContainer(id) {
    $('#' + id + '_form_container').hide();
    $('#' + id + '_datatable_container').show();
}

function showFormContainer(id) {
    $('#' + id + '_datatable_container').hide();
    $('#' + id + '_form_container').show();
}

function askForConfirmPayment(moduleType, moduleId) {
    if (moduleType != VALUE_ONE) {
        showError(invalidAccessValidationMessage);
        return false;
    }
    if (!moduleId) {
        showError(invalidAccessValidationMessage);
        return false;
    }
    var yesEvent = 'confirmPayment(' + moduleType + ',' + moduleId + ')';
    showConfirmation(yesEvent, 'Confirm');
}

function confirmPayment(moduleType, moduleId) {
    if (moduleType != VALUE_ONE) {
        showError(invalidAccessValidationMessage);
        return false;
    }
    if (!moduleId) {
        showError(invalidAccessValidationMessage);
        return false;
    }
    var btnObj = $('#confirm_payment_btn_for_app_' + moduleId);
    var ogBtnHTML = btnObj.html();
    var ogBtnOnclick = btnObj.attr('onclick');
    btnObj.html(iconSpinnerTemplate);
    btnObj.attr('onclick', '');
    $.ajax({
        type: 'POST',
        url: 'utility/confirm_payment',
        data: $.extend({}, {'module_type': moduleType, 'module_id': moduleId}, getTokenData()),
        error: function (textStatus, errorThrown) {
            generateNewCSRFToken();
            if (!textStatus.statusText) {
                loginPage();
                return false;
            }
            btnObj.html(ogBtnHTML);
            btnObj.attr('onclick', ogBtnOnclick);
            showError(textStatus.statusText);
            $('html, body').animate({scrollTop: '0px'}, 0);
        },
        success: function (response) {
            var parseData = JSON.parse(response);
            setNewToken(parseData.temp_token);
            if (parseData.success === false) {
                btnObj.html(ogBtnHTML);
                btnObj.attr('onclick', ogBtnOnclick);
                showError(parseData.message);
                $('html, body').animate({scrollTop: '0px'}, 0);
                return false;
            }
//            $('#confirm_payment_btn_for_app_' + moduleId).remove();
            showSuccess(parseData.message);
            if (moduleType == VALUE_ONE) {
                FactoryInfo.listview.loadFactoryInfoData();
            }
//            $('#status_' + moduleId).html(appStatusArray[parseData.status]);
//            $('#approve_btn_for_app_' + moduleId).show();

        }
    });
}

var dateTimeRenderer = function (data, type, full, meta) {
    return data != '0000-00-00 00:00:00' ? dateTo_DD_MM_YYYY_HH_II_SS(data) : '-';
};

function fileUploadValidation(imageUploadAttrId, size = 1024) {
    var allowedFiles = ['jpg', 'png', 'jpeg', 'jfif', 'pdf', 'zip'];
    var fileName = $('#' + imageUploadAttrId).val();
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    if ($.inArray(ext, allowedFiles) == -1) {
        $('#' + imageUploadAttrId).val('');
        $('#' + imageUploadAttrId).focus();
        return 'Please upload File having extensions: <b>' + allowedFiles.join(', ') + '</b> only.';
    }
    if (($('#' + imageUploadAttrId)[0].files[0].size / 1024) > size) {
        return 'Maximum upload size ' + (size / 1024) + ' MB only.';
    }
    return false;
}

function imagefileUploadValidation(imageUploadAttrId, size = 1024) {
    var allowedFiles = ['jpg', 'png', 'jpeg', 'jfif'];
    var fileName = $('#' + imageUploadAttrId).val();
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    if ($.inArray(ext, allowedFiles) == -1) {
        $('#' + imageUploadAttrId).val('');
        $('#' + imageUploadAttrId).focus();
        return 'Please upload File having extensions: <b>' + allowedFiles.join(', ') + '</b> only.';
    }
    if (($('#' + imageUploadAttrId)[0].files[0].size / 1024) > size) {
        return 'Maximum upload size ' + (size / 1024) + ' MB only.';
    }
    return false;
}

function pdffileUploadValidation(imageUploadAttrId, size = 1024) {
    var allowedFiles = ['pdf', 'zip'];
    var fileName = $('#' + imageUploadAttrId).val();
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    if ($.inArray(ext, allowedFiles) == -1) {
        $('#' + imageUploadAttrId).val('');
        $('#' + imageUploadAttrId).focus();
        return 'Please upload File having extensions: <b>' + allowedFiles.join(', ') + '</b> only.';
    }
    if (($('#' + imageUploadAttrId)[0].files[0].size / 1024) > size) {
        return 'Maximum upload size ' + (size / 1024) + ' MB only.';
    }
    return false;
}

function removeDocumentValue(containerHideId, documentSrcPathId, containerShowId, documentId) {
    $('#' + containerHideId).hide();
    $('#' + documentSrcPathId).attr('src', '');
    $('#' + containerShowId).show();
    $('#' + documentId).show();
    $('#' + documentId).val('');
}

function checkValidationForDocument(documentId, documentCategory, errorMsgName, pdfSize = 1024, imageSize = 1024) {
    // VALUE_ONE for PDF | ZIP
    if (documentCategory == VALUE_ONE) {
        var documentName = $('#' + documentId).val();
        if (documentName == '') {
            $('#' + documentId).focus();
            validationMessageShow(errorMsgName + '-' + documentId, uploadDocumentValidationMessage);
            return false;
        }
        var documentNameMessage = pdffileUploadValidation(documentId, pdfSize);
        if (documentNameMessage != '') {
            $('#' + documentId).focus();
            validationMessageShow(errorMsgName + '-' + documentId, documentNameMessage);
            return false;
        }
    }
    // VALUE_TWO for IMAGE
    if (documentCategory == VALUE_TWO) {
        var documentName = $('#' + documentId).val();
        if (documentName == '') {
            $('#' + documentId).focus();
            validationMessageShow(errorMsgName + '-' + documentId, uploadDocumentValidationMessage);
            return false;
        }
        var documentNameMessage = imagefileUploadValidation(documentId, imageSize);
        if (documentNameMessage != '') {
            $('#' + documentId).focus();
            validationMessageShow(errorMsgName + '-' + documentId, documentNameMessage);
            return false;
        }
}
}

function dateTo_DD_MM_YYYY_HH_II_SS(date, delimeter) {
    var delim = delimeter ? delimeter : '-';
    var d = new Date(date || Date.now()),
            month = d.getMonth() + 1,
            day = '' + d.getDate(),
            year = d.getFullYear();
    if (month < 10)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    var hours = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();
    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    return [day, month, year].join(delim) + ' ' + hours + ':' + minutes + ':' + seconds;
}

function fileUploadValidationForPDF(imageUploadAttrId, maxFileSize) {
    var allowedFiles = ['pdf'];
    var fileName = $('#' + imageUploadAttrId).val();
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    if ($.inArray(ext, allowedFiles) == -1) {
        $('#' + imageUploadAttrId).val('');
        $('#' + imageUploadAttrId).focus();
        return 'Please upload File having extensions: <b>' + allowedFiles.join(', ') + '</b> only.';
    }
    if (($('#' + imageUploadAttrId)[0].files[0].size / 1024) > maxFileSize) {
        return 'Maximum upload size ' + (maxFileSize / 1024) + ' MB only.';
    }
    return false;
}

function validateAadhar(aadharNumber) {

    if (!aadharNumber) {
        return '';
    }
    var reg = /[2-9]{1}[0-9]{11}$/;
    if (!reg.test(aadharNumber)) {
        return 'Invalid Aadhar Number.';
    }
    return '';
}
function checkIfscCode(obj) {
    var ifscCode = obj.val();
    var ifscCodeValidationMessage = validateIfsc(ifscCode);
    if (ifscCodeValidationMessage != '') {
        showError(ifscCodeValidationMessage);
        return false;
    }
}
function validateIfsc(ifscCode) {
    if (!ifscCode) {
        return '';
    }
    var reg = /[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$/;
    if (!reg.test(ifscCode)) {
        return 'Invalid IFSC Code.';
    }
    return '';
}

function ReturnAge(dob, birthdateId) {
    if (!dob) {
        $('#' + birthdateId).val('');
    } else {
        var age = calculateAge(parseDate(dob.val()), new Date());
        if (!age)
        {
            age = '0';
        }
        $('#' + birthdateId).val(age);
    }

}

function calculateAge(dateOfBirth, dateToCalculate) {
    var calculateYear = dateToCalculate.getFullYear();
    var calculateMonth = dateToCalculate.getMonth();
    var calculateDay = dateToCalculate.getDate();

    var birthYear = dateOfBirth.getFullYear();
    var birthMonth = dateOfBirth.getMonth();
    var birthDay = dateOfBirth.getDate();

    var age = calculateYear - birthYear;
    var ageMonth = calculateMonth - birthMonth;
    var ageDay = calculateDay - birthDay;

    if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
        age = parseInt(age) - 1;
    }
    return age;
}

function parseDate(dateStr) {
    var dateParts = dateStr.split("-");
    return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
}

