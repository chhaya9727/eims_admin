var guardDutyHistoryListTemplate = Handlebars.compile($('#guard_duty_history_list_template').html());
var guardDutyHistoryTableTemplate = Handlebars.compile($('#guard_duty_history_table_template').html());

var GuardDutyHistory = {
    run: function () {
        this.router = new this.Router();
        this.listview = new this.listView();
    }
};
GuardDutyHistory.Router = Backbone.Router.extend({
    routes: {
        'guarddutyhistory': 'renderList',
    },
    renderList: function () {
        GuardDutyHistory.listview.listPage();
    },
});
GuardDutyHistory.listView = Backbone.View.extend({
    el: 'div#main_container',
    listPage: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }

        activeLink('menu_guard_duty_history');
        addClass('menu_guarddutyhistory', 'active');
        GuardDutyHistory.router.navigate('guarddutyhistory');
        var templateData = {};
        this.$el.html(guardDutyHistoryListTemplate(templateData));
        this.loadGuardDutyHistoryData();

    },
    loadGuardDutyHistoryData: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        if (tempTypeInSession != TEMP_TYPE_A && tempTypeInSession != TEMP_TYPE_EXCISE_DEPT_USER) {
            Dashboard.router.navigate('dashboard', {trigger: true});
            return false;
        }
        var historyDate = $('#history_date').val();
        if (historyDate == '') {
            showError(dateValidationMessage);
            return false;
        }
        if (historyDate == undefined) {
            historyDate = '00-0000';
        }

        var firstShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.first_shift_guard_name != undefined) {
                if (full.first_shift_guard_name == VALUE_ZERO) {
                    full.first_shift_guard_name = '-';
                }
                var first_shift_guard_name = full.first_shift_guard_name.replace(/,\s*$/, "");
                return first_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        var secondShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.second_shift_guard_name != undefined) {
                if (full.second_shift_guard_name == VALUE_ZERO) {
                    full.second_shift_guard_name = '-';
                }
                var second_shift_guard_name = full.second_shift_guard_name.replace(/,\s*$/, "");
                return  second_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        var nightShiftGuardNameRenderer = function (data, type, full, meta) {
            if (full.night_shift_guard_name != undefined) {
                if (full.night_shift_guard_name == VALUE_ZERO) {
                    full.night_shift_guard_name = '-';
                }
                var night_shift_guard_name = full.night_shift_guard_name.replace(/,\s*$/, "");
                return night_shift_guard_name.replace(/,/g, '<hr>');
            }
            return '';
        };
        var that = this;
        showTableContainer('guard_duty');
        GuardDutyHistory.router.navigate('guarddutyhistory');
        $('#guard_duty_history_datatable_container').html(guardDutyHistoryTableTemplate());
        if (tempTypeInSession == TEMP_TYPE_A || tempTypeInSession == TEMP_TYPE_EXCISE_DEPT_USER) {
            var tempStringDuty = '';
            var tempGuardName = '';

//            var office_one = 'Shri. Ankit Patel:<br/>1. Inward/ Outward of letters<br/>2. All permit related process<br/>3. Brand approval process<br/>Any other assigned work';
//            var office_two = 'Shri. Amit Rajbhar:<br/>1. Maintenance of Crime Register<br/>2. Process of cases<br/>3. Record of seized vehicles and Liquor<br/>Any other assigned work';

            guardDutyHistoryDataTable = $('#guard_duty_history_datatable').DataTable({
                ajax: {url: 'guard_duty/get_guard_duty_history_data', dataSrc: "guard_duty_history_data", type: "post", data: $.extend({}, {'history_date': historyDate}, getTokenData())},
                bAutoWidth: false,
                ordering: false,
                processing: true,
                paging: false,
                language: dataTableProcessingAndNoDataMsg,
                serverSide: false,
                bInfo: false,
                columns: [
                    {data: '', 'render': serialNumberRenderer, 'class': 'text-center'},
                    {data: 'name', 'class': 'text-center'},
                    {data: 'first_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': firstShiftGuardNameRenderer},
                    {data: 'second_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': secondShiftGuardNameRenderer},
                    {data: 'night_shift_guard_name', 'class': 'text-center', "defaultContent": '-', 'render': nightShiftGuardNameRenderer},
                    {data: 'week_from', 'class': 'text-center'},
                    {data: 'week_to', 'class': 'text-center'},
                ],
                createdRow: function (row, data, dataIndex) {
                    // If shift is "General Shift"
                    if (data['general_shift_guard_id'] != '' && data['general_shift_guard_id'] != '-' && data['general_shift_guard_id'] != 'undefined' && data['general_shift_guard_id'] != null) {
                        // Add COLSPAN attribute
                        $('td:eq(2)', row).attr('colspan', 3);
                        // Center horizontally
                        $('td:eq(2)', row).attr('align', 'center');
                        // Hide required number of columns
                        // next to the cell with COLSPAN attribute
                        $('td:eq(3)', row).css('display', 'none');
                        $('td:eq(4)', row).css('display', 'none');
                        // Update cell data
                        this.api().cell($('td:eq(2)', row)).data(data['general_shift_guard_name']);
                    }
                },
                "rowCallback": function (row, data) {

                    if (data.reserve_shift_guard_name != VALUE_ZERO && data.reserve_shift_guard_name != '-' && data.reserve_shift_guard_name != null) {
                        tempGuardName += data.reserve_shift_guard_name + '<br/>';
                    }
                    tempStringDuty = '<tr><td class="text-center">' + tempGuardName + '</td><td class="text-center">' + data.week_from + '</td><td class="text-center">' + data.week_to + '</td></tr>';

                },
                "initComplete": function (settings, json) {
                    $('#reserve_guard_list').html(tempStringDuty);
                }
            });
        }
        $('#guard_duty_history_datatable_filter').remove();
        datePicker();

    },
    generateReport: function (btnObj, weekFrom, weekTo, tblID) {
        if (!weekFrom || !weekTo || !tblID) {
            showError(invalidAccessValidationMessage);
            return false;
        }
        $('#week_from_for_report').val(weekFrom);
        $('#week_to_for_report').val(weekTo);
        $('#guard_duty_history_report_pdf_form').submit();
    },
});