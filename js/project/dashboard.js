var dashboardListTemplate = Handlebars.compile($('#dashboard_list_template').html());
var Dashboard = {
    run: function () {
        this.router = new this.Router();
        this.listview = new this.listView();
    }
};
Dashboard.Router = Backbone.Router.extend({
    routes: {
        '': 'renderList',
        'dashboard': 'renderList'
    },
    renderList: function () {
        Dashboard.listview.listPage();
    },
    renderListForURLChange: function () {
        Dashboard.listview.listPage();
    }
});
Dashboard.listView = Backbone.View.extend({
    el: 'div#main_container',
    listPage: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        Dashboard.router.navigate('dashboard');
        activeLink('menu_dashboard');
        var templateData = {};
        this.$el.html(dashboardListTemplate(templateData));
        datePicker();
        // this.loadDashboardData();
    },
    loadDashboardData: function () {
        if (!tempIdInSession || tempIdInSession == null) {
            loginPage();
            return false;
        }
        $('.zero_value_dashboard_spe_report').html(0);
        $.ajax({
            url: 'main/get_dashboard_data',
            type: 'post',
            data: getTokenData(),
            error: function (textStatus, errorThrown) {
                $('.null-value').html(0);
                generateNewCSRFToken();
                showError(textStatus.statusText);
                $('html, body').animate({scrollTop: '0px'}, 0);
            },
            success: function (response) {
                var parseData = JSON.parse(response);
                setNewToken(parseData.temp_token);
                $.each(parseData.zone_wise_industries, function (id, idValue) {
                    $('#' + id).html(idValue);
                });
                $.each(parseData.employee, function (id, idValue) {
                    $('#' + id).html(idValue);
                });
            }
        });
    }
});
