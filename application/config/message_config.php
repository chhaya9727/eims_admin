<?php

define('INVALID_ACCESS', 1);
define('INVALID_ACCESS_MESSAGE', 'Invalid Access !');

$config['invalid_access_array'] = array(
    INVALID_ACCESS => INVALID_ACCESS_MESSAGE
);

define('PASSWORD_VALIDATION_MESSAGE', '1. Password must be between 8 to 16 characters long.<br>'
        . '2. Contain at least one digit and two alphabetic character.<br>'
        . '3. At least one upper case and one lower case character.<br>'
        . '4. Contain at least one special character of (!#$@%-_+<>=).');

define('DATABASE_ERROR_MESSAGE', 'Some unexpected database error encountered due to which your transaction could not be complete');
define('INVALID_USER_MESSAGE', 'Invalid User !');
define('INVALID_PASSWORD_MESSAGE', 'Invalid Password !');
define('RETYPE_PASSWORD_MESSAGE', 'Retype your Password !');
define('NEW_PASSWORD_MESSAGE', 'Enter New Password !');
define('PASSWORD_AND_RETYPE_PASSWORD_NOT_MATCH_MESSAGE', 'Password and Retype password do not match !');
define('USERNAME_MESSAGE', 'Enter Username !');
define('PASSWORD_MESSAGE', 'Enter Password !');
define('USER_EXISTS_MESSAGE', 'User Already Exists !');
define('PASSWORD_POLICY_MESSAGE', 'Password entered is not as per password policy !');
define('NO_RECORD_FOUND_MESSAGE', 'No Record Found..!');
define('APPROVE_MESSAGE', 'Approved Successfully !');
define('APP_DRAFT_MESSAGE', 'Application Save as Draft Successfully  !');
define('APP_SUBMITTED_MESSAGE', 'Data Submitted Successfully  !');
define('ONE_CONTRACTOR_MESSAGE', 'Enter Atleast One Contractor Details!');
define('EMAIL_MESSAGE', 'Enter Email !');
define('INVALID_EMAIL_MESSAGE', 'Invalid Email !');
define('DATE_MESSAGE', 'Select Date !');
define('MOBILE_NUMBER_MESSAGE', 'Enter Mobile Number !');
define('INVALID_MOBILE_NUMBER_MESSAGE', 'Invalid Mobile Number !');
define('CHALLAN_UPLOADED_MESSAGE', 'Challan Send Successfully !');
define('PAYMENT_CONFIRMED_MESSAGE', 'Payment Confirmed Successfully !');
define('LICENSE_NO_NOT_AVAILABLE', 'Invalid License Number !');
define('REGISTRATION_NUMBER_EXISTS_MESSAGE', 'Registration Number Already Exists !');
define('REGISTRATION_FILE_NO_MESSAGE', 'Enter Only File No. !');
define('ONE_PAYMENT_OPTION_MESSAGE', 'Select One Payment Option !');
define('INVALID_AADHAR_MESSAGE', 'Invalid Aadhar Number !');
define('AADHAR_MESSAGE', 'Enter Aadhar Number !');
define('EXISTS_AADHAR_MESSAGE', 'Aadhar Number Already Exists !');
define('UPLOAD_DOCUMENT_MESSAGE', 'Upload Valid Document !');
define('CATEGORY_MESSAGE', "Select Category !");
define('VALUE_MESSAGE', 'Enter Value !');
define('SAVED_MESSAGE', 'Saved Successfully  !');
define('INVALID_ID_MESSAGE', 'Select Valid ID !');

//Login
define('INVALID_USERNAME_OR_PASSWORD_MESSAGE', 'Username or Password is Invalid !');
define('EMAIL_NOT_VERIFY_MESSAGE', 'Your email is not verified. Please verify your email !');
define('MOBILE_NUMBER_NOT_VERIFY_MESSAGE', 'Your mobile number is not verified. Please verify your mobile number !');
define('ACCOUNT_NOT_ACTIVE_MESSAGE', 'Permission Denied !');
define('ACCOUNT_DELETE_MESSAGE', 'Your Account is Disabled. Please contect to System Administration !');

//User Type
define('INVALID_USER_TYPE_MESSAGE', 'Invalid User Type !');
define('USER_TYPE_MESSAGE', 'Enter User Type !');
define('USER_TYPE_EXISTS_MESSAGE', 'User Type Already Exists !');
define('USER_TYPE_SAVED_MESSAGE', 'User Type Saved Successfully !');
define('USER_TYPE_UPDATED_MESSAGE', 'User Type Updated Successfully !');

//User Module
define('NAME_MESSAGE', 'Enter Name !');
define('SELECT_USER_TYPE_MESSAGE', 'Select User Type !');
define('SELECT_USER_MESSAGE', 'Select User !');
define('USER_SAVED_MESSSAGE', 'User Saved Successfully !');
define('USER_UPDATED_MESSSAGE', 'User Updated Successfully !');

//Change Password Module
define('PASSWORD_CHANGED_MESSAGE', 'Password Changed Successfully !');
define('CURRENT_NEW_PASSWORD_SAME_MESSAGE', 'Your Current Password and New Password are Same. Please Enter Another Password !');
define('INCORRECT_CURRENT_PASSWORD', 'Incorrect Current Password !');

//Department Messages
define('DEPARTMENT_MESSAGE', 'Enter Department Name !');
define('SELECT_DEPARTMENT_MESSAGE', 'Select Department !');
define('INVALID_DEPARTMENT_MESSAGE', 'Invalid Department !');
define('DEPARTMENT_EXISTS_MESSAGE', 'Department Name Already Exists !');
define('DEPARTMENT_SAVED_MESSAGE', 'Department Saved Successfully !');
define('DEPARTMENT_UPDATED_MESSAGE', 'Department Updated Successfully !');

//Checkpost / Distillery Messages
define('CHECKPOST_NAME_MESSAGE', 'Enter Checkpost / Distillery Name !');
define('CHECKPOST_ADDRESS_MESSAGE', 'Enter Checkpost / Distillery Address !');
define('NODEL_PERSONE_NAME_MESSAGE', 'Enter Nodel Persone Name !');
define('EXCISE_GUARD_NO_MESSAGE', 'Enter No. of Excise Guard !');

//Query Management Module
define('REMARKS_MESSAGE', 'Enter Remarks !');
define('QUERY_RAISED_MESSAGE', 'Query Raised Successfully !');
define('QUERY_RESOLVED_MESSAGE', 'Query Resolved Successfully !');
define('DOCUMENT_NAME_MESSAGE', 'Enter Document Name !');
define('QUERY_DOCUMENT_ITEM_REMOVED_MESSAGE', 'Query Document Item Removed Successfully !');

// Guard
define('DESIGANTION_MESSAGE', 'Enter Designation !');
define('SELECT_OPTION_MESSAGE', 'Select one Option !');

