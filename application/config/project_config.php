<?php

define('IS_CHECKED_NO', 0);
define('IS_CHECKED_YES', 1);
define('IS_DELETE', 1);
define('IS_ACTIVE', 0);
define('IS_DEACTIVE', 1);
define('IS_VERIFY', 1);

define('LOGIN', 1);
define('LOGOUT', 2);

$config['log_type'] = array(
    LOGIN => 'Login',
    LOGOUT => 'Logout'
);

$config['account_type_array'] = array(
    IS_ACTIVE => 'Activated',
    IS_DEACTIVE => 'Deactivated'
);

define("ENCRYPTION_KEY", "!@#$%^&*");

define('PASSWORD_REGEX', '/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!#$@%_+\-=<>]).{8,16}$/');


define('API_ENCRYPTION_KEY', 'sgAD#@$@^^&fAB%^*(*&&^%$');
define('API_ACCESS_KEY', '%#d@AE$#Idgqw$$^jhhh');


// Logs Table
define('TBL_LOGS_LOGIN_LOGOUT', 'sa_logs_login_details');
define('TBL_LOGS_LOGIN_LOGOUT_PRIMARY_KEY', 'sa_logs_login_details_id');
define('TBL_LOGS_CHANGE_PASSWORD', 'sa_logs_change_password');
define('TBL_LOGS_API', 'sa_logs_api');
define('VIEW_UPLODED_DOCUMENT', 'View Uploaded Document');

define('DEFAULT_PASSWORD', 'Admin@1819');

define('FROM_NAME', 'noreply@safe.in');
define('FROM_EMAIL', 'noreply@safe.in');

define('TEMP_TYPE_A', 1);
define('TEMP_TYPE_EXCISE_DEPT_USER', 2);
define('TEMP_TYPE_SEC_USER', 3);

define('VERSION', 'v=1.1.3');

//define('GS_PATH', "C:\Program Files\gs\gs9.55.0\bin\gswin64c");
define('GS_PATH', "gs");
define('PROJECT_PATH', 'https://eims.nicdemo.in/');
//define('PROJECT_PATH', 'http://localhost:90/eims_admin/');
define('DOC_PATH', PROJECT_PATH . 'documents/');

define('VALUE_ZERO', 0);
define('VALUE_ONE', 1);
define('VALUE_TWO', 2);
define('VALUE_THREE', 3);
define('VALUE_FOUR', 4);
define('VALUE_FIVE', 5);
define('VALUE_SIX', 6);
define('VALUE_SEVEN', 7);
define('VALUE_EIGHT', 8);
define('VALUE_NINE', 9);
define('VALUE_TEN', 10);

$config['yes_no_type_array'] = array(
    VALUE_ONE => 'Yes',
    VALUE_TWO => 'No',
);

define('TALUKA_DAMAN', 1);
define('TALUKA_DIU', 2);
define('TALUKA_DNH', 3);


$config['taluka_array'] = array(
    TALUKA_DAMAN => 'Daman',
    TALUKA_DIU => 'Diu',
    TALUKA_DNH => 'DNH'
);


define('MAX_FILE_SIZE_IN_KB', 100);
define('MAX_FILE_SIZE_IN_MB', 5);

$config['query_module_array'] = array(
    VALUE_ONE => array(
        'title' => 'FI',
        'key_id_text' => 'factoryinfo_id',
        'tbl_text' => 'factoryinfo'),
    VALUE_TWO => array(
        'title' => 'GUARD',
        'key_id_text' => 'guard_id',
        'tbl_text' => 'guard'),
);

$config['prefix_module_array'] = array(
    VALUE_ONE => 'FI',
    VALUE_TWO => 'G',
);

$config['shifts_array'] = array(
    VALUE_ONE => 'General',
    VALUE_TWO => 'First',
    VALUE_THREE => 'Second',
    VALUE_FOUR => 'Night',
);

$config['shifts_category_array'] = array(
    VALUE_ZERO => 'First / Second / Night',
    VALUE_ONE => 'General',
);
