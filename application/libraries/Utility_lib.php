<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Utility_lib {

    var $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('utility_model');
    }

    function login_log($user_id) {
        $logs_data = array();
        $logs_data['sa_user_id'] = $user_id;
        $logs_data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $logs_data['login_timestamp'] = time();
        $logs_data['logs_data'] = json_encode($this->_get_client_info());
        $logs_data['created_time'] = date('Y-m-d H:i:s');
        return $this->CI->logs_model->insert_log(TBL_LOGS_LOGIN_LOGOUT, $logs_data);
    }

    function logout_log($log_id) {
        $logs_data = array();
        $logs_data['logout_timestamp'] = time();
        $logs_data['updated_time'] = date('Y-m-d H:i:s');
        return $this->CI->logs_model->update_log(TBL_LOGS_LOGIN_LOGOUT, TBL_LOGS_LOGIN_LOGOUT_PRIMARY_KEY, $log_id, $logs_data);
    }

    function _get_client_info() {
        return array(
            'HTTP_USER_AGENT' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
            'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR']
        );
    }

function check_post_id_validation($key_post_id, $post_id, $table_name) {
        if ($post_id == '' || $post_id == 0) {
            return "Please select any $table_name";
        }
        $is_valid = $this->check_post_data_of_entity($key_post_id, $post_id, $table_name);
        if (!$is_valid) {
            return "This $table_name does not exist.";
        }
        return '';
    }
    
    function check_post_data_of_entity($key_post_id, $post_id, $table_name) {
        $post_data = $this->CI->utility_model->is_valid_post_data($key_post_id, $post_id, $table_name);
        if (!empty($post_data)) {
            return TRUE;
        }
        return FALSE;
    }
    
    function send_sms_and_email_for_app_approve($user_id, $sms_email_type, $module_type, $module_id) {
        $ex_user_data = $this->CI->utility_model->get_by_id('user_id', $user_id, 'users');
        $prefix_module_array = $this->CI->config->item('prefix_module_array');
        $registration_message = 'Your Application Number : ' . generate_registration_number($prefix_module_array[$module_type], $module_id) . ' is Approved !';
        $this->CI->load->helper('sms_helper');
        send_SMS($this, $user_id, $ex_user_data['mobile_number'], $registration_message, $sms_email_type);
        $this->CI->load->library('email_lib');
        $this->CI->email_lib->send_email($ex_user_data, 'Application Approved', $registration_message, $sms_email_type, $module_type, $module_id);
    }

    function send_sms_and_email_for_app_reject($user_id, $sms_email_type, $module_type, $module_id) {
        $ex_user_data = $this->CI->utility_model->get_by_id('user_id', $user_id, 'users');
        $prefix_module_array = $this->CI->config->item('prefix_module_array');
        $registration_message = 'Your Application Number : ' . generate_registration_number($prefix_module_array[$module_type], $module_id) . ' is Rejected !';
        $this->CI->load->helper('sms_helper');
        send_SMS($this, $user_id, $ex_user_data['mobile_number'], $registration_message, $sms_email_type);
        $this->CI->load->library('email_lib');
        $this->CI->email_lib->send_email($ex_user_data, 'Application Rejected', $registration_message, $sms_email_type, $module_type, $module_id);
    }
}

/**
 * EOF: ./application/libraries/Email_lib.php
 */