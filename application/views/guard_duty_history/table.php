<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            <!-- <form role="form" id="guard_history_form" name="guard_history_form" onsubmit="return false;"> -->
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label>Date<span style="color: red;font-size: 16px;">*</span></label>
                        <div class="input-group date">
                            <input type="text" name="history_date" id="history_date" class="form-control date_picker"
                                   placeholder="mm-yyyy" data-date-format="MM-YYYY"
                                   value="{{history_date}}"
                                   onblur="checkValidation('guard-history', 'history_date', dateValidationMessage);">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="far fa-calendar"></i></span>
                            </div>
                        </div>
                        <span class="error-message error-message-guard-history-history_date"></span>
                    </div>
                    <div class="form-group col-sm-6" style="margin-top: 27px;">
                        <button type="button" class="btn btn-sm btn-primary guardhistory" onclick="GuardDutyHistory.listview.loadGuardDutyHistoryData();">Submit</button>
                    </div>
                </div>
            <!-- </form> -->
        </h3>
    </div>
    <div class="card-body" id="guard_duty_history_datatable_container">
        <div class="row">
            <div class="form-group col-sm-12">
                <div class="table-responsive">
                    <table id="guard_duty_history_datatable" class="table table-bordered table-hover">
                        <thead>
                            <tr class="bg-light-gray">
                                <th class="text-center" style="width: 30px;">No.</th>
                                <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                <th class="text-center" style="min-width: 80px;">From Date</th>
                                <th class="text-center" style="min-width: 80px;">To Date</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered table-hover">
                    <thead>
                            <!--<th class="text-center">Office</th>-->
                            <th class="text-center">Reserve Guards</th>
                            <th class="text-center">From Date</th>
                            <th class="text-center">To Date</th>

                    </thead>
                    <tbody id="reserve_guard_list">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>