<div class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="m-0 text-dark"><i class="nav-icon fa fa-buildings"></i> Guard Duty History</h1>
            </div>
            <div class="col-sm-4">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="main#dashboard">Home</a></li>
                    <li class="breadcrumb-item active">Guard Duty History</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid" id="guard_duty_history_datatable_container">
    </div>
    <div class="container-fluid" id="guard_duty_history_form_container" style="display: none;">
    </div>
</section>
 <form target="_blank" id="guard_duty_history_report_pdf_form" action="guard_duty/generate_report" method="post">
    <input type="hidden" id="week_from_for_report" name="week_from_for_report">
    <input type="hidden" id="week_to_for_report" name="week_to_for_report">
</form>