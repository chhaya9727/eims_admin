<html>
    <head>
        <title><?php echo isset($title) ? $title : ''; ?></title>
        <style type="text/css">
            @page {
                margin: 20px;
            }
            body {
		font-family: arial_unicode_ms;
                font-size: 14px;
            }
            .page-border{
                /*border: 2px solid red;*/
                padding: 10px;
                height: 100%;
            }
            .table-header {
                width: 100%;
            }
            .header-title {
                font-weight: bold;
                font-size: 15px;
            }
            .f-w-b{
                font-weight: bold;
            }
            .color-nic-blue{
                color: black;
            }
            .footer-title{
                font-size: 15px;
            }
            .t-a-c{
                text-align: center;
            }
            .f-aum{
                font-family: arial_unicode_ms;
            }
        </style>
    </head>
    <body>
    <watermarkimage src="images/ee-logo.png" alpha="0.03" />
    <div class="page-border">
        <table class="table-header">
            <tr>

            </tr>
            <tr>
                <td style="width: 120px;text-align: center;">
                    <img src="images/ddd-logo.png" style="width: 90px;"> 
                </td>
                <td style="vertical-align: top;text-align: center;">
                    <div class="header-title f-aum">संघ प्रदेश दादरा एवं नगर हवेली और दमण एवं दीव प्रशासन,</div>
                    <div class="header-title">U.T. Administration of Dadra & Nagar Haveli and Daman &Diu,</div>
                    <div class="header-title f-aum">आबकारी सहायक का कार्यालय, आबकारी विभाग, दमन</div>
                    <div class="header-title">O/o. The Assistant Commissioner of Excise, Excise Department, Daman</div>
                    <div class="header-title"><?php echo isset($department_name) ? $department_name : ''; ?></div>
                    <div class="header-title"><?php echo isset($district) ? $district : ''; ?></div>
                </td>
                <td style="width: 120px;text-align: center;">
                    <img src="images/ee-logo.png" style="width: 90px;"> 
                </td>
<!--                <td style="text-align: center;">
                    <img src="images/ddd.png" style="width: 90px;"> 
                </td>-->
            </tr>
        </table>
        <hr style="color: black;height: 2px;">
        <div style="margin-top: 10px; <?php echo $page_size == 'A4' ? 'height: 79.9%' : ($page_size == 'Legal' ? 'height: 83.2%' : ''); ?>">