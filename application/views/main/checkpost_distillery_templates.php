<script type="text/x-handlebars-template" id="checkpost_distillery_list_template">
<?php $this->load->view('checkpost_distillery/list'); ?>
</script>
<script type="text/x-handlebars-template" id="checkpost_distillery_table_template">
<?php $this->load->view('checkpost_distillery/table'); ?>
</script>
<script type="text/x-handlebars-template" id="checkpost_distillery_action_template">
<?php $this->load->view('checkpost_distillery/action'); ?>
</script>
<script type="text/x-handlebars-template" id="checkpost_distillery_form_template">
<?php $this->load->view('checkpost_distillery/form'); ?>
</script>
<script type="text/x-handlebars-template" id="checkpost_distillery_view_template">
    <?php $this->load->view('checkpost_distillery/view'); ?>
</script>
