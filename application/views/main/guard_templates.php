<script type="text/x-handlebars-template" id="guard_list_template">
<?php $this->load->view('guard/list'); ?>
</script>
<script type="text/x-handlebars-template" id="guard_table_template">
<?php $this->load->view('guard/table'); ?>
</script>
<script type="text/x-handlebars-template" id="guard_action_template">
<?php $this->load->view('guard/action'); ?>
</script>
<script type="text/x-handlebars-template" id="guard_form_template">
<?php $this->load->view('guard/form'); ?>
</script>
<script type="text/x-handlebars-template" id="guard_view_template">
<?php $this->load->view('guard/view'); ?>
</script>
<script type="text/x-handlebars-template" id="guard_approve_template">
<?php $this->load->view('guard/approve'); ?>
</script>
<script type="text/x-handlebars-template" id="guard_reject_template">
    <?php $this->load->view('guard/reject'); ?>
</script>
