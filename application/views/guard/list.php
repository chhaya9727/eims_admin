<div class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="m-0 text-dark"><i class="nav-icon fa fa-buildings"></i> Guard</h1>
            </div>
            <div class="col-sm-4">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="main#dashboard">Home</a></li>
                    <li class="breadcrumb-item active">Guard</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid" id="guard_datatable_container">
    </div>
    <div class="container-fluid" id="guard_form_container" style="display: none;">
    </div>
</section>
<form target="_blank" id="guard_form_pdf_form" action="guard/generate_form" method="post">
    <input type="hidden" id="guard_id_for_guard_form" name="guard_id_for_guard_form">
</form>
<form target="_blank" id="guard_certificate_pdf_form" action="guard/generate_certificate" method="post">
    <input type="hidden" id="guard_id_for_certificate" name="guard_id_for_certificate">
</form>
<form target="_blank" id="generate_excel_for_guard" action="guard/generate_excel" method="post">
</form>