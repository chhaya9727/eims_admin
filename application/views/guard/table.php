<div class="card">
    <div class="card-header">
        <button type="button" class="btn btn-sm btn-success" style="padding: 2px 7px;"
                onclick="$('#generate_excel_for_guard').submit();">
            <i class="fas fa-file-excel" style="margin-right: 2px;"></i>&nbsp; Download Excel</button>
        <h3 class="card-title pull-right">
            <button type="button" class="btn btn-sm btn-primary" onclick="GUARD.listview.newGUARDForm(false, {});">Add New Guard</button>
        </h3>
    </div>
    <div class="card-body" id="guard_datatable_container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <span class="error-message error-message-guard f-w-b" style="border-bottom: 2px solid red;"></span>
            </div>
        </div>
        <div class="table-responsive">
            <table id="guard_datatable" class="table table-bordered table-hover">
                <thead>
                    <tr class="bg-light-gray">
                        <th class="text-center" style="width: 30px;">No.</th>
                        <th class="text-center" style="width: 100px;">Guard Number</th>
                        <th class="text-center" style="min-width: 120px;">Guard Name</th>
                        <th class="text-center" style="width: 180px;">Guard Mobile Number</th>
                        <th class="text-center" style="width: 50px;">Status</th>
                        <th class="text-center" style="width: 180px;">Action</th>
                    </tr>
                    <tr>
                        <th colspan="2"></th>
                        <th><input type="text" class="form-control" placeholder="Guard Name" />  </th>
                        <th><input type="text" id="mobile_number_for_guard_list" class="form-control" placeholder="Mobile Number"  maxlength="10" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>