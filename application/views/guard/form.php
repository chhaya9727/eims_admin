<div class="row">
    <!--  <div class="col-sm-12"></div> -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <!--<h3 class="card-title" style="float: none; text-align: center;">DEPARTMENT OF EXCISE</h3>-->
                <div style="font-size: 16px; text-align: center; margin-top: 0px;font-weight: bold;">U.T. Administration of Dadra And Nagar Haveli & Daman & Diu</div>
                <div style="font-size: 16px; text-align: center; margin-top: 0px;font-weight: bold;">Office of the Assistant Commissioner of Excise</div>
                <div style="font-size: 16px; text-align: center; margin-top: 0px;font-weight: bold;">Excise Department, Daman</div>
                <div style="font-size: 16px; text-align: center; margin-top: 0px;font-weight: bold;">Application of Guard</div>
            </div>
            <form role="form" id="guard_form" name="guard_form" onsubmit="return false;">
                <input type="hidden" id="guard_id" name="guard_id" value="{{guard_data.guard_id}}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <span class="error-message error-message-guard f-w-b" style="border-bottom: 2px solid red;"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>1. Guard Name <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <input type="text" id="guard_name" name="guard_name" class="form-control" placeholder="Guard Name !" maxlength="100" value="{{guard_data.guard_name}}" 
                                       onblur="checkValidation('guard', 'guard_name', nameValidationMessage);">
                            </div>
                            <span class="error-message error-message-guard-guard_name"></span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>2. Guard Designation <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <input type="text" name="guard_designation" id="guard_designation" class="form-control" placeholder="Guard Designation"
                                       value="{{guard_data.guard_designation}}" onblur="checkValidation('guard', 'guard_designation', designationValidationMessage);">
                            </div>
                            <span class="error-message error-message-guard-guard_designation"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>3. Mobile No. <span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" id="guard_mob" name="guard_mob" class="form-control" placeholder="Guard Mobile No. !"
                                       maxlength="10" onblur="checkNumeric($(this)); checkValidationForMobileNumber('guard', 'guard_mob', mobileValidationMessage);" value="{{guard_data.guard_mob}}">
                            </div>
                            <span class="error-message error-message-guard-guard_mob"></span>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>4. Email Address  <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <input type="text" id="guard_email" name="guard_email" class="form-control" placeholder="Guard Email Address !"
                                       maxlength="200"  onblur="checkValidationForEmail('guard', 'guard_email', emailValidationMessage);" value="{{guard_data.guard_email}}">
                            </div>
                            <span class="error-message error-message-guard-guard_email"></span>
                        </div>
                    </div>
                    <hr class="m-b-1rem"> 
                    <div class="form-group">
                        <!--<button type="button" id="draft_btn_for_guard" class="btn btn-sm btn-nic-blue" onclick="GUARD.listview.submitGUARD({{VALUE_ONE}});" style="margin-right: 5px;">Save as a Draft</button>-->
                        <button type="button" id="submit_btn_for_guard" class="btn btn-sm btn-success" onclick="GUARD.listview.askForSubmitGUARD({{VALUE_TWO}});" style="margin-right: 5px;">Submit</button>
                        <button typee="button" class="btn btn-sm btn-danger" onclick="GUARD.listview.loadData();">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>