<div class="text-center">
    <button type="button" class="btn btn-sm btn-success" onclick="GUARD.listview.editOrViewGUARD($(this),'{{guard_id}}', true);"
            style="padding: 2px 7px; margin-top: 1px; margin-bottom: 2px;">
        <i class="fas fa-pencil-alt" style="margin-right: 2px;"></i>Edit</button>
    <button type="button" class="btn btn-sm btn-primary" onclick="GUARD.listview.editOrViewGUARD($(this),'{{guard_id}}', false);"
            style="padding: 2px 7px; margin-top: 1px; margin-bottom: 2px;">
        <i class="fas fa-eye" style="margin-right: 2px;"></i>View</button>
    <button type="button" class="btn btn-sm btn-{{active_deactive_class}}" onclick="$(this).closest('tr').click(); GUARD.listview.activeDeactiveUser($(this),{{guard_id}}, '{{new_active_deactive_type}}')"
            data-placement="top" data-toggle="tooltip" title="{{active_deactive_title}}" style="padding: 2px 7px; margin-top: 1px; margin-bottom: 2px;">
        <label class="fas fa-{{active_deactive_type}} label-btn-icon"></label>
    </button>
</div>