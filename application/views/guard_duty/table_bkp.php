<div class="card-body">
    <!-- we are adding the accordion ID so Bootstrap's collapse plugin detects it -->
    <div id="accordion">
        <div class="card">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseCurr" aria-expanded="false">
                        Current Week ({{curr_week_from}} - {{curr_week_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseCurr" class="collapse" data-parent="#accordion" style="">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="curr_guard_duty_datatable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Duty Point" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th colspan="1">
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-4">
                            <table id="guard_reserve_duty_datatable" class="table table-bordered table-hover">
                                <thead>
                                    <tr class="bg-light-gray">
                                        <th class="text-center" style="width: 30px;">No.</th>
                                        <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <input type="text" class="form-control" placeholder="Guard Name" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="true" onclick="GuardDuty.listview.loadGuardDutyData($(this),'{{week_one_from}}','{{week_one_to}}');">
                        Week One ({{week_one_from}} - {{week_one_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="collapse" data-parent="#accordion" style="">
            <!--<div id="collapseOne" class="collapse show" data-parent="#accordion" style="">-->
                <div class="card-header">
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{week_one_from}}','{{week_one_to}}');">Frizee Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary" onclick="GuardDuty.listview.loadGuardDutyData($(this),'{{week_one_from}}','{{week_one_to}}');">Refresh Guard List</button>
                    </h3>
                </div>
                <div class="card-body" id="guard_duty_datatable_container">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Duty Point" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th colspan="1">
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-4">
                            <table id="guard_reserve_duty_datatable" class="table table-bordered table-hover">
                                <thead>
                                    <tr class="bg-light-gray">
                                        <th class="text-center" style="width: 30px;">No.</th>
                                        <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <input type="text" class="form-control" placeholder="Guard Name" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" onclick="GuardDuty.listview.loadGuardDutyData($(this),'{{week_two_from}}','{{week_two_to}}');">
                        Week Two ({{week_two_from}} - {{week_two_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="collapse" data-parent="#accordion" style="">
                <div class="card-header">
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{week_two_from}}','{{week_two_to}}');">Frizee Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary" onclick="GuardDuty.listview.loadGuardDutyData($(this),'{{week_two_from}}','{{week_two_to}}');">Refresh Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Duty Point" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th colspan="1">
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-4">
                            <table id="guard_reserve_duty_datatable" class="table table-bordered table-hover">
                                <thead>
                                    <tr class="bg-light-gray">
                                        <th class="text-center" style="width: 30px;">No.</th>
                                        <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <input type="text" class="form-control" placeholder="Guard Name" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100" data-toggle="collapse" href="#collapseThree" aria-expanded="false" onclick="GuardDuty.listview.loadGuardDutyData($(this),'{{week_three_from}}','{{week_three_to}}');">
                        Week Three ({{week_three_from}} - {{week_three_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="collapse" data-parent="#accordion" style="">
                <div class="card-header">
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{week_three_from}}','{{week_three_to}}');">Frizee Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary" onclick="GuardDuty.listview.loadGuardDutyData($(this),'{{week_three_from}}','{{week_three_to}}');">Refresh Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Duty Point" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th colspan="1">
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-4">
                            <table id="guard_reserve_duty_datatable" class="table table-bordered table-hover">
                                <thead>
                                    <tr class="bg-light-gray">
                                        <th class="text-center" style="width: 30px;">No.</th>
                                        <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <input type="text" class="form-control" placeholder="Guard Name" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseFOUR" aria-expanded="false" onclick="GuardDuty.listview.loadGuardDutyData($(this),'{{week_four_from}}','{{week_four_to}}');">
                        Week Four ({{week_four_from}} - {{week_four_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseFOUR" class="collapse" data-parent="#accordion" style="">
                <div class="card-header">
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{week_four_from}}','{{week_four_to}}');">Frizee Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary" onclick="GuardDuty.listview.loadGuardDutyData($(this),'{{week_four_from}}','{{week_four_to}}');">Refresh Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Duty Point" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th colspan="1">
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th>
                                                <input type="text" class="form-control" placeholder="Guard Name" />
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-4">
                            <table id="guard_reserve_duty_datatable" class="table table-bordered table-hover">
                                <thead>
                                    <tr class="bg-light-gray">
                                        <th class="text-center" style="width: 30px;">No.</th>
                                        <th class="text-center" style="min-width: 80px;">Guard Name - Reserve Shift</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <input type="text" class="form-control" placeholder="Guard Name" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>