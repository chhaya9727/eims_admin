<?php
$header_array = array();
$header_array['page_size'] = 'Legal';
$this->load->view('certificate/header', $header_array);
?>
<style type="text/css">
    .f-s-14px{
        font-size: 14px;
    }
    .read{
        margin-left: 50px;
        margin-bottom: 5px;
        word-spacing: 8px;
    }
    .m-t-read{
        margin-top: -18px;
    }
    .f-aum{
        font-family: arial_unicode_ms;
    }
</style>
<div style="height: 1100px;">
    <div class="f-s-14px" style="margin-top: -10px;">No.&emsp;/&emsp;&emsp;/EST-EXC/&emsp;-/&emsp;&emsp;</div>
    <div class="f-s-14px" style="margin-top: -18px; text-align: right;">Date:&emsp;&emsp;/&emsp;&emsp;/&emsp;&emsp;</div>
    <br/>
    <div style="font-size: 25px; text-align: center; margin-top: 5px;font-weight: bold;letter-spacing: 5px;"><u>ORDER</u></div>
    <br/>
    <div class="f-s-14px" style="margin-left: 50px; margin-bottom: 5px;word-spacing: 4px;margin-top: 0px; line-height: 20px;
         text-align: justify; text-justify: inter-word;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The following Excise Guards are deployed at the Check-post, Manufacturing Unit (Distilleries + Breweries), Zones, Points to perform their duty as mentioned against their names with immediate effect till further order as per Annexure attached.<br/></div>
    <div class="f-s-14px" style="margin-left: 50px; margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
         text-align: justify; text-justify: inter-word;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All the excise staff should perform their duty as a team and with responsibility as under:</div>

    <ol style="margin-left: 5px;">
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">They should not leave the premises without prior permission of the Assistant Commissioner of Excise during duty hours.</li>
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">All the vehicles going out of the distillery premises should be checked properly with relevant documents.</li>
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">All the Excise staff should perform their duty in full uniform only.</li>
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">4.	All the Excise Guards should report any kind of untoward incidences or illegal activities in violation to the DNHDDED Act and Rules to the Excise Inspector (Station). If any illegal activities in violation to the DNHDDED Act and Rules found by Excise Guards posted in Zones or distillery/brewery same is reported to In-Charge of Zone / distillery/brewery under intimation of Excise Inspector (Station).</li>
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">The Excise Guards posted in the Distillery shall immediately inform the Excise Guard posted at Dabhel check post by mobile phone for every dispatch made against the export permits issued to the distilleries by giving the details of export permit and vehicle no. and time of dispatch.</li>
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">The Excise Guards posted at Jampore and Devka Beach will comply strict vigilance on public place drinking and impose fine as per SOP as directed by Zone In-charge.</li>
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">The Excise Guard shall perform their duties in three shifts irrespective of the duty posts i.e from 08:00 to 14:00 hours, 14:00 to 21:00 hours and 21:00 to 08:00 hours. Any internal arrangement shall be intimated to the Assistant Commissioner of Excise.</li>
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">The Excise Guard posted at the distillery/brewery and at the check-post shall maintain a duty register, wherein the staffs shall log their duty timings.</li>
        <li class="f-s-14px" style="margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
            text-align: justify; text-justify: inter-word;">In addition to above, the Excise Guards may be engaged in any other urgent matters and they have to attend such matters “on call”.</li>
    </ol>  
    <div class="f-s-14px" style="margin-left: 28px; margin-bottom: 5px;word-spacing: 3px;margin-top: 10px; line-height: 20px;
         text-align: justify; text-justify: inter-word;">&nbsp; This is issued with approval of the Competent Authority.</div>
    <div class="f-s-14px" style="margin-top: 20px;">Encl: Annexure</div>
    <div class="f-s-14px" style="margin-top: 128px;margin-left: 400px;text-align: center;font-weight: bold;">(Mohit Mishra)<br/>Assistant Commissioner of Excise,<br/>Daman.</div>
</div>
<hr style="color: black;height: 2px;">
<table class="table-header">
    <tr>
        <td style="vertical-align: bottom;" class="t-a-c">
            <div class="color-nic-blue footer-title">
                Address: GF-14, Collectorate Premises, Dholar, Moti Daman – 396220
            </div>
        </td>
        </td>
    </tr>
</table>
<?php
$header_array['page_size'] = 'Legal';
$this->load->view('certificate/header', $header_array);
?>
<div style="font-size: 15px; text-align: center; margin-top: -20px;font-weight: bold;letter-spacing: 5px;margin-bottom: 5px;"><u>ANNEXURE</u></div>
<div class="table-responsive">
    <div style="height: 1100px;">
        <table class="table table-bordered table-padding" style="width: 100%;border-collapse: collapse;font-size: 13px;">
            <thead>
                <tr style="border: 1px solid black;">
                    <td class="f-w-b text-center v-a-m bg-beige" style="width: 120px;border: 1px solid black;text-align: center;">Duty Points</td>
                    <td class="f-w-b text-center v-a-m bg-beige" style="width: 100px;border: 1px solid black;text-align: center;">0800 to 1400 hours</td>
                    <td class="f-w-b text-center v-a-m bg-beige" style="width: 100px;border: 1px solid black;text-align: center;">1400 to 2100 hours</td>
                    <td class="f-w-b text-center v-a-m bg-beige" style="width: 100px;border: 1px solid black;text-align: center;">2100 to 0800 hours</td>
                </tr>
            </thead>
            <?php
            foreach ($guard_duty_data as $gd) {
                if (!empty($gd['first_shift_guard_name'])) {
                    $hr_fs = str_replace(",", "<hr style='margin-top:1px;margin-bottom:1px;'>", rtrim($gd['first_shift_guard_name'], ","));
                }
                if (!empty($gd['second_shift_guard_name'])) {
                    $hr_ss = str_replace(",", "<hr style='margin-top:1px;margin-bottom:1px;'>", rtrim($gd['second_shift_guard_name'], ","));
                }
                if (!empty($gd['night_shift_guard_name']) && $gd['night_shift_guard_name'] != null) {
                    $hr_ns = str_replace(",", "<hr style='margin-top:1px;margin-bottom:1px;'>", rtrim($gd['night_shift_guard_name'], ","));
                }
                if (!empty($gd['general_shift_guard_name']) && $gd['general_shift_guard_name'] != null) {
                    $hr_gs = str_replace(",", "<hr style='margin-top:1px;margin-bottom:1px;'>", rtrim($gd['general_shift_guard_name'], ","));
                }
                ?>
                <tr style="border: 1px solid black;">
                    <td class="t-a-c" style="border: 1px solid black;width: 100px;"><?php echo $gd['name']; ?></td>
                    <?php if ($hr_gs != 'undefined' && $hr_gs != NULL && $hr_gs != '-') { ?>
                        <td class = "t-a-c" colspan = "3" style = "border: 1px solid black;"><?php echo $hr_gs; ?></td>
                        <?php } else {
                        ?>
                        <td class="t-a-c" style="border: 1px solid black;"><?php echo $hr_fs == 'undefined' ? '-' : $hr_fs; ?></td>
                        <td class="t-a-c" style="border: 1px solid black;"><?php echo $hr_ss == 'undefined' ? '-' : $hr_ss; ?></td>
                        <td class="t-a-c" style="border: 1px solid black;"><?php echo $hr_ns == 'undefined' ? '-' : $hr_ns; ?></td>
                        <?php }
                        ?>

                    </tr>
                <?php } ?>
            </table>
            <?php
            $i = 0;
            $tempGuardName = '';
            foreach ($guard_duty_data as $gd) {
                if ($guard_duty_data[$i]['reserve_shift_guard_name'] != '-') {
                    $tempGuardName .= $guard_duty_data[$i]['reserve_shift_guard_name'] . '<br/>';
                }
                $i++;
            }
            ?>
            <table class="table table-bordered table-padding" style="width: 100%;border-collapse: collapse;font-size: 13px;">
                <thead>
                    <tr style="border: 1px solid black;">
        <!--                <td  class="f-w-b text-center v-a-m bg-beige" style="width: 100px;border: 1px solid black;text-align: center;height: 10px;"><u>OFFICE</u></td>-->
                        <td  class="f-w-b text-center v-a-m bg-beige" style="width: 100px;text-align: center;border: 1px solid black;"><u>RESERVE GUARDS</u></td>
                    </tr>
                </thead>
                <tbody>
                    <tr style="border: 1px solid black;">
        <!--                <td class="text-center v-a-m bg-beige" style="width: 100px;border: 1px solid black;text-align: left;height: 40px;">Shri. Ankit Patel:<br/>1. Inward/ Outward of letters<br/>
                                2. All permit related process<br/>3. Brand approval process<br/>Any other assigned work 
                        </td>-->
                        <td  class="text-center v-a-m bg-beige" style="width: 100px;border: 1px solid black;text-align: center; height: 100px;"><?php echo $tempGuardName; ?></td>
                </tr>
    <!--            <tr style="border: 1px solid black;">
                    <td class="text-center v-a-m bg-beige" style="width: 100px;border: 1px solid black;text-align: left;height: 40px;">Shri. Amit Rajbhar:<br/>1. Maintenance of Crime Register<br/>
                            2. Process of cases<br/>3. Record of seized vehicles and Liquor<br/>Any other assigned work </td>
                </tr>-->

            </tbody>

        </table>
        <div class="f-s-14px" style="margin-top: 50px;margin-left: 400px;text-align: center;font-weight: bold;">(Mohit Mishra)<br/>Assistant Commissioner of Excise,<br/>Daman.</div>
        <div class="f-s-14px" style="margin-top: -30px;">To,<br/>All concerned.</div>
    </div>
    <hr style="color: black;height: 2px;">
    <table class="table-header">
        <tr>
            <td style="vertical-align: bottom;" class="t-a-c">
                <div class="color-nic-blue footer-title">
                    Address: GF-14, Collectorate Premises, Dholar, Moti Daman – 396220
                </div>
            </td>
            </td>
        </tr>
    </table>
</div>