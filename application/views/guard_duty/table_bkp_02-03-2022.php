<div class="card-body">
    <div id="accordion">
        <div class="card">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseCurr" aria-expanded="false">
                        {{currMonthName}} - ({{curr_week_from}} - {{curr_week_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseCurr" class="collapse" data-parent="#accordion" style="">
                <div class="card-header">
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-danger" onclick="GuardDuty.listview.generateReport($(this),'{{curr_week_from}}','{{curr_week_to}}', 'one');">Report Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="curr_guard_duty_datatable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                        <tr>
                                            <th class="text-center">Office</th>
                                            <th class="text-center">Reserve Guards</th>
                                        </tr>
                                </thead>
                                <tbody id="current_reserve_guard_list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card cardOne">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="true" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{week_one_from}}','{{week_one_to}}','{{VALUE_ONE}}');">
                        {{oneMonthName}} - ({{week_one_from}} - {{week_one_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="collapse week_one_data" data-parent="#accordion">
                <!--<div id="collapseOne" class="collapse show" data-parent="#accordion" style="">-->
                <div class="card-header">
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-danger" style="{{week_one_report_data_status}}" id ="report_btn_{{VALUE_ONE}}" onclick="GuardDuty.listview.generateReport($(this),'{{week_one_from}}','{{week_one_to}}', '{{VALUE_ONE}}');">Report Guard List</button>
                    </h3>
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success" style="{{week_one_frizee_data_status}}" id ="frizee_btn_{{VALUE_ONE}}" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{week_one_from}}','{{week_one_to}}','{{VALUE_ONE}}');">Freeze Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary" style="{{week_one_frizee_data_status}}" id ="referesh_btn_{{VALUE_ONE}}" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{week_one_from}}','{{week_one_to}}','{{VALUE_ONE}}');">Randomize Guard List</button>
                    </h3>
                </div>

                <div class="card-body" id="guard_duty_datatable_container">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable_{{VALUE_ONE}}" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                        <tr>
                                            <th class="text-center">Office</th>
                                            <th class="text-center">Reserve Guards</th>
                                        </tr>
                                </thead>
                                <tbody id="reserve_guard_list_one">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="card" style="{{collapse_show_status}}">-->
        <div class="card cardTwo">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{week_two_from}}','{{week_two_to}}','{{VALUE_TWO}}');">
                        {{twoMonthName}} - ({{week_two_from}} - {{week_two_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="collapse week_two_data" data-parent="#accordion">
                <div class="card-header">
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-danger" style="{{week_two_report_data_status}}" id ="report_btn_{{VALUE_TWO}}" onclick="GuardDuty.listview.generateReport($(this),'{{week_two_from}}','{{week_two_to}}', '{{VALUE_TWO}}');">Report Guard List</button>
                    </h3>
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success" style="{{week_two_frizee_data_status}}" id ="frizee_btn_{{VALUE_TWO}}" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{week_two_from}}','{{week_two_to}}','{{VALUE_TWO}}');">Freeze Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary" style="{{week_two_frizee_data_status}}" id ="referesh_btn_{{VALUE_TWO}}" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{week_two_from}}','{{week_two_to}}','{{VALUE_TWO}}');">Randomize Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable_{{VALUE_TWO}}" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                        <tr>
                                            <th class="text-center">Office</th>
                                            <th class="text-center">Reserve Guards</th>
                                        </tr>
                                </thead>
                                <tbody id="reserve_guard_list_two">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card cardThree">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100" data-toggle="collapse" data-parent="" href="#collapseThree" aria-expanded="false" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{week_three_from}}','{{week_three_to}}','{{VALUE_THREE}}');">
                        {{threeMonthName}} - ({{week_three_from}} - {{week_three_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="collapse week_three_data" data-parent="#accordion">
                <div class="card-header">
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-danger" style="{{week_three_report_data_status}}" id ="report_btn_{{VALUE_THREE}}" onclick="GuardDuty.listview.generateReport($(this),'{{week_three_from}}','{{week_three_to}}', '{{VALUE_THREE}}');">Report Guard List</button>
                    </h3>
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success" style="{{week_three_frizee_data_status}}" id ="frizee_btn_{{VALUE_THREE}}" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{week_three_from}}','{{week_three_to}}','{{VALUE_THREE}}');">Freeze Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary" style="{{week_three_frizee_data_status}}" id ="referesh_btn_{{VALUE_THREE}}" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{week_three_from}}','{{week_three_to}}','{{VALUE_THREE}}');">Randomize Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable_{{VALUE_THREE}}" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                        <tr>
                                            <th class="text-center">Office</th>
                                            <th class="text-center">Reserve Guards</th>
                                        </tr>
                                </thead>
                                <tbody id="reserve_guard_list_three">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card cardFour">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseFOUR" aria-expanded="false" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{week_four_from}}','{{week_four_to}}','{{VALUE_FOUR}}');">
                        {{fourMonthName}} - ({{week_four_from}} - {{week_four_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseFOUR" class="collapse week_four_data" data-parent="#accordion" style="">
                <div class="card-header">
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-danger" style="{{week_four_report_data_status}}" id ="report_btn_{{VALUE_FOUR}}" onclick="GuardDuty.listview.generateReport($(this),'{{week_four_from}}','{{week_four_to}}', '{{VALUE_FOUR}}');">Report Guard List</button>
                    </h3>
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success" style="{{week_four_frizee_data_status}}" id ="frizee_btn_{{VALUE_FOUR}}" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{week_four_from}}','{{week_four_to}}','{{VALUE_FOUR}}');">Freeze Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary" style="{{week_four_frizee_data_status}}" id ="referesh_btn_{{VALUE_FOUR}}" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{week_four_from}}','{{week_four_to}}','{{VALUE_FOUR}}');">Randomize Guard List</button>
                    </h3>
                </div>
                <!--                <div class="card-header">
                                    
                                </div>-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable_{{VALUE_FOUR}}" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                        <tr>
                                            <th class="text-center">Office</th>
                                            <th class="text-center">Reserve Guards</th>
                                        </tr>
                                </thead>
                                <tbody id="reserve_guard_list_four">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>