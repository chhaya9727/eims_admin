<div class="card-body">
    <div id="accordion">
        <div class="card cardOne">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapseprev" aria-expanded="false" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{prev_month_from}}','{{prev_month_to}}','{{VALUE_ONE}}');">
                        {{prevMonthName}} - ({{prev_month_from}} - {{prev_month_to}})
                    </a>
                </h4>
            </div>
            <div id="collapseprev" class="collapse" data-parent="#accordion" style="">
                <div class="card-header">
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-danger r1" style="{{prev_month_report_data_status}}" id ="report_btn_{{VALUE_ONE}}" onclick="GuardDuty.listview.generateReport($(this),'{{prev_month_from}}','{{prev_month_to}}', 'one');">Report Guard List</button>
                    </h3>
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success f1" style="{{prev_month_frizee_data_status}}" id ="frizee_btn_{{VALUE_ONE}}" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{prev_month_from}}','{{prev_month_to}}','{{VALUE_ONE}}');">Freeze Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary f1" style="{{prev_month_frizee_data_status}}" id ="referesh_btn_{{VALUE_ONE}}" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{prev_month_from}}','{{prev_month_to}}','{{VALUE_ONE}}');">Randomize Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable_{{VALUE_ONE}}" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                        <tr>
                                            <!--<th class="text-center">Office</th>-->
                                            <th class="text-center">Reserve Guards</th>
                                        </tr>
                                </thead>
                                <tbody id="previous_reserve_guard_list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card cardTwo">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapsecurr" aria-expanded="false" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{curr_month_from}}','{{curr_month_to}}','{{VALUE_TWO}}');">
                        {{currMonthName}} - ({{curr_month_from}} - {{curr_month_to}})
                    </a>
                </h4>
            </div>
            <div id="collapsecurr" class="collapse" data-parent="#accordion" style="">
                <div class="card-header">
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-danger r2" style="{{curr_month_report_data_status}}" id ="report_btn_{{VALUE_TWO}}"  onclick="GuardDuty.listview.generateReport($(this),'{{curr_month_from}}','{{curr_month_to}}', 'two');">Report Guard List</button>
                    </h3>
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success f2" style="{{curr_month_frizee_data_status}}" id ="frizee_btn_{{VALUE_TWO}}" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{curr_month_from}}','{{curr_month_to}}','{{VALUE_TWO}}');">Freeze Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary f2" style="{{curr_month_frizee_data_status}}" id ="referesh_btn_{{VALUE_TWO}}" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{curr_month_from}}','{{curr_month_to}}','{{VALUE_TWO}}');">Randomize Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable_{{VALUE_TWO}}" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                        <tr>
                                            <!--<th class="text-center">Office</th>-->
                                            <th class="text-center">Reserve Guards</th>
                                        </tr>
                                </thead>
                                <tbody id="current_reserve_guard_list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card cardThree">
            <div class="card-header" style="background-color:#dfeeed">
                <h4 class="card-title w-100">
                    <a class="d-block w-100 collapsed" data-toggle="collapse" href="#collapsenext" aria-expanded="false" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{next_month_from}}','{{next_month_to}}','{{VALUE_THREE}}');">
                        {{nextMonthName}} - ({{next_month_from}} - {{next_month_to}})
                    </a>
                </h4>
            </div>
            <div id="collapsenext" class="collapse" data-parent="#accordion" style="">
                <div class="card-header">
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-danger r3" style="{{next_month_report_data_status}}" id ="report_btn_{{VALUE_THREE}}" onclick="GuardDuty.listview.generateReport($(this),'{{next_month_from}}','{{next_month_to}}', 'three');">Report Guard List</button>
                    </h3>
                    <h3 class="card-title float-right">
                        <button type="button" class="btn btn-sm btn-success f3" style="{{next_month_frizee_data_status}}" id ="frizee_btn_{{VALUE_THREE}}" onclick="GuardDuty.listview.frizeeGuardDutyData($(this),'{{next_month_from}}','{{next_month_to}}','{{VALUE_THREE}}');">Freeze Guard List</button>
                    </h3>
                    <h3 class="card-title float-right" style="margin-right: 5px;">
                        <button type="button" class="btn btn-sm btn-primary f3" style="{{next_month_frizee_data_status}}" id ="referesh_btn_{{VALUE_THREE}}" onclick="GuardDuty.listview.guardDutyDataWithStatus($(this),'{{next_month_from}}','{{next_month_to}}','{{VALUE_THREE}}');">Randomize Guard List</button>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="table-responsive">
                                <table id="guard_duty_datatable_{{VALUE_THREE}}" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light-gray">
                                            <th class="text-center" style="width: 30px;">No.</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center" style="min-width: 120px;">Duty Point</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - First Shift </th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Second Shift</th>
                                            <th class="text-center" style="min-width: 80px;">Guard Name - Night Shift</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                        <tr>
                                            <!--<th class="text-center">Office</th>-->
                                            <th class="text-center">Reserve Guards</th>
                                        </tr>
                                </thead>
                                <tbody id="next_reserve_guard_list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>