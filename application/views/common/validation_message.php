<script type="text/javascript">
    var baseUrl = '<?php echo base_url(); ?>';
    var invalidAccessValidationMessage = '<?php echo INVALID_ACCESS_MESSAGE ?>';
    var passwordPolicyValidationMessage = '<?php echo PASSWORD_POLICY_MESSAGE ?>';
    var passwordAndRetypePasswordValidationMessage = '<?php echo PASSWORD_AND_RETYPE_PASSWORD_NOT_MATCH_MESSAGE; ?>';
    var noRecordFoundMessage = '<?php echo NO_RECORD_FOUND_MESSAGE; ?>';
    var emailValidationMessage = '<?php echo EMAIL_MESSAGE; ?>';
    var invalidEmailValidationMessage = '<?php echo INVALID_EMAIL_MESSAGE; ?>';
    var dateValidationMessage = '<?php echo DATE_MESSAGE; ?>';
    var mobileValidationMessage = '<?php echo MOBILE_NUMBER_MESSAGE; ?>';
    var invalidMobileValidationMessage = '<?php echo INVALID_MOBILE_NUMBER_MESSAGE; ?>';
    var licenseNoNotAvailable = '<?php echo LICENSE_NO_NOT_AVAILABLE; ?>';
    var registrationNumberExistsValidationMessage = '<?php echo REGISTRATION_NUMBER_EXISTS_MESSAGE; ?>';
    var registrationFileNoValidationMessage = '<?php echo REGISTRATION_FILE_NO_MESSAGE; ?>';
    var onePaymentOptionValidationMessage = '<?php echo ONE_PAYMENT_OPTION_MESSAGE; ?>';
    var invalidAadharNumberValidationMessage = '<?php echo INVALID_AADHAR_MESSAGE; ?>';
    var aadharValidationMessage = '<?php echo AADHAR_MESSAGE; ?>';
    //var districtValidationMessage = '<?php //echo DISTRICT_MESSAGE; ?>';
    var uploadDocumentValidationMessage = '<?php echo UPLOAD_DOCUMENT_MESSAGE; ?>';
    var selectCategoryValidationMessage = '<?php echo CATEGORY_MESSAGE; ?>';
    var enterValueValidationMessage = '<?php echo VALUE_MESSAGE; ?>';
    var invalidIdValidationMessage = '<?php echo INVALID_ID_MESSAGE; ?>';

    // Login Messages
    var passwordRegex = <?php echo PASSWORD_REGEX ?>;
    var usernameValidationMessage = '<?php echo USERNAME_MESSAGE ?>';
    var passwordValidationMessage = '<?php echo PASSWORD_MESSAGE ?>';
    var newPasswordValidationMessage = '<?php echo NEW_PASSWORD_MESSAGE ?>';
    var invalidPasswordValidationMessage = '<?php echo INVALID_PASSWORD_MESSAGE ?>';
    var retypePasswordValidationMessage = '<?php echo RETYPE_PASSWORD_MESSAGE ?>';
    var usernameOrPasswordIsInvalid = '<?php echo INVALID_USERNAME_OR_PASSWORD_MESSAGE; ?>';

    // User Type Messages
    var invalidUserTypeValidationMessage = '<?php echo INVALID_USER_TYPE_MESSAGE; ?>';
    var userTypeValidationMessage = '<?php echo USER_TYPE_MESSAGE; ?>';

    // Users Messages
    var nameValidationMessage = '<?php echo NAME_MESSAGE; ?>';
    var selectUserTypeValidationMessage = '<?php echo SELECT_USER_TYPE_MESSAGE; ?>';
    var selectUserValidationMessage = '<?php echo SELECT_USER_MESSAGE; ?>';
    var invalidUserValidationMessage = '<?php echo INVALID_USER_MESSAGE; ?>';

    // Department Messages
    var departmentValidationMessage = '<?php echo DEPARTMENT_MESSAGE; ?>';
    var selectDepartmentValidationMessage = '<?php echo SELECT_DEPARTMENT_MESSAGE; ?>';
    var invalidDepartmentValidationMessage = '<?php echo INVALID_DEPARTMENT_MESSAGE; ?>';

    
    // Checkpost / Distillery Messages
    var checkpostNameValidationMessage = '<?php echo CHECKPOST_NAME_MESSAGE; ?>';
    var checkpostAddressValidationMessage = '<?php echo CHECKPOST_ADDRESS_MESSAGE; ?>';
    var nodelPersoneNameValidationMessage = '<?php echo NODEL_PERSONE_NAME_MESSAGE; ?>';
    var exciseGuardNoValidationMessage = '<?php echo EXCISE_GUARD_NO_MESSAGE; ?>';
    // // State Reforms Action Plan Module
    // var serialNoValidationMessage = '<?php //echo SERIAL_NO_MESSAGE; ?>';
    // var invalidSRAPValidationMessage = '<?php //echo INVALID_SRAP_MESSAGE; ?>';
    // var districtValidationMessage = '<?php //echo DISTRICT_MESSAGE; ?>';
    // var areaValidationMessage = '<?php //echo AREA_MESSAGE; ?>';
    // var subAreaValidationMessage = '<?php //echo SUB_AREA_MESSAGE; ?>';
    // var reformsValidationMessage = '<?php //echo REFORMS_MESSAGE; ?>';
    // var oneOptionValidationMessage = '<?php //echo ONE_OPTION_MESSAGE; ?>';
    // var reformContentValidationMessage = '<?php //echo REFORM_CONTENT_MESSAGE; ?>';

    // Query Mananagemt Module
    var remarksValidationMessage = '<?php echo REMARKS_MESSAGE; ?>';
    var documentNameValidationMessage = '<?php echo DOCUMENT_NAME_MESSAGE; ?>';

    // Guard
    var designationValidationMessage = '<?php echo DESIGANTION_MESSAGE; ?>';
    var selectOptionValidationMessage = '<?php echo SELECT_OPTION_MESSAGE; ?>';

</script>