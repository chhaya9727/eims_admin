<aside class="main-sidebar sidebar-dark-primary">
    <?php $this->load->view('common/logo'); ?>
    <!-- Sidebar -->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
<!--                <li class="nav-item">
                    <a id="menu_dashboard" href="Javascript:void(0);" class="nav-link menu-close-click"
                       onclick="Dashboard.listview.listPage();">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>-->
                <?php if (is_admin() || is_excise_dept_user()) { ?>
                    <li class="nav-item has-treeview">
                        <li class="nav-item">
                            <a id="menu_checkpost_distillery" href="Javascript:void(0);"
                               onclick="CheckpostDistillery.listview.listPage();" class="nav-link menu-close-click">
                                <!--<i class="fas fa-archway nav-icon"></i>-->
                                <i class="fab fa-fort-awesome nav-icon"></i>
                                <p>Checkpost / Distillery</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="menu_guard" href="Javascript:void(0);"
                               onclick="GUARD.listview.listPage();" class="nav-link menu-close-click">
                                <i class="fas fa-user-nurse nav-icon"></i>
                                <p>Guard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="menu_guard_duty" href="Javascript:void(0);"
                               onclick="GuardDuty.listview.listPage();" class="nav-link menu-close-click">
                                <!--<i class="fas fa-house-user nav-icon"></i>-->
                                <i class="fas fa-user-friends nav-icon"></i>
                                <p>Guard Duty Posting</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="menu_guard_duty_history" href="Javascript:void(0);"
                               onclick="GuardDutyHistory.listview.listPage();" class="nav-link menu-close-click">
                                <!--<i class="fas fa-house-user nav-icon"></i>-->
                                <i class="fas fa fa-search nav-icon"></i>
                                <p>Guard Duty History</p>
                            </a>
                        </li>
                    </li>
                <?php } if (is_admin()) { ?>
                    <li class="nav-item has-treeview">
                        <a id="menu_users" href="Javascript:void(0)" class="nav-link">
                            <i class="nav-icon fas fa-user-cog"></i>
                            <p>User Management <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a id="menu_users_user" href="Javascript:void(0);"
                                   onclick="Users.listview.listPage();" class="nav-link menu-close-click">
                                    <i class="fas fa-users nav-icon"></i>
                                    <p>Users</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a id="menu_users_user_type" href="Javascript:void(0);"
                                   onclick="Users.listview.listPageForUserType();" class="nav-link menu-close-click">
                                    <i class="fas fa-list-alt nav-icon"></i>
                                    <p>User Type</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a id="menu_logs" href="Javascript:void(0);" class="nav-link">
                            <i class="nav-icon fas fa-list-alt"></i>
                            <p>Logs <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a id="menu_logs_login_detail" href="Javascript:void(0);"
                                   onclick="Logs.listview.listPage();" class="nav-link menu-close-click">
                                    <i class="fas fa-user-lock nav-icon"></i>
                                    <p>Login Details</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
                <li class="nav-item">
                    <a id="menu_change_password" href="Javascript:void(0);"
                       onclick="Users.listview.listPageForChangePassword();" class="nav-link menu-close-click">
                        <i class="nav-icon fa fa-key"></i>
                        <p>Change Password</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a id="menu_logout" href="<?php echo base_url() ?>login/logout" class="nav-link menu-close-click" onclick="activeLink('menu_logout');">
                        <i class="nav-icon fa fa-sign-out-alt"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>