<div class="row">
    <!--  <div class="col-sm-12"></div> -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title" style="float: none; text-align: center;">EXCISE DEPARTMENT DAMAN </h3>
                <div style="font-size: 16px; text-align: center; margin-top: 0px;font-weight: bold;">U.T. Administration of Dadra & Nagar Haveli and Daman & Diu  </div>
                <div style="font-size: 16px; text-align: center; margin-top: 0px;font-weight: bold;">Check post / Out post And Distilleries / Breweries</div>
            </div>
            <form role="form" id="checkpost_distillery_form" name="checkpost_distillery_form" onsubmit="return false;">

                <input type="hidden" id="checkpost_distillery_id" name="checkpost_distillery_id" value="{{checkpost_distillery_id}}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <span class="error-message error-message-checkpost_distillery f-w-b" style="border-bottom: 2px solid red;"></span>
                        </div>
                    </div>
                    <hr class="m-b-5px">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>1. Check post / Distillery Name <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <input type="text" id="name" name="name" class="form-control" placeholder="Check post / Distillery Name !" maxlength="100" value="{{name}}" 
                                       readonly onblur="checkValidation('checkpost_distillery', 'name', checkpostNameValidationMessage);">
                            </div>
                            <span class="error-message error-message-checkpost_distillery-name"></span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>2. Check post / Distillery Address <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <textarea id="address" name="address" class="form-control" placeholder="Check post / Distillery Address !" maxlength="100" readonly onblur="checkValidation('checkpost_distillery', 'address', checkpostAddressValidationMessage);">{{address}}</textarea>
                            </div>
                            <span class="error-message error-message-checkpost_distillery-address"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>3. Select Shift Wise / General <span class="color-nic-red">*</span></label>
                            <div id="shifts_category_container_for_checkpost_distillery"></div>
                            <span class="error-message error-message-checkpost_distillery-shifts_category"></span>    
                        </div>
                    </div>
                    <div class="row" id="shifts_category_item_container_for_checkpost_distillery" style="display: none;">
                        <div class="form-group col-sm-4">
                            <label>4. No. of Excise Guard for First Shift <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <input type="text" id="no_of_eg_for_first_shift" name="no_of_eg_for_first_shift" class="form-control" placeholder="No. of Excise Guard for First Shift !" maxlength="2" value="{{no_of_eg_for_first_shift}}" 
                                       onblur="checkValidation('checkpost_distillery', 'no_of_eg_for_first_shift', exciseGuardNoValidationMessage);" disabled="">
                            </div>
                            <span class="error-message error-message-checkpost_distillery-no_of_eg_for_first_shift"></span>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>5. No. of Excise Guard for Second Shift <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <input type="text" id="no_of_eg_for_second_shift" name="no_of_eg_for_second_shift" class="form-control" placeholder="No. of Excise Guard for Second Shift !" maxlength="2" value="{{no_of_eg_for_second_shift}}" 
                                       onblur="checkValidation('checkpost_distillery', 'no_of_eg_for_second_shift', exciseGuardNoValidationMessage);" disabled="">
                            </div>
                            <span class="error-message error-message-checkpost_distillery-no_of_eg_for_second_shift"></span>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>6. No. of Excise Guard for Night Shift <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <input type="text" id="no_of_eg_for_night_shift" name="no_of_eg_for_night_shift" class="form-control" placeholder="No. of Excise Guard for Night Shift !" maxlength="2" value="{{no_of_eg_for_night_shift}}" 
                                       onblur="checkValidation('checkpost_distillery', 'no_of_eg_for_night_shift', exciseGuardNoValidationMessage);" disabled="">
                            </div>
                            <span class="error-message error-message-checkpost_distillery-no_of_eg_for_night_shift"></span>
                        </div>
                    </div>
                    <div class="row" id="shifts_category_general_item_container_for_checkpost_distillery" style="display: none;">
                        <div class="form-group col-sm-4">
                            <label>4. No. of Excise Guard for General Shift <span class="color-nic-red">*</span></label>
                            <div class="input-group">
                                <input type="text" id="no_of_eg_for_general_shift" name="no_of_eg_for_general_shift" class="form-control" placeholder="No. of Excise Guard for General Shift !" maxlength="2" value="{{no_of_eg_for_general_shift}}" 
                                       onblur="checkValidation('checkpost_distillery', 'no_of_eg_for_general_shift', exciseGuardNoValidationMessage);" disabled="">
                            </div>
                            <span class="error-message error-message-checkpost_distillery-no_of_eg_for_general_shift"></span>
                        </div>
                    </div>
                </div>
                <hr class="m-b-1rem"> 
                <div class="form-group">
                    <button typee="button" class="btn btn-sm btn-danger" onclick="CheckpostDistillery.listview.loadCheckpostDistilleryData();">Close</button>
                </div>
        </div>
        </form>
    </div>
</div>
</div>