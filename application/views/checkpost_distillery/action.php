<div class="text-center">
    <button type="button" class="btn btn-sm btn-success" onclick="CheckpostDistillery.listview.editOrViewCheckpostDistillery($(this),'{{checkpost_distillery_id}}', true);"
            style="padding: 2px 7px; margin-top: 1px; margin-bottom: 2px;">
        <i class="fas fa-pencil-alt" style="margin-right: 2px;"></i>Edit</button>
    <button type="button" class="btn btn-sm btn-primary" onclick="CheckpostDistillery.listview.editOrViewCheckpostDistillery($(this),'{{checkpost_distillery_id}}', false);"
            style="padding: 2px 7px; margin-top: 1px; margin-bottom: 2px;">
        <i class="fas fa-eye" style="margin-right: 2px;"></i>View</button>
</div>