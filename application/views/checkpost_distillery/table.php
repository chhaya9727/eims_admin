<div class="card">
    <div class="card-header">
        <h3 class="card-title pull-right">
            <button type="button" class="btn btn-sm btn-primary" onclick="CheckpostDistillery.listview.newCheckpostDistilleryForm(false, {});">Add Check post / Distilleries</button>
        </h3>
    </div>
    <div class="card-body" id="checkpost_distillery_datatable_container">
        <div class="table-responsive">
            <table id="checkpost_distillery_datatable" class="table table-bordered table-hover">
                <thead>
                    <tr class="bg-light-gray">
                        <th class="text-center" style="width: 30px;">No.</th>
                        <th class="text-center" style="min-width: 120px;">Name</th>
                        <th class="text-center" style="min-width: 80px;">Address</th>
                        <th class="text-center" style="width: 150px;">Action</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th><input type="text" class="form-control" placeholder="Name" /></th>
                        <th><input type="text" class="form-control" placeholder="Address" /></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>