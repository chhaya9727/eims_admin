<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Guard_Duty_model extends CI_Model {

    function get_all_shift_guard_duty_list($fs_guard_id, $ss_guard_id, $ns_guard_id, $gs_guard_id, $last_week_fs_guard_id, $last_week_ss_guard_id, $last_week_ns_guard_id, $last_week_gs_guard_id, $length, $same_checkpost_guards_id, $field_name) {
        if ($length != NULL) {
            $this->db->limit($length);
        }
        if ($fs_guard_id != NULL) {
            $this->db->where_not_in('guard_id', $fs_guard_id);
        }
        if ($ss_guard_id != NULL) {
            $this->db->where_not_in('guard_id', $ss_guard_id);
        }
        if ($ns_guard_id != NULL) {
            $this->db->where_not_in('guard_id', $ns_guard_id);
        }
        if ($gs_guard_id != NULL) {
            $this->db->where_not_in('guard_id', $gs_guard_id);
        }
        if ($last_week_fs_guard_id != NULL) {
            $this->db->where_not_in('guard_id', $last_week_fs_guard_id);
        }
        if ($last_week_ss_guard_id != NULL) {
            $this->db->where_not_in('guard_id', $last_week_ss_guard_id);
        }
        if ($last_week_ns_guard_id != NULL) {
            $this->db->where_not_in('guard_id', $last_week_ns_guard_id);
        }
        if ($last_week_gs_guard_id != NULL) {
            $this->db->where_not_in('guard_id', $last_week_gs_guard_id);
        }
        if ($same_checkpost_guards_id != NULL) {
            $this->db->where_not_in('guard_id', $same_checkpost_guards_id);
        }
        if ($field_name == VALUE_ONE) {
            $this->db->where('is_fs_done', VALUE_ZERO);
        }
        if ($field_name == VALUE_TWO) {
            $this->db->where('is_ss_done', VALUE_ZERO);
        }
        if ($field_name == VALUE_THREE) {
            $this->db->where('is_ns_done', VALUE_ZERO);
        }
        if ($field_name == VALUE_FOUR) {
            $this->db->where('is_gs_done', VALUE_ZERO);
        }
        $this->db->where('is_active !=' . VALUE_ONE);
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('guard');
        $this->db->order_by('guard_id', 'RANDOM');
        $resc = $this->db->get();
        //echo $this->db->last_query(); 
        return $resc->row_array();
    }

    function get_reserve_shift_guard_duty_list($fs_guard_id, $ss_guard_id, $ns_guard_id, $gs_guard_id, $last_week_rs_guard_id, $length) {
        $this->db->select("t.*");
        if ($length != NULL) {
            //$this->db->limit($length);
        }
        if ($fs_guard_id != NULL) {
            $this->db->where_not_in('t.guard_id', $fs_guard_id);
        }
        if ($ss_guard_id != NULL) {
            $this->db->where_not_in('t.guard_id', $ss_guard_id);
        }
        if ($ns_guard_id != NULL) {
            $this->db->where_not_in('t.guard_id', $ns_guard_id);
        }
        if ($gs_guard_id != NULL) {
            $this->db->where_not_in('t.guard_id', $gs_guard_id);
        }
        if ($last_week_rs_guard_id != NULL) {
            $this->db->where_not_in('t.guard_id', $last_week_rs_guard_id);
        }
        $this->db->where('t.is_active !=' . VALUE_ONE);
        $this->db->where('t.is_delete !=' . IS_DELETE);
        $this->db->from('guard AS t');
        $this->db->order_by('t.guard_id', 'RANDOM');
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function get_all_checkpost_distillery_list($start, $length) {
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('checkpost_distillery');
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function get_total_count_of_records_for_guard() {
        $this->db->select('COUNT(guard_id) AS total_records_for_guard');
        $this->db->where('is_active !=' . VALUE_ONE);
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('guard');
        $resc = $this->db->get();
        $record = $resc->row_array();
        return $record['total_records_for_guard'];
    }

    function get_total_count_of_records() {
        $this->db->select('COUNT(checkpost_distillery_id) AS total_records');
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('checkpost_distillery');
        $resc = $this->db->get();
        $record = $resc->row_array();
        return $record['total_records'];
    }

    function get_filter_count_of_records() {
        $this->db->select('COUNT(t.checkpost_distillery_id) AS total_records');
        $this->db->where('t.is_delete !=' . IS_DELETE);
        $this->db->from('checkpost_distillery AS t');
        $resc = $this->db->get();
        $record = $resc->row_array();
        return $record['total_records'];
    }

    function insert_guard_duty_data($guard_data) {
        $this->db->insert('guard_duty', $guard_data);
    }

    function get_all_frizee_month_guard_duty_list($start_date, $end_date) {
        $this->db->select('gd.*,cd.name');
        $this->db->where('gd.week_from', $start_date);
        $this->db->where('gd.week_to', $end_date);
        $this->db->where('gd.is_delete !=' . IS_DELETE);
        $this->db->from('guard_duty As gd');
        $this->db->join('checkpost_distillery As cd', 'cd.checkpost_distillery_id = gd.checkpost_distillery_id');
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function check_assign_guard_data($start_date, $end_date) {
        $this->db->select('COUNT(gd.guard_duty_id) AS total_guards,is_freeze');
        $this->db->where('gd.week_from', $start_date);
        $this->db->where('gd.week_to', $end_date);
        $this->db->where('gd.is_delete !=' . IS_DELETE);
        $this->db->from('guard_duty As gd');
        $resc = $this->db->get();
        return $resc->row_array();
        // $record = $resc->row_array();
        // return $record['total_guards'];
    }

    function get_last_week_frizee_data() {
        $this->db->select('week_from,week_to');
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('guard_duty');
        $this->db->order_by('week_to', 'desc');
        $this->db->limit(1);
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function get_last_month_frizee_data_list() {
        $sql = "SELECT u1.* FROM guard_duty u1 WHERE u1.week_to = (SELECT MAX(u2.week_to) FROM guard_duty u2)";
        //$sql = "SELECT * FROM guard_duty WHERE week_to >= DATE_SUB(CURDATE(),INTERVAL 1 MONTH)";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_search_guard_duty_history_list($month, $year) {
        $this->db->select('gd.*,cd.name');
        $this->db->where('MONTH(gd.week_from)', $month);
        $this->db->where('YEAR(gd.week_from)', $year);
        $this->db->where('gd.is_delete !=' . IS_DELETE);
        $this->db->from('guard_duty AS gd');
        $this->db->join('checkpost_distillery As cd', 'cd.checkpost_distillery_id = gd.checkpost_distillery_id');
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function get_last_month_same_checkpost_guard_list($checkpost_distillery_id) {
        $sql = "SELECT u1.* FROM guard_duty u1 WHERE u1.week_to = (SELECT MAX(u2.week_to) FROM guard_duty u2) AND checkpost_distillery_id = $checkpost_distillery_id";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function update_guard_shift($guard_id, $field_name) {
        $data = array(
            "$field_name" => VALUE_ONE
        );
        $this->db->where('guard_id', $guard_id);
        $this->db->update('guard', $data);
        //echo $this->db->last_query(); 
    }

    function update_all_guard_shift($data) {
        // $data = array(
        //     "is_fs_done" => VALUE_ZERO,
        //     "is_ss_done" => VALUE_ZERO,
        //     "is_ns_done" => VALUE_ZERO,
        //     "is_rs_done" => VALUE_ZERO,
        // );
        // $this->db->where('is_fs_done', VALUE_ONE);
        // $this->db->where('is_ss_done', VALUE_ONE);
        // $this->db->where('is_ns_done', VALUE_ONE);
        // $this->db->where('is_rs_done', VALUE_ONE);
        $this->db->update('guard', $data);
    }

    function get_total_guard_shift_wise() {
        $this->db->select('SUM(no_of_eg_for_first_shift) AS total_first_shift_guard,SUM(no_of_eg_for_second_shift) AS total_second_shift_guard,SUM(no_of_eg_for_night_shift) AS total_night_shift_guard,SUM(no_of_eg_for_general_shift) AS total_general_shift_guard');
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('checkpost_distillery');
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function get_total_updated_guard_shift_wise($field_name) {
        $this->db->select('SUM(is_fs_done) AS total_fs_guard,SUM(is_ss_done) AS total_ss_guard,SUM(is_ns_done) AS total_ns_guard,SUM(is_gs_done) AS total_gs_guard');
        $this->db->where("$field_name",VALUE_ONE);
        $this->db->where('is_active !=' . VALUE_ONE);
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('guard');
        $resc = $this->db->get();
        return $resc->row_array();
    }

}

/*
 * EOF: ./application/models/Guard_Duty_model.php
 */