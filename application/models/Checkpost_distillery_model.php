<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Checkpost_distillery_model extends CI_Model {

    function get_all_checkpost_distillery_list($start, $length, $search_name = '', $search_address = '', $search_nodel_persone_name = '') {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $this->db->select("t.*,date_format(t.created_time, '%d-%m-%Y %H:%i:%s') AS display_datetime");
        if ($search_name != '') {
            $this->db->like('t.name', $search_name);
        }
        if ($search_address != '') {
            $this->db->like('t.address', $search_address);
        }
        if ($search_nodel_persone_name != '') {
            $this->db->like('t.nodel_persone_name', $search_nodel_persone_name);
        }
        $this->db->limit($length, $start);
        $this->db->where('t.is_delete !=' . IS_DELETE);
        $this->db->from('checkpost_distillery AS t');
        $this->db->order_by('t.checkpost_distillery_id', 'DESC');
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function get_checkpost_distillery_by_id($checkpost_distillery_id) {
        $this->db->where('is_delete !=', IS_DELETE);
        $this->db->where('checkpost_distillery_id', $checkpost_distillery_id);
        $this->db->from('checkpost_distillery');
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function get_total_count_of_records() {
        $this->db->select('COUNT(checkpost_distillery_id) AS total_records');
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('checkpost_distillery');
        $resc = $this->db->get();
        $record = $resc->row_array();
        return $record['total_records'];
    }

    function get_filter_count_of_records($search_name = '', $search_address = '', $search_nodel_persone_name = '') {
        $this->db->select('COUNT(t.checkpost_distillery_id) AS total_records');
        if ($search_name != '') {
            $this->db->like('t.name', $search_name);
        }
        if ($search_address != '') {
            $this->db->like('t.address', $search_address);
        }
        if ($search_nodel_persone_name != '') {
            $this->db->where('t.nodel_persone_name', $search_nodel_persone_name);
        }
        $this->db->where('t.is_delete !=' . IS_DELETE);
        $this->db->from('checkpost_distillery AS t');
        $resc = $this->db->get();
        $record = $resc->row_array();
        return $record['total_records'];
    }

}

/*
 * EOF: ./application/models/Checkpost_distillery_model.php
 */