<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Utility_model extends CI_Model {

    function get_by_id($id, $compare_id, $table_name, $second_id = NULL, $second_value = NULL) {
        $this->db->where($id, $compare_id);
        if ($second_id != NULL && $second_value != NULL) {
            $this->db->where($second_id, $second_value);
        }
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from($table_name);
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function get_by_id_with_applicant_name($id, $compare_id, $table_name) {
        $this->db->select('t.*, u.applicant_name,u.user_type');
        $this->db->where("t.$id", $compare_id);
        $this->db->where('t.is_delete !=' . IS_DELETE);
        $this->db->from("$table_name AS t");
        $this->db->join('users as u', 'u.user_id = t.user_id');
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function insert_data($table_name, $table_data) {
        $this->db->insert($table_name, $table_data);
        return $this->db->insert_id();
    }

    function insert_data_batch($table_name, $table_data) {
        $this->db->insert_batch($table_name, $table_data);
    }

    function update_data($id, $id_value, $table_name, $table_data, $where_id = NULL, $where_id_text = NULL) {
        $this->db->where($id, $id_value);
        if ($where_id != NULL && $where_id_text != NULL) {
            $this->db->where($where_id, $where_id_text);
        }
        $this->db->update($table_name, $table_data);
    }

    function update_data_not_in($id, $id_value, $id2, $ids2, $table_name, $table_data, $where_id = NULL, $where_id_text = NULL) {
        $this->db->where($id, $id_value);
        $this->db->where_not_in($id2, $ids2);
        if ($where_id != NULL && $where_id_text != NULL) {
            $this->db->where($where_id, $where_id_text);
        }
        $this->db->update($table_name, $table_data);
    }

    function get_by_id_not_in($id, $compare_id, $table_name, $second_id = NULL, $second_value = NULL) {
        $this->db->where($id, $compare_id);
        if ($second_id != NULL && $second_value != NULL) {
            $this->db->where_not_in($second_id, $second_value);
        }
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from($table_name);
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function update_data_batch($id, $table_name, $table_data) {
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->update_batch($table_name, $table_data, $id);
    }

    function get_result_data($table_name, $order_by_id = NULL, $order_by = NULL) {
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from($table_name);
        if ($order_by_id != NULL && $order_by != NULL) {
            $this->db->order_by($order_by_id, $order_by);
        }
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function get_result_data_by_id($id_text, $id, $table_name, $id_text2 = NULL, $id2 = NULL) {
        $this->db->where($id_text, $id);
        if ($id_text2 != NULL && $id2 != NULL) {
            $this->db->where($id_text2, $id2);
        }
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from($table_name);
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function check_field_value_exists_or_not($field_name, $field_value, $table_name, $id = NULL, $id_value = NULL, $field_name2 = NULL, $field_value2 = NULL) {
        $this->db->where('is_delete !=', IS_DELETE);
        $this->db->where($field_name, $field_value);
        if ($field_name2 != NULL && $field_value2 != NULL) {
            $this->db->where($field_name2, $field_value2);
        }
        if ($id != NULL && $id_value != NULL) {
            $this->db->where("$id != $id_value");
        }
        $this->db->from($table_name);
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function is_valid_post_data($key_post_id, $post_id, $table_name) {
        $this->db->where($key_post_id, $post_id);
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from($table_name);
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function query_data_by_type_id($module_type, $module_id) {
        $this->db->select("q.*, date_format(q.query_datetime, '%d-%m-%Y %H:%i:%s') AS display_datetime, "
                . "qd.query_document_id, qd.doc_name, qd.document");
        $this->db->where('q.module_type', $module_type);
        $this->db->where('q.module_id', $module_id);
        $this->db->where('q.is_delete != ' . IS_DELETE);
        $this->db->from('query AS q');
        $this->db->join('query_document AS qd', 'qd.query_id = q.query_id AND qd.is_delete != ' . IS_DELETE, 'LEFT');
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function check_registration_number($field_name, $field_value, $table_name) {
        $this->db->select($field_name);
        $this->db->where($field_name, $field_value);
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from($table_name);
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function upload_document($field_name, $folder_name, $replace_name, $db_name) {
        if ($_FILES[$field_name]['name'] == '') {
            echo json_encode(array('success' => FALSE, 'message' => UPLOAD_DOC_MESSAGE));
            return;
        }
        $evidence_size = $_FILES[$field_name]['size'];
        if ($evidence_size == 0) {
            echo json_encode(array('success' => FALSE, 'message' => DOC_INVALID_SIZE_MESSAGE));
            return;
        }
//        $maxsize = '20971520';
//        if ($evidence_size >= $maxsize) {
//            echo json_encode(array('success' => FALSE, 'message' => UPLOAD_MAX_ONE_MB_MESSAGE));
//            return;
//        }

        if ($_FILES[$field_name]['name'] != '') {
            $main_path = "documents/$folder_name";
            // if (!is_dir($main_path)) {
            //     mkdir($main_path);
            //     chmod("$main_path", 0755);
            // }
            $documents_path = 'documents';
            if (!is_dir($documents_path)) {
                mkdir($documents_path);
                chmod($documents_path, 0777);
            }
            $module_path = $documents_path . DIRECTORY_SEPARATOR . "$folder_name";
            if (!is_dir($module_path)) {
                mkdir($module_path);
                chmod($module_path, 0777);
            }
            $this->load->library('upload');
            $temp_filename = str_replace('_', '', $_FILES[$field_name]['name']);
            $filename = "$replace_name" . (rand(100000000, 999999999)) . time() . '.' . pathinfo($temp_filename, PATHINFO_EXTENSION);
            //Change file name
            $final_path = $main_path . DIRECTORY_SEPARATOR . $filename;
            if (!move_uploaded_file($_FILES[$field_name]['tmp_name'], $final_path)) {
                echo json_encode(get_error_array(DOCUMENT_NOT_UPLOAD_MESSAGE));
                return;
            }
            $document_data[$db_name] = $filename;

            return $document_data;
        }
    }

    function get_result_data_approved($table_name, $order_by_id = NULL, $order_by = NULL) {
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->where('status', VALUE_FIVE);
        $this->db->from($table_name);
        if ($order_by_id != NULL && $order_by != NULL) {
            $this->db->order_by($order_by_id, $order_by);
        }
        $resc = $this->db->get();
        return $resc->result_array();
    }
    
    function get_user_data_for_query_management($id_text, $id, $table_name) {
        $this->db->select('u.mobile_number, u.email, u.user_id');
        $this->db->where('m.' . $id_text, $id);
        $this->db->where('m.is_delete !=' . IS_DELETE);
        $this->db->from($table_name . ' AS m');
        $this->db->join('users AS u', 'u.user_id = m.user_id');
        $resc = $this->db->get();
        return $resc->row_array();
    }

}

/*
 * EOF: ./application/models/Utility_model.php
 */