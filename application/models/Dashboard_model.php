<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    function get_zone_wise_industries_count($session_district) {
        if (is_labour_dept_user()) {
            $this->db->where('district', $session_district);
        }
        $this->db->from('view_zone_wise_industries');
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function get_employee_count($session_district) {
        if (is_labour_dept_user()) {
            $this->db->where('district', $session_district);
        }
        $this->db->from('view_employee');
        $resc = $this->db->get();
        return $resc->result_array();
    }

}

/*
 * EOF: ./application/models/Dashboard_model.php
 */