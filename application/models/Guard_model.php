<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Guard_model extends CI_Model {

    function get_all_guard_list($start, $length, $search_applicant_name = '', $search_mob = '') {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $this->db->select("g.*,date_format(g.created_time, '%d-%m-%Y %H:%i:%s') AS display_datetime");
        if ($search_applicant_name != '') {
            $this->db->like('g.guard_name', $search_applicant_name);
        }
        if ($search_mob != '') {
            $this->db->like('g.guard_mob', $search_mob);
        }
        $this->db->limit($length, $start);
        $this->db->where('g.is_delete !=' . IS_DELETE);
        $this->db->from('guard AS g');
        $this->db->order_by('g.guard_id', 'DESC');
        $resc = $this->db->get();
        return $resc->result_array();
    }

    function get_guard_by_id($guard_id) {
        $this->db->where('is_delete !=', IS_DELETE);
        $this->db->where('guard_id', $guard_id);
        $this->db->from('guard');
        $resc = $this->db->get();
        return $resc->row_array();
    }

    function get_total_count_of_records() {
        $this->db->select('COUNT(guard_id) AS total_records');
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('guard');
        $resc = $this->db->get();
        $record = $resc->row_array();
        return $record['total_records'];
    }
    
    function get_assign_count_of_guard() {
        $this->db->select('SUM(no_of_eg_for_first_shift+no_of_eg_for_second_shift+no_of_eg_for_night_shift+no_of_eg_for_general_shift) AS total_records');
        $this->db->where('is_delete !=' . IS_DELETE);
        $this->db->from('checkpost_distillery');
        $resc = $this->db->get();
        $record = $resc->row_array();
        return $record['total_records'];
    }

    function get_filter_count_of_records($search_applicant_name = '', $search_mob = '') {
        $this->db->select('COUNT(g.guard_id) AS total_records');
        if ($search_applicant_name != '') {
            $this->db->like('g.guard_name', $search_applicant_name);
        }
        if ($search_mob != '') {
            $this->db->like('g.guard_mob', $search_mob);
        }
        $this->db->where('g.is_delete !=' . IS_DELETE);
        $this->db->from('guard AS g');
        $resc = $this->db->get();
        $record = $resc->row_array();
        return $record['total_records'];
    }

    function get_records_for_excel() {
        $this->db->select('g.guard_id, g.guard_name,guard_designation,g.guard_mob,g.guard_email');
        $this->db->where('g.is_delete !=' . IS_DELETE);
        $this->db->from('guard AS g');
        $this->db->order_by('g.guard_id', 'DESC');
        $resc = $this->db->get();
        return $resc->result_array();
    }

}

/*
 * EOF: ./application/models/Guard_model.php
 */