<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkpost_distillery extends CI_Controller {

    function __construct() {
        parent::__construct();
        check_authenticated();
        $this->load->model('checkpost_distillery_model');
    }

    function get_checkpost_distillery_data() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $success_array = array();
        $success_array['checkpost_distillery_data'] = array();
        if ($session_user_id == NULL || !$session_user_id) {
            echo json_encode($success_array);
            return false;
        }
        $columns = $this->input->post('columns');
        $search_district = '';
        if (is_admin() || is_excise_dept_user()) {
            $search_name = trim($columns[1]['search']['value']);
            $search_address = trim($columns[2]['search']['value']);
        }
        $start = get_from_post('start');
        $length = get_from_post('length');
        $this->db->trans_start();
        $success_array['checkpost_distillery_data'] = $this->checkpost_distillery_model->get_all_checkpost_distillery_list($start, $length, $search_name, $search_address);
        $success_array['recordsTotal'] = $this->checkpost_distillery_model->get_total_count_of_records();
        if ((is_admin() || is_excise_dept_user()) || $search_name != '' || $search_address != '') {
            $success_array['recordsFiltered'] = $this->checkpost_distillery_model->get_filter_count_of_records($search_name, $search_address);
        } else {
            $success_array['recordsFiltered'] = $success_array['recordsTotal'];
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $success_array['checkpost_distillery_data'] = array();
            echo json_encode($success_array);
            return;
        }
        echo json_encode($success_array);
    }

    function get_checkpost_distillery_data_by_id() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $checkpost_distillery_id = get_from_post('checkpost_distillery_id');
        if (!$checkpost_distillery_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_start();
        $checkpost_distillery_data = $this->checkpost_distillery_model->get_checkpost_distillery_by_id($checkpost_distillery_id);

        if (empty($checkpost_distillery_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['checkpost_distillery_data'] = $checkpost_distillery_data;
        echo json_encode($success_array);
    }

    function submit_checkpost_distillery() {
        $user_id = get_from_session('temp_id_for_eims_admin');
        $module_type = get_from_post('module_type');
        if (!is_post() || $user_id == NULL || !$user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $checkpost_distillery_id = get_from_post('checkpost_distillery_id');
        $checkpost_distillery_data = $this->_get_post_data_for_checkpost_distillery();
        $validation_message = $this->_check_validation_for_checkpost_distillery($checkpost_distillery_data);
        if ($validation_message != '') {
            echo json_encode(get_error_array($validation_message));
            return false;
        }
        $this->db->trans_start();
        if (!$checkpost_distillery_id || $checkpost_distillery_id == NULL) {
            $checkpost_distillery_data['created_by'] = $user_id;
            $checkpost_distillery_data['created_time'] = date('Y-m-d H:i:s');
            $checkpost_distillery_id = $this->utility_model->insert_data('checkpost_distillery', $checkpost_distillery_data);
        } else {
            $checkpost_distillery_data['updated_by'] = $user_id;
            $checkpost_distillery_data['updated_time'] = date('Y-m-d H:i:s');
            $this->utility_model->update_data('checkpost_distillery_id', $checkpost_distillery_id, 'checkpost_distillery', $checkpost_distillery_data);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['message'] = APP_SUBMITTED_MESSAGE;
        echo json_encode($success_array);
    }

    function _get_post_data_for_checkpost_distillery() {
        $checkpost_distillery_data = array();
        $checkpost_distillery_data['name'] = get_from_post('name');
        $checkpost_distillery_data['address'] = get_from_post('address');
        $checkpost_distillery_data['shifts_category'] = get_from_post('shifts_category_for_checkpost_distillery');
        if ($checkpost_distillery_data['shifts_category'] == VALUE_ZERO) {
            $checkpost_distillery_data['no_of_eg_for_first_shift'] = get_from_post('no_of_eg_for_first_shift');
            $checkpost_distillery_data['no_of_eg_for_second_shift'] = get_from_post('no_of_eg_for_second_shift');
            $checkpost_distillery_data['no_of_eg_for_night_shift'] = get_from_post('no_of_eg_for_night_shift');
            $checkpost_distillery_data['no_of_eg_for_general_shift'] = VALUE_ZERO;
        }
        if ($checkpost_distillery_data['shifts_category'] == VALUE_ONE) {
            $checkpost_distillery_data['no_of_eg_for_general_shift'] = get_from_post('no_of_eg_for_general_shift');
            $checkpost_distillery_data['no_of_eg_for_first_shift'] = VALUE_ZERO;
            $checkpost_distillery_data['no_of_eg_for_second_shift'] = VALUE_ZERO;
            $checkpost_distillery_data['no_of_eg_for_night_shift'] = VALUE_ZERO;
        }

        return $checkpost_distillery_data;
    }

    function _check_validation_for_checkpost_distillery($checkpost_distillery_data) {
        if (!$checkpost_distillery_data['name']) {
            return CHECKPOST_NAME_MESSAGE;
        }
        if (!$checkpost_distillery_data['address']) {
            return CHECKPOST_ADDRESS_MESSAGE;
        }
        if (!$checkpost_distillery_data['shifts_category'] && $checkpost_distillery_data['shifts_category'] != VALUE_ZERO) {
            return SELECT_OPTION_MESSAGE;
        }
        if ($checkpost_distillery_data['shifts_category'] == VALUE_ZERO) {
            if (!$checkpost_distillery_data['no_of_eg_for_first_shift'] && $checkpost_distillery_data['no_of_eg_for_first_shift'] != VALUE_ZERO) {
                return EXCISE_GUARD_NO_MESSAGE;
            }
            if (!$checkpost_distillery_data['no_of_eg_for_second_shift'] && $checkpost_distillery_data['no_of_eg_for_second_shift'] != VALUE_ZERO) {
                return EXCISE_GUARD_NO_MESSAGE;
            }
            if (!$checkpost_distillery_data['no_of_eg_for_night_shift'] && $checkpost_distillery_data['no_of_eg_for_night_shift'] != VALUE_ZERO) {
                return EXCISE_GUARD_NO_MESSAGE;
            }
        }
        if ($checkpost_distillery_data['shifts_category'] == VALUE_ONE) {
            if (!$checkpost_distillery_data['no_of_eg_for_general_shift']) {
                return EXCISE_GUARD_NO_MESSAGE;
            }
        }

        return '';
    }

}

/*
 * EOF: ./application/controller/Checkpost_distillery.php
 */