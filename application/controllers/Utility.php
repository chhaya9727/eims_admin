<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Utility extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('utility_model');
    }

    function generate_new_token() {
        if (!is_post()) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        echo json_encode(get_success_array());
    }

    function get_query_data() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type = get_from_post('module_type');
        if (!$module_type) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type_array = $this->config->item('query_module_array');
        if (!isset($module_type_array[$module_type])) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $temp_access_data = $module_type_array[$module_type];
        if (empty($temp_access_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_id = get_from_post('module_id');
        if (!$module_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_start();
        $module_data = $this->utility_model->get_by_id($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text']);
        if (empty($module_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $temp_query_data = $this->utility_model->query_data_by_type_id($module_type, $module_id);
        $query_data = array();
        foreach ($temp_query_data as $qd_data) {
            if (!isset($query_data[$qd_data['query_id']])) {
                $query_data[$qd_data['query_id']] = array();
                $query_data[$qd_data['query_id']]['query_id'] = $qd_data['query_id'];
                $query_data[$qd_data['query_id']]['module_type'] = $qd_data['module_type'];
                $query_data[$qd_data['query_id']]['module_id'] = $qd_data['module_id'];
                $query_data[$qd_data['query_id']]['query_type'] = $qd_data['query_type'];
                $query_data[$qd_data['query_id']]['user_id'] = $qd_data['user_id'];
                $query_data[$qd_data['query_id']]['remarks'] = $qd_data['remarks'];
                $query_data[$qd_data['query_id']]['display_datetime'] = $qd_data['display_datetime'];
                $query_data[$qd_data['query_id']]['status'] = $qd_data['status'];
                $query_data[$qd_data['query_id']]['query_documents'] = array();
            }
            if ($qd_data['query_document_id']) {
                $tmp_doc = array();
                $tmp_doc['query_document_id'] = $qd_data['query_document_id'];
                $tmp_doc['doc_name'] = $qd_data['doc_name'];
                $tmp_doc['document'] = $qd_data['document'];
                array_push($query_data[$qd_data['query_id']]['query_documents'], $tmp_doc);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['module_data'] = $module_data;
        $success_array['query_data'] = $query_data;
        echo json_encode($success_array);
    }

    function raise_a_query() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type = get_from_post('module_type_for_query');
        if ($module_type != VALUE_ONE) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type_array = $this->config->item('query_module_array');
        if (!isset($module_type_array[$module_type])) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $temp_access_data = $module_type_array[$module_type];
        if (empty($temp_access_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_id = get_from_post('module_id_for_query');
        if (!$module_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $query_type = get_from_post('query_type_for_query');
        if ($query_type != VALUE_ONE) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $remarks = get_from_post('remarks_for_query');
        if (!$remarks) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $query_id = get_from_post('query_id_for_query');
        $this->db->trans_start();
        $insert_data = array();
        $insert_data['remarks'] = $remarks;
        $insert_data['status'] = VALUE_ONE;
        if (!$query_id || $query_id == NULL) {
            $insert_data['module_type'] = $module_type;
            $insert_data['module_id'] = $module_id;
            $insert_data['query_type'] = $query_type;
            $insert_data['user_id'] = $session_user_id;
            $insert_data['created_by'] = $session_user_id;
            $insert_data['created_time'] = date('Y-m-d H:i:s');
            $insert_data['query_datetime'] = $insert_data['created_time'];
            $insert_data['query_id'] = $this->utility_model->insert_data('query', $insert_data);
        } else {
            $insert_data['updated_by'] = $session_user_id;
            $insert_data['updated_time'] = date('Y-m-d H:i:s');
            $insert_data['query_datetime'] = $insert_data['updated_time'];
            $this->utility_model->update_data('query_id', $query_id, 'query', $insert_data);
            $insert_data['query_id'] = $query_id;
        }

        $this->_update_qd_items($session_user_id, $query_id);

        $update_data = array();
        $update_data['query_status'] = VALUE_ONE;
        $update_data['updated_by'] = $session_user_id;
        $update_data['updated_time'] = date('Y-m-d H:i:s');
        $this->utility_model->update_data($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text'], $update_data);

        $qd_data = $this->utility_model->get_result_data_by_id('query_id', $query_id, 'query_document');

        $ex_data = $this->utility_model->get_user_data_for_query_management($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text']);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $prefix_module_array = $this->config->item('prefix_module_array');
        $registration_message = 'Query Raised for your Application Number : ' . generate_registration_number($prefix_module_array[$module_type], $module_id);
        $this->load->helper('sms_helper');
        send_SMS($this, $session_user_id, $ex_data['mobile_number'], $registration_message, VALUE_FIVE);
        $this->load->library('email_lib');
        $this->email_lib->send_email($ex_data, 'Query Raised', $registration_message, VALUE_FIVE);

        $success_array = get_success_array();
        $success_array['message'] = QUERY_RAISED_MESSAGE;
        $success_array['query_status'] = VALUE_ONE;
        $success_array['query_datetime'] = convert_to_new_datetime_format($insert_data['query_datetime']);
        $success_array['query_document_data'] = $qd_data;
        echo json_encode($success_array);
    }

    function _update_qd_items($user_id, $query_id) {
        $exi_qd_items = $this->input->post('exi_qd_items');
        if ($exi_qd_items != '') {
            if (!empty($exi_qd_items)) {
                foreach ($exi_qd_items as &$value) {
                    $value['query_id'] = $query_id;
                    $value['updated_by'] = $user_id;
                    $value['updated_time'] = date('Y-m-d H:i:s');
                }
                $this->utility_model->update_data_batch('query_document_id', 'query_document', $exi_qd_items);
            }
        }
        $new_qd_items = $this->input->post('new_qd_items');
        if ($new_qd_items != '') {
            if (!empty($new_qd_items)) {
                foreach ($new_qd_items as &$value) {
                    $value['query_id'] = $query_id;
                    $value['created_by'] = $user_id;
                    $value['created_time'] = date('Y-m-d H:i:s');
                }
                $this->utility_model->insert_batch_data('query_document', $new_qd_items);
            }
        }
    }

    function resolved_query() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type = get_from_post('module_type');
        if ($module_type != VALUE_ONE) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type_array = $this->config->item('query_module_array');
        if (!isset($module_type_array[$module_type])) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $temp_access_data = $module_type_array[$module_type];
        if (empty($temp_access_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_id = get_from_post('module_id');
        if (!$module_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_start();
        $update_data = array();
        $update_data['query_status'] = VALUE_THREE;
        $update_data['updated_by'] = $session_user_id;
        $update_data['updated_time'] = date('Y-m-d H:i:s');
        $this->utility_model->update_data($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text'], $update_data);
        $ex_module_data = $this->utility_model->get_by_id($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text']);

        $ex_data = $this->utility_model->get_user_data_for_query_management($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text']);
        $prefix_module_array = $this->config->item('prefix_module_array');
        $registration_message = 'Query has been Resolved for your Application Number : ' . generate_registration_number($prefix_module_array[$module_type], $module_id);
        $this->load->helper('sms_helper');
        send_SMS($this, $session_user_id, $ex_data['mobile_number'], $registration_message, VALUE_NINE);
        $this->load->library('email_lib');
        $this->email_lib->send_email($ex_data, 'Query Resolved', $registration_message, VALUE_NINE);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['message'] = QUERY_RESOLVED_MESSAGE;
        $success_array['query_status'] = VALUE_THREE;
        $success_array['status'] = $ex_module_data['status'];
        echo json_encode($success_array);
    }

    function confirm_payment() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type = get_from_post('module_type');
        if ($module_type != VALUE_ONE) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type_array = $this->config->item('query_module_array');
        if (!isset($module_type_array[$module_type])) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $temp_access_data = $module_type_array[$module_type];
        if (empty($temp_access_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_id = get_from_post('module_id');
        if (!$module_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_start();
        $ex_module_data = $this->utility_model->get_by_id($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text']);
        if (empty($ex_module_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return;
        }
//        if ($module_type == VALUE_FOURTYONE || $module_type == VALUE_THIRTYFIVE || $module_type == VALUE_FOURTYFOUR || $module_type == VALUE_THIRTYSEVEN) {
//            if ($ex_module_data['status'] == VALUE_TWO || $ex_module_data['status'] == VALUE_THREE || $ex_module_data['status'] == VALUE_FOUR ||
//                    $ex_module_data['status'] == VALUE_EIGHT || $ex_module_data['status'] == VALUE_NINE) {
//                
//            } else {
//                echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
//                return;
//            }
//        } else {
            if ($ex_module_data['status'] == VALUE_FOUR || $ex_module_data['status'] == VALUE_EIGHT || $ex_module_data['status'] == VALUE_NINE) {
                
            } else {
                echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
                return;
            }
//        }
//        if ($ex_module_data['status'] == VALUE_FOUR || $ex_module_data['status'] == VALUE_EIGHT) {
//            
//        } else {
//            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
//            return;
//        }
        $update_data = array();
        $update_data['status'] = VALUE_SEVEN;
        $update_data['updated_by'] = $session_user_id;
        $update_data['updated_time'] = date('Y-m-d H:i:s');
        $this->utility_model->update_data($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text'], $update_data);

        $ex_data = $this->utility_model->get_user_data_for_query_management($temp_access_data['key_id_text'], $module_id, $temp_access_data['tbl_text']);
        $prefix_module_array = $this->config->item('prefix_module_array');
        $registration_message = 'Payment Confirmed for your Application Number : ' . generate_registration_number($prefix_module_array[$module_type], $module_id);
        $this->load->helper('sms_helper');
        send_SMS($this, $session_user_id, $ex_data['mobile_number'], $registration_message, VALUE_TEN);
        $this->load->library('email_lib');
        $this->email_lib->send_email($ex_data, 'Payment Confirmed', $registration_message, VALUE_TEN);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['message'] = PAYMENT_CONFIRMED_MESSAGE;
        $success_array['status'] = $update_data['status'];
        echo json_encode($success_array);
    }

    function upload_query_document() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $query_id = get_from_post('query_id_for_query');
        $query_document_id = get_from_post('query_document_id_for_query');
        if ($session_user_id == NULL || !$session_user_id) {
            echo json_encode(array('success' => FALSE, 'message' => INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_type = get_from_post('module_type_for_query');
        if ($module_type != VALUE_ONE) {
            echo json_encode(array('success' => FALSE, 'message' => 'd'));
            return false;
        }
        $module_type_array = $this->config->item('query_module_array');
        if (!isset($module_type_array[$module_type])) {
            echo json_encode(array('success' => FALSE, 'message' => INVALID_ACCESS_MESSAGE));
            return false;
        }
        $temp_access_data = $module_type_array[$module_type];
        if (empty($temp_access_data)) {
            echo json_encode(array('success' => FALSE, 'message' => INVALID_ACCESS_MESSAGE));
            return false;
        }
        $module_id = get_from_post('module_id_for_query');
        if (!$module_id) {
            echo json_encode(array('success' => FALSE, 'message' => INVALID_ACCESS_MESSAGE));
            return false;
        }
        $query_type = get_from_post('query_type_for_query');
        if ($query_type != VALUE_ONE && $query_type != VALUE_TWO) {
            echo json_encode(array('success' => FALSE, 'message' => INVALID_ACCESS_MESSAGE));
            return false;
        }

        if ($_FILES['document_for_query']['name'] == '') {
            echo json_encode(array('success' => FALSE, 'message' => UPLOAD_DOC_MESSAGE));
            return;
        }
        $evidence_size = $_FILES['document_for_query']['size'];
        if ($evidence_size == 0) {
            echo json_encode(array('success' => FALSE, 'message' => DOC_INVALID_SIZE_MESSAGE));
            return;
        }
        $maxsize = '20971520';
        if ($evidence_size >= $maxsize) {
            echo json_encode(array('success' => FALSE, 'message' => UPLOAD_MAX_ONE_MB_MESSAGE));
            return;
        }
        $path = 'documents';
        if (!is_dir($path)) {
            mkdir($path);
            chmod("$path", 0755);
        }
        $main_path = $path . DIRECTORY_SEPARATOR . 'query';
        if (!is_dir($main_path)) {
            mkdir($main_path);
            chmod("$main_path", 0755);
        }
        $this->load->library('upload');
        $temp_qd_filename = str_replace('_', '', $_FILES['document_for_query']['name']);
        $qd_filename = 'query_doc_' . (rand(10000, 99999)) . time() . '.' . pathinfo($temp_qd_filename, PATHINFO_EXTENSION);
        //Change file name
        $qd_final_path = $main_path . DIRECTORY_SEPARATOR . $qd_filename;
        if (!move_uploaded_file($_FILES['document_for_query']['tmp_name'], $qd_final_path)) {
            echo json_encode(array('success' => FALSE, 'message' => DOCUMENT_NOT_UPLOAD_MESSAGE));
            return;
        }
        $this->db->trans_start();
        $qdata = array();
        if (!$query_id || $query_id == NULL) {
            $qdata['module_type'] = $module_type;
            $qdata['module_id'] = $module_id;
            $qdata['query_type'] = $query_type;
            $qdata['user_id'] = $session_user_id;
            $qdata['created_by'] = $session_user_id;
            $qdata['created_time'] = date('Y-m-d H:i:s');
            $query_id = $this->utility_model->insert_data('query', $qdata);
        }

        $qd_data = array();
        $qd_data['document'] = $qd_filename;
        if (!$query_document_id || $query_document_id == NULL) {
            $qd_data['query_id'] = $query_id;
            $qd_data['created_by'] = $session_user_id;
            $qd_data['created_time'] = date('Y-m-d H:i:s');
            $query_document_id = $this->utility_model->insert_data('query_document', $qd_data);
        } else {
            $qd_data['updated_by'] = $session_user_id;
            $qd_data['updated_time'] = date('Y-m-d H:i:s');
            $this->utility_model->update_data('query_document_id', $query_document_id, 'query_document', $qd_data);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(array('success' => FALSE, 'message' => DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = array();
        $success_array['query_id'] = $query_id;
        $success_array['query_document_id'] = $query_document_id;
        $success_array['document_name'] = $qd_filename;
        echo json_encode($success_array);
    }

    function remove_query_document_item() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $query_document_id = get_from_post('query_document_id');
        if ($session_user_id == NULL || !$session_user_id || !$query_document_id || $query_document_id == NULL) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_start();
        $ex_data = $this->utility_model->get_by_id('query_document_id', $query_document_id, 'query_document');
        if (empty($ex_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return;
        }
        if ($ex_data['document'] != '') {
            $file_path = 'documents' . DIRECTORY_SEPARATOR . 'query' . DIRECTORY_SEPARATOR . $ex_data['document'];
            if (file_exists($file_path)) {
                unlink($file_path);
            }
        }
        $update_data = array();
        $update_data['is_delete'] = IS_DELETE;
        $update_data['updated_by'] = $session_user_id;
        $update_data['updated_time'] = date('Y-m-d H:i:s');
        $this->utility_model->update_data('query_document_id', $query_document_id, 'query_document', $update_data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['message'] = QUERY_DOCUMENT_ITEM_REMOVED_MESSAGE;
        echo json_encode($success_array);
    }

}

/*
     * EOF: ./application/controller/Utility.php
     */    