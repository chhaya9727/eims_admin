<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Guard_duty extends CI_Controller {

    function __construct() {
        parent::__construct();
        check_authenticated();
        $this->load->model('guard_duty_model');
        $this->load->model('guard_model');
    }

    function get_guard_duty_data() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $success_array = array();
        $success_array['guard_duty_data'] = array();
        $success_array['guard_reserve_duty_data'] = array();
        if ($session_user_id == NULL || !$session_user_id) {
            echo json_encode($success_array);
            return false;
        }
        $columns = $this->input->post('columns');
        $search_district = '';

        $start = get_from_post('start');
        $length = get_from_post('length');

        $month_from = convert_to_mysql_date_format(get_from_post('month_from'));
        $month_to = convert_to_mysql_date_format(get_from_post('month_to'));

        $this->db->trans_start();
        $assign_guard_data = $this->guard_duty_model->check_assign_guard_data($month_from, $month_to);
        if ($assign_guard_data['total_guards'] > VALUE_ZERO) {
            $success_array['guard_duty_data'] = $this->guard_duty_model->get_all_frizee_month_guard_duty_list($month_from, $month_to);
        } else {
            $total_guard = $this->guard_model->get_total_count_of_records();
            $checkpost_assign_guard = $this->guard_model->get_assign_count_of_guard();
            $total_assign_guard = $checkpost_assign_guard + VALUE_ONE;
            if ($total_guard < $total_assign_guard) {
                $success_array['guard_duty_data'] = array();
                echo json_encode($success_array);
                return false;
            } else {
                $checkpost_distillery_data = $this->guard_duty_model->get_all_checkpost_distillery_list($start, $length);

                //$last_month_frizee_data = $this->guard_duty_model->get_last_month_frizee_data_list();
                //print_r($last_month_frizee_data); die;
                $last_month_first_guard_id = array();
                $last_month_second_guard_id = array();
                $last_month_night_guard_id = array();
                $last_month_general_guard_id = array();
                $last_month_reserve_guard_id = array();

                // if (!empty($last_month_frizee_data)) {
                //     foreach ($last_month_frizee_data as $frow) {
                //         $first_shift_guard_id = rtrim($frow['first_shift_guard_id'], ',');
                //         $fsgids = explode(',', $first_shift_guard_id);
                //         foreach ($fsgids as $fsg) {
                //             array_push($last_month_first_guard_id, $fsg);
                //         }
                //         $second_shift_guard_id = rtrim($frow['second_shift_guard_id'], ',');
                //         $ssgids = explode(',', $second_shift_guard_id);
                //         foreach ($ssgids as $ssg) {
                //             array_push($last_month_second_guard_id, $ssg);
                //         }
                //         $night_shift_guard_id = rtrim($frow['night_shift_guard_id'], ',');
                //         $nsgids = explode(',', $night_shift_guard_id);
                //         foreach ($nsgids as $nsg) {
                //             array_push($last_month_night_guard_id, $nsg);
                //         }
                //         $reserve_shift_guard_id = rtrim($frow['reserve_shift_guard_id'], ',');
                //         $rsgids = explode(',', $reserve_shift_guard_id);
                //         foreach ($rsgids as $rsg) {
                //             array_push($last_month_reserve_guard_id, $rsg);
                //         }
                //     }
                // }
                // var_dump($last_month_frizee_data);
                // exit;

                $first_guard_id = array();
                $second_guard_id = array();
                $night_guard_id = array();
                $general_guard_id = array();
                $reserve_guard_id = array();

                $randomguardlist = array();
                $randomguardreservelist = array();

                $index = 0;
                foreach ($checkpost_distillery_data as $row) {
                    $same_checkpost_guards_id = array();
                    $same_checkpost_guards = $this->guard_duty_model->get_last_month_same_checkpost_guard_list($row['checkpost_distillery_id']);

                    if (!empty($same_checkpost_guards)) {
                        $last_month_first_shift_guard_id = rtrim($same_checkpost_guards['first_shift_guard_id'], ',');
                        $lmfsgid = explode(',', $last_month_first_shift_guard_id);
                        foreach ($lmfsgid as $lmfid) {
                            array_push($same_checkpost_guards_id, $lmfid);
                        }

                        $last_month_second_shift_guard_id = rtrim($same_checkpost_guards['second_shift_guard_id'], ',');
                        $lmssgid = explode(',', $last_month_second_shift_guard_id);
                        foreach ($lmssgid as $lmsid) {
                            array_push($same_checkpost_guards_id, $lmsid);
                        }

                        $last_month_night_shift_guard_id = rtrim($same_checkpost_guards['night_shift_guard_id'], ',');
                        $lmnsgid = explode(',', $last_month_night_shift_guard_id);
                        foreach ($lmnsgid as $lmnid) {
                            array_push($same_checkpost_guards_id, $lmnid);
                        }

                        $last_month_general_shift_guard_id = rtrim($same_checkpost_guards['general_shift_guard_id'], ',');
                        $lmgsgid = explode(',', $last_month_general_shift_guard_id);
                        foreach ($lmgsgid as $lmgid) {
                            array_push($same_checkpost_guards_id, $lmgid);
                        }
                    }

                    $randomguardlist[$index]['checkpost_distillery_id'] = $row['checkpost_distillery_id'];
                    $randomguardlist[$index]['name'] = $row['name'];
                    $first_shift_guard_name = '';
                    $first_shift_guard_id = '';
                    for ($i = 1; $i <= $row['no_of_eg_for_first_shift']; $i++) {
                        $first_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, $general_guard_id, $last_month_first_guard_id, NULL, NULL, NULL, VALUE_ONE, $same_checkpost_guards_id, VALUE_ONE);
                        if (!empty($first_shift_guard_data)) {
                            $temp_first_shift_guard_name = $first_shift_guard_data['guard_name'];
                            $temp_first_shift_guard_id = $first_shift_guard_data['guard_id'];

                            $first_shift_guard_name .= "$temp_first_shift_guard_name,";
                            $first_shift_guard_id .= "$temp_first_shift_guard_id,";

                            $randomguardlist[$index]['first_shift_guard_name'] = $first_shift_guard_name;
                            $randomguardlist[$index]['first_shift_guard_id'] = $first_shift_guard_id;

                            array_push($first_guard_id, $first_shift_guard_data['guard_id']);
                        }
                    }

                    $second_shift_guard_name = '';
                    $second_shift_guard_id = '';
                    for ($i = 1; $i <= $row['no_of_eg_for_second_shift']; $i++) {
                        $second_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, $general_guard_id, NULL, $last_month_second_guard_id, NULL, NULL, VALUE_ONE, $same_checkpost_guards_id, VALUE_TWO);
                        if (!empty($second_shift_guard_data)) {
                            $temp_second_shift_guard_name = $second_shift_guard_data['guard_name'];
                            $temp_second_shift_guard_id = $second_shift_guard_data['guard_id'];

                            $second_shift_guard_name .= "$temp_second_shift_guard_name,";
                            $second_shift_guard_id .= "$temp_second_shift_guard_id,";

                            $randomguardlist[$index]['second_shift_guard_name'] = $second_shift_guard_name;
                            $randomguardlist[$index]['second_shift_guard_id'] = $second_shift_guard_id;

                            array_push($second_guard_id, $second_shift_guard_data['guard_id']);
                        }
                    }

                    $night_shift_guard_name = '';
                    $night_shift_guard_id = '';
                    for ($i = 1; $i <= $row['no_of_eg_for_night_shift']; $i++) {
                        $night_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, $general_guard_id, NULL, NULL, $last_month_night_guard_id, NULL, VALUE_ONE, $same_checkpost_guards_id, VALUE_THREE);
                        if (!empty($night_shift_guard_data)) {
                            $temp_night_shift_guard_name = $night_shift_guard_data['guard_name'];
                            $temp_night_shift_guard_id = $night_shift_guard_data['guard_id'];

                            $night_shift_guard_name .= "$temp_night_shift_guard_name,";
                            $night_shift_guard_id .= "$temp_night_shift_guard_id,";

                            $randomguardlist[$index]['night_shift_guard_name'] = $night_shift_guard_name;
                            $randomguardlist[$index]['night_shift_guard_id'] = $night_shift_guard_id;

                            array_push($night_guard_id, $night_shift_guard_data['guard_id']);
                        }
                    }

                    $general_shift_guard_name = '';
                    $general_shift_guard_id = '';
                    for ($i = 1; $i <= $row['no_of_eg_for_general_shift']; $i++) {
                        $general_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, $general_guard_id, NULL, NULL, NULL, $last_month_general_shift_guard_id, VALUE_ONE, $same_checkpost_guards_id, VALUE_FOUR);
                        if (!empty($general_shift_guard_data)) {
                            $temp_general_shift_guard_name = $general_shift_guard_data['guard_name'];
                            $temp_general_shift_guard_id = $general_shift_guard_data['guard_id'];

                            $general_shift_guard_name .= "$temp_general_shift_guard_name,";
                            $general_shift_guard_id .= "$temp_general_shift_guard_id,";

                            $randomguardlist[$index]['general_shift_guard_name'] = $general_shift_guard_name;
                            $randomguardlist[$index]['general_shift_guard_id'] = $general_shift_guard_id;

                            array_push($general_guard_id, $general_shift_guard_data['guard_id']);
                        }
                    }
                    $index++;
                }
                $grindex = 0;
                $reserve_shift_guard_name = '';
                $reserve_shift_guard_id = '';
                $reserve_shift_guard_data = $this->guard_duty_model->get_reserve_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, $general_guard_id, $last_month_reserve_guard_id, VALUE_THREE);
                if (!empty($reserve_shift_guard_data)) {
                    foreach ($reserve_shift_guard_data as $r) {
                        $randomguardlist[$grindex]['reserve_shift_guard_name'] = $r['guard_name'];
                        $randomguardlist[$grindex]['reserve_shift_guard_id'] = $r['guard_id'];
                        $grindex++;
                    }
                }
                $success_array['guard_duty_data'] = $randomguardlist;
            }

            $success_array['recordsTotal'] = $this->guard_duty_model->get_total_count_of_records_for_guard();
            if ((is_admin() || is_excise_dept_user()) || $search_duty_point != '' || $search_guard_name_first != '' || $search_guard_name_second || $search_guard_name_night != '') {
                $success_array['recordsFiltered'] = $this->guard_duty_model->get_filter_count_of_records();
            } else {
                $success_array['recordsFiltered'] = $success_array['recordsTotal'];
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $success_array['guard_duty_data'] = array();
            echo json_encode($success_array);
            return;
        }
        echo json_encode($success_array);
    }

    public function submit_guard_duty() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if ($session_user_id == null || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $guardData = $this->input->post('guardData');
        //print_r($guardData);
        $first_shift_guard_id_str = '';
        $second_shift_guard_id_str = '';
        $night_shift_guard_id_str = '';
        $general_shift_guard_id_str = '';
        $reserve_shift_guard_id_str = '';

        if (!empty($guardData)) {
            foreach ($guardData as &$row) {
                $first_shift_guard_id_str .= $row["first_shift_guard_id"];
                $second_shift_guard_id_str .= $row["second_shift_guard_id"];
                $night_shift_guard_id_str .= $row["night_shift_guard_id"];
                $general_shift_guard_id_str .= $row["general_shift_guard_id"];
                $reserve_shift_guard_id_str .= $row["reserve_shift_guard_id"] . ",";
                $row['checkpost_distillery_id'] = $row["checkpost_distillery_id"];
                $row['first_shift_guard_id'] = $row["first_shift_guard_id"] == 'undefined' ? '-' : $row["first_shift_guard_id"];
                $row['second_shift_guard_id'] = $row["second_shift_guard_id"] == 'undefined' ? '-' : $row["second_shift_guard_id"];
                $row['night_shift_guard_id'] = $row["night_shift_guard_id"] == 'undefined' ? '-' : $row["night_shift_guard_id"];
                $row['general_shift_guard_id'] = $row["general_shift_guard_id"] == 'undefined' ? '-' : $row["general_shift_guard_id"];
                $row['reserve_shift_guard_id'] = $row["reserve_shift_guard_id"] == 'undefined' ? '-' : $row["reserve_shift_guard_id"];
                $row['first_shift_guard_name'] = $row["first_shift_guard_name"] == 'undefined' ? '-' : $row["first_shift_guard_name"];
                $row['second_shift_guard_name'] = $row["second_shift_guard_name"] == 'undefined' ? '-' : $row["second_shift_guard_name"];
                $row['night_shift_guard_name'] = $row["night_shift_guard_name"] == 'undefined' ? '-' : $row["night_shift_guard_name"];
                $row['general_shift_guard_name'] = $row["general_shift_guard_name"] == 'undefined' ? '-' : $row["general_shift_guard_name"];
                $row['reserve_shift_guard_name'] = $row["reserve_shift_guard_name"] == 'undefined' ? '-' : $row["reserve_shift_guard_name"];
                $row['is_freeze '] = VALUE_ONE;
                $row['week_from'] = convert_to_mysql_date_format($row['week_from']);
                $row['week_to'] = convert_to_mysql_date_format($row['week_to']);
                $row['created_by'] = $session_user_id;
                $row['created_time'] = date('Y-m-d H:i:s');
            }
            $this->utility_model->insert_data_batch('guard_duty', $guardData);
        }

        $fsgis = explode(",", $first_shift_guard_id_str);
        foreach ($fsgis as $fs) {
            $fs = trim($fs);
            $this->guard_duty_model->update_guard_shift($fs, 'is_fs_done');
        }
        $ssgis = explode(",", $second_shift_guard_id_str);
        foreach ($ssgis as $ss) {
            $ss = trim($ss);
            $this->guard_duty_model->update_guard_shift($ss, 'is_ss_done');
        }
        $nsgis = explode(",", $night_shift_guard_id_str);
        foreach ($nsgis as $ns) {
            $ns = trim($ns);
            $this->guard_duty_model->update_guard_shift($ns, 'is_ns_done');
        }
        $gsgis = explode(",", $general_shift_guard_id_str);
        foreach ($gsgis as $gs) {
            $gs = trim($gs);
            $this->guard_duty_model->update_guard_shift($gs, 'is_gs_done');
        }
//        $rsgis = explode(",", $reserve_shift_guard_id_str);
//        foreach($rsgis as $rs) {
//            $rs = trim($rs);
//            $this->guard_duty_model->update_guard_shift($rs,'is_rs_done');
//        }
        $total_guard_shift_wise = $this->guard_duty_model->get_total_guard_shift_wise();
        $total_updated_first_shift_guard_cp_wise = $this->guard_duty_model->get_total_updated_guard_shift_wise('is_fs_done');
        $total_updated_second_shift_guard_cp_wise = $this->guard_duty_model->get_total_updated_guard_shift_wise('is_ss_done');
        $total_updated_night_shift_guard_cp_wise = $this->guard_duty_model->get_total_updated_guard_shift_wise('is_ns_done');
        $total_updated_general_shift_guard_cp_wise = $this->guard_duty_model->get_total_updated_guard_shift_wise('is_gs_done');

        $update_data = array();
        if ($total_guard_shift_wise['total_first_shift_guard'] <= $total_updated_first_shift_guard_cp_wise['total_fs_guard']) {
            $update_data['is_fs_done'] = VALUE_ZERO;
            $this->guard_duty_model->update_all_guard_shift($update_data);
        }
        if ($total_guard_shift_wise['total_second_shift_guard'] <= $total_updated_second_shift_guard_cp_wise['total_ss_guard']) {
            $update_data['is_ss_done'] = VALUE_ZERO;
            $this->guard_duty_model->update_all_guard_shift($update_data);
        }
        if ($total_guard_shift_wise['total_night_shift_guard'] <= $total_updated_night_shift_guard_cp_wise['total_ns_guard']) {
            $update_data['is_ns_done'] = VALUE_ZERO;
//            $update_data['is_rs_done'] = VALUE_ZERO;
            $this->guard_duty_model->update_all_guard_shift($update_data);
        }
        if ($total_guard_shift_wise['total_general_shift_guard'] <= $total_updated_general_shift_guard_cp_wise['total_gs_guard']) {
            $update_data['is_gs_done'] = VALUE_ZERO;
            $update_data['is_rs_done'] = VALUE_ZERO;
            $this->guard_duty_model->update_all_guard_shift($update_data);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array['message'] = 'Duty Freeze Successfully.';
        echo json_encode($success_array);
    }

    function get_curr_week_guard_duty_data() {

        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $success_array = array();
        $success_array['curr_week_guard_duty_data'] = array();
        if ($session_user_id == NULL || !$session_user_id) {
            echo json_encode($success_array);
            return false;
        }
        $columns = $this->input->post('columns');
        $search_district = '';
        if (is_admin() || is_excise_dept_user()) {
            $search_name = trim($columns[2]['search']['value']);
            $search_address = trim($columns[3]['search']['value']);
        }
        $start = get_from_post('start');
        $length = get_from_post('length');

        // $monday = strtotime('next Sunday -1 week');
        // $monday = date('w', $monday) == date('w') ? strtotime(date("Y-m-d", $monday) . " +7 days") : $monday;
        // $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");
        // $start_date = date("Y-m-d", $monday);
        // $end_date = date("Y-m-d", $sunday);

        $start_date = date('Y-m-01');
        $end_date = date('Y-m-t');

        $this->db->trans_start();
        $success_array['curr_week_guard_duty_data'] = $this->guard_duty_model->get_all_curr_week_guard_duty_list($start_date, $end_date);
        $success_array['recordsTotal'] = $this->guard_duty_model->get_total_count_of_records();
        if ((is_admin() || is_excise_dept_user()) || $search_name != '' || $search_address != '') {
            $success_array['recordsFiltered'] = $this->guard_duty_model->get_filter_count_of_records($search_name, $search_address);
        } else {
            $success_array['recordsFiltered'] = $success_array['recordsTotal'];
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $success_array['curr_week_guard_duty_data'] = array();
            echo json_encode($success_array);
            return;
        }
        echo json_encode($success_array);
    }

    function get_frizee_data_status() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $week_from = convert_to_mysql_date_format(get_from_post('week_from'));
        $week_to = convert_to_mysql_date_format(get_from_post('week_to'));
        $btn_status = true;
        $this->db->trans_start();
        $frizee_data_status = $this->guard_duty_model->check_assign_guard_data($week_from, $week_to);
        $last_week_frizee_data = $this->guard_duty_model->get_last_week_frizee_data();

        $total_guard = $this->guard_model->get_total_count_of_records();
        $checkpost_assign_guard = $this->guard_model->get_assign_count_of_guard();
        $total_assign_guard = $checkpost_assign_guard + VALUE_TWO;
        if ($total_guard < $total_assign_guard) {
            $btn_status = false;
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['frizee_data_status'] = $frizee_data_status;
        $success_array['last_week_frizee_data'] = $last_week_frizee_data;
        $success_array['btn_status'] = $btn_status;
        echo json_encode($success_array);
    }

    function get_all_frizee_data_status() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $prev_month_from = convert_to_mysql_date_format(get_from_post('prev_month_from'));
        $prev_month_to = convert_to_mysql_date_format(get_from_post('prev_month_to'));
        $curr_month_from = convert_to_mysql_date_format(get_from_post('curr_month_from'));
        $curr_month_to = convert_to_mysql_date_format(get_from_post('curr_month_to'));
        $next_month_from = convert_to_mysql_date_format(get_from_post('next_month_from'));
        $next_month_to = convert_to_mysql_date_format(get_from_post('next_month_to'));
        $btn_status_prev_month = true;
        $btn_status_curr_month = true;
        $btn_status_next_month = true;

        $this->db->trans_start();
        $frizee_data_status_prev_month = $this->guard_duty_model->check_assign_guard_data($prev_month_from, $prev_month_to);
        $frizee_data_status_curr_month = $this->guard_duty_model->check_assign_guard_data($curr_month_from, $curr_month_to);
        $frizee_data_status_next_month = $this->guard_duty_model->check_assign_guard_data($next_month_from, $next_month_to);

        $total_guard = $this->guard_model->get_total_count_of_records();
        $checkpost_assign_guard = $this->guard_model->get_assign_count_of_guard();
        $total_assign_guard = $checkpost_assign_guard + VALUE_TWO;
        if ($total_guard < $total_assign_guard) {
            //$btn_status = false;

            if ($frizee_data_status_prev_month['is_freeze'] != VALUE_ONE) {
                $btn_status_prev_month = false;
            }

            if ($frizee_data_status_curr_month['is_freeze'] != VALUE_ONE) {
                $btn_status_curr_month = false;
            }

            if ($frizee_data_status_next_month['is_freeze'] != VALUE_ONE) {
                $btn_status_next_month = false;
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['frizee_data_status_prev_month'] = $frizee_data_status_prev_month;
        $success_array['frizee_data_status_curr_month'] = $frizee_data_status_curr_month;
        $success_array['frizee_data_status_next_month'] = $frizee_data_status_next_month;
        $success_array['btn_status_prev_month'] = $btn_status_prev_month;
        $success_array['btn_status_curr_month'] = $btn_status_curr_month;
        $success_array['btn_status_next_month'] = $btn_status_next_month;
        echo json_encode($success_array);
    }

    function generate_report() {
        $user_id = get_from_session('temp_id_for_eims_admin');
        $week_from = convert_to_mysql_date_format(get_from_post('week_from_for_report'));
        $week_to = convert_to_mysql_date_format(get_from_post('week_to_for_report'));
        if (!is_post() || $user_id == null || !$user_id || $week_from == null || !$week_from ||
                $week_to == null || !$week_to) {
            print_r(INVALID_ACCESS_MESSAGE);
            return false;
        }
        $this->db->trans_start();
        $existing_guard_duty_data = $this->guard_duty_model->get_all_frizee_month_guard_duty_list($week_from, $week_to);
        if (empty($existing_guard_duty_data)) {
            print_r(INVALID_ACCESS_MESSAGE);
            return;
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            print_r(DATABASE_ERROR_MESSAGE);
            return;
        }
        error_reporting(E_ERROR);
        $data = array('guard_duty_data' => $existing_guard_duty_data);
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'Legal']);
        $mpdf->setFooter('{PAGENO} / {nb}');
        $mpdf->WriteHTML($this->load->view('guard_duty/report', $data, TRUE));
        $mpdf->Output('guard_duty_' . time() . '.pdf', 'I');
    }

    function get_guard_duty_history_data() {

        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $success_array = array();
        $success_array['guard_duty_history_data'] = array();
        if ($session_user_id == NULL || !$session_user_id) {
            echo json_encode($success_array);
            return false;
        }
        $start = get_from_post('start');
        $length = get_from_post('length');

        $history_date = get_from_post('history_date');
        $hdate = explode('-', $history_date);
        $month = $hdate[0];
        $year = $hdate[1];

        $this->db->trans_start();
        $success_array['guard_duty_history_data'] = $this->guard_duty_model->get_search_guard_duty_history_list($month, $year);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $success_array['guard_duty_history_data'] = array();
            echo json_encode($success_array);
            return;
        }
        echo json_encode($success_array);
    }

}

/*
 * EOF: ./application/controller/Guard_duty.php
 */