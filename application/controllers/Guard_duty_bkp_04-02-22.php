<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Guard_duty extends CI_Controller {

    function __construct() {
        parent::__construct();
        check_authenticated();
        $this->load->model('guard_duty_model');
    }

    function get_guard_duty_data() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $success_array = array();
        $success_array['guard_duty_data'] = array();
        $success_array['guard_reserve_duty_data'] = array();
        if ($session_user_id == NULL || !$session_user_id) {
            echo json_encode($success_array);
            return false;
        }
        $columns = $this->input->post('columns');
        $search_district = '';
//        if (is_admin()) {
//            $search_duty_point = trim($columns[2]['search']['value']);
//            $search_guard_name_first = trim($columns[3]['search']['value']);
//            $search_guard_name_second = trim($columns[4]['search']['value']);
//            $search_guard_name_night = trim($columns[5]['search']['value']);
//            //$search_guard_name_reserve = trim($columns[6]['search']['value']);
//        }
        $start = get_from_post('start');
        $length = get_from_post('length');
        $this->db->trans_start();
        $checkpost_distillery_data = $this->guard_duty_model->get_all_checkpost_distillery_list($start, $length);
        $total_records = $this->guard_duty_model->get_total_count_of_records();
        $data_limit = ($total_records);

//        $fs_guard_id = array();
//        $ss_guard_id = array();
//        $ns_guard_id = array();
//        $rs_guard_id = array();

        $first_guard_id = array();
        $second_guard_id = array();
        $night_guard_id = array();
        $reserve_guard_id = array();

//        $second_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($data_limit,$data_limit+1);
//        $night_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($data_limit,$data_limit+$data_limit);
//        $reserve_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($data_limit,$data_limit+$data_limit);

        $randomguardlist = array();
        $randomguardreservelist = array();
//        $first_shift_guard = array();
//        $second_shift_guard = array();
//        $night_shift_guard = array();
//        $reserve_guard = array();

        $index = 0;
        foreach ($checkpost_distillery_data as $row) {
            //echo $row['name'];
            $randomguardlist[$index]['checkpost_distillery_id'] = $row['checkpost_distillery_id'];
            $randomguardlist[$index]['name'] = $row['name'];
            //$no_of_guard_first_shift = $row['no_of_eg_for_first_shift'];
            $first_shift_guard_name = '';
            for ($i = 1; $i <= $row['no_of_eg_for_first_shift']; $i++) {
                $first_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, VALUE_ONE);
                $temp_first_shift_guard_name = $first_shift_guard_data['guard_name'];
                $first_shift_guard_name .= "$temp_first_shift_guard_name,";
                $randomguardlist[$index]['first_shift_guard_name'] = $first_shift_guard_name;
                array_push($first_guard_id, $first_shift_guard_data['guard_id']);
            }

            $second_shift_guard_name = '';
            for ($i = 1; $i <= $row['no_of_eg_for_second_shift']; $i++) {
                $second_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, VALUE_ONE);
                $temp_second_shift_guard_name = $second_shift_guard_data['guard_name'];
                $second_shift_guard_name .= "$temp_second_shift_guard_name,";
                $randomguardlist[$index]['second_shift_guard_name'] = $second_shift_guard_name;
                array_push($second_guard_id, $second_shift_guard_data['guard_id']);
            }

            $night_shift_guard_name = '';
            for ($i = 1; $i <= $row['no_of_eg_for_night_shift']; $i++) {
                $night_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, VALUE_ONE);
                $temp_night_shift_guard_name = $night_shift_guard_data['guard_name'];
                $night_shift_guard_name .= "$temp_night_shift_guard_name,";
                $randomguardlist[$index]['night_shift_guard_name'] = $night_shift_guard_name;
                array_push($night_guard_id, $night_shift_guard_data['guard_id']);
            }

            $index++;
        }

        $grindex = 0;
        $reserve_shift_guard_name = '';
        //for($i=1;$i<=$row['no_of_eg_for_night_shift'];$i++){
        $reserve_shift_guard_data = $this->guard_duty_model->get_reserve_shift_guard_duty_list($first_guard_id, $second_guard_id, $night_guard_id, VALUE_FIVE);
        foreach ($reserve_shift_guard_data as $r) {
           
                $randomguardlist[$grindex]['reserve_guard_name'] = $r['guard_name'];
                //array_push($reserve_guard_id, $r['guard_id']);
            //}
            
            // array_push($rs_guard_id, $r['guard_id']);
            
            $grindex++;
        }

        ///////////////////////
        // $gfindex = 0;
        // $first_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list(NULL, $ss_guard_id, $ns_guard_id, $data_limit);
        // foreach ($first_shift_guard_data as $f) {
        //     $randomguardlist[$gfindex]['first_shift_guard_name'] = $f['guard_name'];
        //     array_push($fs_guard_id, $f['guard_id']);
        //     $gfindex++;
        // }
        // $gsindex = 0;
        // $second_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($fs_guard_id, NULL, $ns_guard_id, $data_limit);
        // foreach ($second_shift_guard_data as $s) {
        //     $randomguardlist[$gsindex]['second_shift_guard_name'] = $s['guard_name'];
        //     array_push($ss_guard_id, $s['guard_id']);
        //     $gsindex++;
        // }
        // $gnindex = 0;
        // $night_shift_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($fs_guard_id, $ss_guard_id, NULL, $data_limit);
        // foreach ($night_shift_guard_data as $n) {
        //     $randomguardlist[$gnindex]['night_shift_guard_name'] = $n['guard_name'];
        //     array_push($ns_guard_id, $n['guard_id']);
        //     $gnindex++;
        // }
        // $grindex = 0;
        // $reserve_guard_data = $this->guard_duty_model->get_all_shift_guard_duty_list($fs_guard_id, $ss_guard_id, $ns_guard_id, null);
        // foreach ($reserve_guard_data as $r) {
        //     $randomguardreservelist[$grindex]['reserve_guard_name'] = $r['guard_name'];
        //     array_push($rs_guard_id, $r['guard_id']);
        //     $grindex++;
        // }
        ///////////////////////////
//        $gfindex = 0;
//        foreach ($first_shift_guard_data as $grow) {
//            if ((!in_array($grow['guard_name'], $second_shift_guard)) && (!in_array($grow['guard_name'], $night_shift_guard)) && (!in_array($grow['guard_name'], $reserve_guard))) {
//                $randomguardlist[$gfindex]['first_shift_guard_name'] = $grow['guard_name'];
//                array_push($first_shift_guard, $grow['guard_name']);
//                //$randomguardlist[$index]['first_shift_guard_name'] = $grow['guard_name'];
//            } else {
//                continue;
//            }
//            $gfindex++;
//        }
//        $gsindex = 0;
//        foreach ($second_shift_guard_data as $grow) {
//            if ((!in_array($grow['guard_name'], $first_shift_guard)) && (!in_array($grow['guard_name'], $night_shift_guard)) && (!in_array($grow['guard_name'], $reserve_guard))) {
//                $randomguardlist[$gsindex]['second_shift_guard_name'] = $grow['guard_name'];
//                array_push($second_shift_guard, $grow['guard_name']);
//            }
//            $gsindex++;
//        }
//        $gnindex = 0;
//        foreach ($night_shift_guard_data as $grow) {
//            if ((!in_array($grow['guard_name'], $first_shift_guard)) && (!in_array($grow['guard_name'], $second_shift_guard)) && (!in_array($grow['guard_name'], $reserve_guard))) {
//                $randomguardlist[$gnindex]['night_shift_guard_name'] = $grow['guard_name'];
//                array_push($night_shift_guard, $grow['guard_name']);
//            } else {
//                continue;
//            }
//            $gnindex++;
//        }
//        $grindex = 0;
//        foreach ($reserve_guard_data as $grow) {
//            if ((!in_array($grow['guard_name'], $first_shift_guard)) && (!in_array($grow['guard_name'], $second_shift_guard)) && (!in_array($grow['guard_name'], $night_shift_guard))) {
//                $randomguardlist[$grindex]['reserve_guard_name'] = $grow['guard_name'];
//                array_push($reserve_guard, $grow['guard_name']);
//            } else {
//                continue;
//            }
//            $grindex++;
//        }
        //if($gindex == 1)
        //$randomguardlist[$index]['first_shift_guard_name'] = $grow['guard_name'];
        //if($gindex == 2)
        //$randomguardlist[$index]['second_shift_guard_name'] = $grow['guard_name'];
        //if($gindex == 3)
        //$randomguardlist[$index]['night_shift_guard_name'] = $grow['guard_name'];
        //     $gindex++;
        // }
        // var_dump($randomguardreservelist);
        //     exit;
        $success_array['guard_duty_data'] = $randomguardlist;
        //$success_array['guard_reserve_duty_data'] = $randomguardreservelist;
        // $success_array['guard_reserve_duty_data'] = $randomguardreservelist;
        // $success_array['guard_duty_data'] = $this->guard_duty_model->get_all_guard_duty_list($start, $length, $search_duty_point, $search_guard_name_first, $search_guard_name_second ,$search_guard_name_night);
        $success_array['recordsTotal'] = $this->guard_duty_model->get_total_count_of_records();
        if ((is_admin()) || $search_duty_point != '' || $search_guard_name_first != '' || $search_guard_name_second || $search_guard_name_night != '') {
            $success_array['recordsFiltered'] = $this->guard_duty_model->get_filter_count_of_records();
        } else {
            $success_array['recordsFiltered'] = $success_array['recordsTotal'];
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $success_array['guard_duty_data'] = array();
            $success_array['guard_reserve_duty_data'] = array();
            echo json_encode($success_array);
            return;
        }
        echo json_encode($success_array);
    }
    public function submit_guard_duty(){
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if ($session_user_id == null || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $guardData = $this->input->post('guardData');

        foreach ($guardData as $row) {
            $guard_data['checkpost_distillery_id'] = $row["checkpostId"];
            $guard_data['first_shift_guard_name'] = $row["firstShiftGuardName"];
            $guard_data['second_shift_guard_name'] = $row["secondShiftGuardName"];
            $guard_data['night_shift_guard_name'] = $row["nightShiftGuardName"] == 'undefined' ? '-' : $row["nightShiftGuardName"];
            $guard_data['week_from'] =convert_to_mysql_date_format($row['weekFrom']);//time($row['weekFrom']));
            $guard_data['week_to'] = convert_to_mysql_date_format($row['weekTo']);//date('Y-m-d',strtotime($row['weekTo']));
            $guard_data['created_by'] = $session_user_id;
            $guard_data['created_time'] = date('Y-m-d H:i:s');
            $this->guard_duty_model->insert_guard_duty_data($guard_data);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array['message'] = 'Duty Freeze Successfully.';
        echo json_encode($success_array);
    }
    function get_curr_week_guard_duty_data() {
        // $user_id = get_from_session('temp_id_for_eims_admin');
        // $success_array = get_success_array();
        // $success_array['curr_week_guard_duty_data'] = array();
        // if ($user_id == NULL || !$user_id) {
        //     echo json_encode($success_array);
        //     return false;
        // }

        // $monday = strtotime('next Sunday -1 week');
        // $monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
        // $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
        // $start_date = date("Y-m-d",$monday);
        // $end_date =  date("Y-m-d",$sunday);

        // $this->db->trans_start();
        // $success_array['curr_week_guard_duty_data'] = $this->guard_duty_model->get_all_curr_week_guard_duty_list($start_date,$end_date);
        // $this->db->trans_complete();
        // if ($this->db->trans_status() === FALSE) {
        //     $success_array['curr_week_guard_duty_data'] = array();
        //     echo json_encode($success_array);
        //     return;
        // }
        // echo json_encode($success_array);


        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $success_array = array();
        $success_array['curr_week_guard_duty_data'] = array();
        if ($session_user_id == NULL || !$session_user_id) {
            echo json_encode($success_array);
            return false;
        }
        $columns = $this->input->post('columns');
        $search_district = '';
        if (is_admin()) {
            $search_name = trim($columns[2]['search']['value']);
            $search_address = trim($columns[3]['search']['value']);
            //$search_nodel_persone_name = trim($columns[4]['search']['value']);
        }
        $start = get_from_post('start');
        $length = get_from_post('length');

        $monday = strtotime('next Sunday -1 week');
        $monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
        $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
        $start_date = date("Y-m-d",$monday);
        $end_date =  date("Y-m-d",$sunday);
        
        $this->db->trans_start();
        $success_array['curr_week_guard_duty_data'] = $this->guard_duty_model->get_all_curr_week_guard_duty_list($start_date,$end_date);
        $success_array['recordsTotal'] = $this->guard_duty_model->get_total_count_of_records();
        if ((is_admin()) || $search_name != '' || $search_address != '') {
            $success_array['recordsFiltered'] = $this->guard_duty_model->get_filter_count_of_records($search_name, $search_address);
        } else {
            $success_array['recordsFiltered'] = $success_array['recordsTotal'];
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $success_array['curr_week_guard_duty_data'] = array();
            echo json_encode($success_array);
            return;
        }
        echo json_encode($success_array);
    }
}

/*
 * EOF: ./application/controller/BOCW.php
 */