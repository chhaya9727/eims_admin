<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Guard extends CI_Controller {

    function __construct() {
        parent::__construct();
        check_authenticated();
        $this->load->model('guard_model');
    }

    function get_guard_data() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $success_array = array();
        $success_array['guard_data'] = array();
        if ($session_user_id == NULL || !$session_user_id) {
            echo json_encode($success_array);
            return false;
        }
        $session_district = get_from_session('temp_district_for_eims_admin');
        $columns = $this->input->post('columns');
        $search_district = '';
        if (is_admin() || is_excise_dept_user()) {
            $search_applicant_name = trim($columns[2]['search']['value']);
            $search_mob = trim($columns[3]['search']['value']);
        } else {
            $search_applicant_name = trim($columns[2]['search']['value']);
            $search_mob = trim($columns[3]['search']['value']);
        }
        $start = get_from_post('start');
        $length = get_from_post('length');
        $this->db->trans_start();
        $success_array['guard_data'] = $this->guard_model->get_all_guard_list($start, $length, $search_applicant_name, $search_mob);
        $success_array['recordsTotal'] = $this->guard_model->get_total_count_of_records($search_mob);
        if ($search_applicant_name != '' || $search_mob != '') {
            $success_array['recordsFiltered'] = $this->guard_model->get_filter_count_of_records($search_applicant_name, $search_mob);
        } else {
            $success_array['recordsFiltered'] = $success_array['recordsTotal'];
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $success_array['guard_data'] = array();
            echo json_encode($success_array);
            return;
        }
        echo json_encode($success_array);
    }

    function get_guard_data_by_id() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $guard_id = get_from_post('guard_id');
        if (!$guard_id) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_start();
        $guard_data = $this->guard_model->get_guard_by_id($guard_id);
        if (empty($guard_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['guard_data'] = $guard_data;
        echo json_encode($success_array);
    }

    function submit_guard() {
        $user_id = get_from_session('temp_id_for_eims_admin');
        $module_type = get_from_post('module_type');
        if (!is_post() || $user_id == NULL || !$user_id || ($module_type != VALUE_ONE && $module_type != VALUE_TWO)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $guard_id = get_from_post('guard_id');
        $guard_data = $this->_get_post_data_for_guard();
        $validation_message = $this->_check_validation_for_guard($guard_data);
        if ($validation_message != '') {
            echo json_encode(get_error_array($validation_message));
            return false;
        }
        $this->db->trans_start();
        $guard_data['status'] = $module_type;
        if ($module_type == VALUE_TWO) {
            $guard_data['submitted_datetime'] = date('Y-m-d H:i:s');
        }
        if (!$guard_id || $guard_id == NULL) {
            $guard_data['created_by'] = $user_id;
            $guard_data['user_id'] = $user_id;
            $guard_data['created_time'] = date('Y-m-d H:i:s');
            $guard_id = $this->utility_model->insert_data('guard', $guard_data);
        } else {
            $guard_data['updated_by'] = $user_id;
            $guard_data['updated_time'] = date('Y-m-d H:i:s');
            $this->utility_model->update_data('guard_id', $guard_id, 'guard', $guard_data);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $success_array = get_success_array();
        $success_array['message'] = $module_type == VALUE_ONE ? APP_DRAFT_MESSAGE : APP_SUBMITTED_MESSAGE;
        echo json_encode($success_array);
    }

    function _get_post_data_for_guard() {
        $guard_data = array();
        $guard_data['guard_name'] = get_from_post('guard_name');
        $guard_data['guard_designation'] = get_from_post('guard_designation');
        $guard_data['guard_mob'] = get_from_post('guard_mob');
        $guard_data['guard_email'] = get_from_post('guard_email');

        return $guard_data;
    }

    function _check_validation_for_guard($guard_data) {
        if (!$guard_data['guard_name']) {
            return NAME_MESSAGE;
        }
        if (!$guard_data['guard_designation']) {
            return DESIGNATION_MESSAGE;
        }
        if (!$guard_data['guard_mob']) {
            return MOBILE_NUMBER_MESSAGE;
        }
        if (!$guard_data['guard_email']) {
            return EMAIL_MESSAGE;
        }

        return '';
    }

//    function get_guard_data_by_guard_id() {
//        $session_user_id = get_from_session('temp_id_for_eims_admin');
//        if (!is_post() || $session_user_id == NULL || !$session_user_id) {
//            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
//            return false;
//        }
//        $guard_id = get_from_post('guard_id');
//        if (!$guard_id) {
//            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
//            return false;
//        }
//        $this->db->trans_start();
//        $guard_data = $this->utility_model->get_by_id_with_applicant_name('guard_id', $guard_id, 'guard');
//        if (empty($guard_data)) {
//            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
//            return false;
//        }
//        $success_array = get_success_array();
//        $success_array['guard_data'] = $guard_data;
//        echo json_encode($success_array);
//    }

    function generate_excel() {
        $user_id = get_from_session('temp_id_for_eims_admin');
        if (!is_post() || $user_id == null || !$user_id) {
            print_r(INVALID_ACCESS_MESSAGE);
            return false;
        }
        $session_district = get_from_session('temp_district_for_eims_admin');
        $this->db->trans_start();
        $excel_data = $this->guard_model->get_records_for_excel($session_district);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            print_r(INVALID_ACCESS_MESSAGE);
            return;
        }
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=guard_Report_' . date('Y-m-d H:i:s') . '.csv');
        $output = fopen("php://output", "w");
        fputcsv($output, array('Application Number', 'Guard Name', 'Guard Designation', 'Guard Mobile Number', 'Gurad Email-ID'));
        if (!empty($excel_data)) {
            $prefix_module_array = $this->config->item('prefix_module_array');
            foreach ($excel_data as $list) {
                $prefix = isset($prefix_module_array[VALUE_TWO]) ? $prefix_module_array[VALUE_TWO] : '';
                $list['guard_id'] = generate_registration_number($prefix, $list['guard_id']);
                $list['submitted_datetime'] = isset($list['submitted_datetime']) ? (convert_to_new_datetime_format($list['submitted_datetime'])) : '';
                fputcsv($output, $list);
            }
        }
        fclose($output);
    }

    function active_deactive_guard() {
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $guard_id = get_from_post('guard_id');
        $type = get_from_post('type');
        if (!is_post() || $guard_id == NULL || !$guard_id || (!is_excise_dept_user() && !is_admin()) || $session_user_id == NULL || !$session_user_id || ($type != IS_ACTIVE && $type != IS_DEACTIVE)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }
        $this->db->trans_start();
        $guard_data = $this->utility_model->get_by_id('guard_id', $guard_id, 'guard');
        if (empty($guard_data)) {
            echo json_encode(get_error_array(INVALID_ACCESS_MESSAGE));
            return false;
        }

        if ($type != VALUE_ZERO) {
            $total_guard = $this->guard_model->get_total_count_of_records();
            $checkpost_assign_guard = $this->guard_model->get_assign_count_of_guard();
            $total_assign_guard = $checkpost_assign_guard + VALUE_ONE;
            if ($total_guard <= $total_assign_guard) {
                echo json_encode(get_error_array("You Can't be Deactive this guard, because you Require a Minimum $total_assign_guard Guards for Randomization Process   !"));
                return false;
            }
        }

        $update_user_data = array();
        $update_user_data['is_active'] = $type;
        $update_user_data['updated_by'] = $session_user_id;
        $update_user_data['updated_time'] = date('Y-m-d H:i:s');
        $this->utility_model->update_data('guard_id', $guard_id, 'guard', $update_user_data);

        $new_user_data = $this->utility_model->get_by_id('guard_id', $guard_id, 'guard');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode(get_error_array(DATABASE_ERROR_MESSAGE));
            return;
        }
        $msg = $type == IS_DEACTIVE ? 'Deactivated' : 'Activated';
        $success_array = get_success_array();
        $success_array['message'] = $new_user_data['guard_name'] . " $msg Successfully.";
        echo json_encode($success_array);
    }

}

/*
 * EOF: ./application/controller/Guard.php
 */