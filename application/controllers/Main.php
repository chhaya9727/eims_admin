<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        check_authenticated();
    }

    public function index() {
        $this->load->view('common/header');
        $this->load->view('main/main');
        $this->load->view('common/footer');
        $this->load->view('common/backbone_footer');
    }

    function _get_basic_array_for_zone_wise_industries($district, &$zone_wise_industries) {
        $zone_wise_industries['total_red_zone_industries_' . $district] = 0;
        $zone_wise_industries['total_orange_zone_industries_' . $district] = 0;
        $zone_wise_industries['total_blue_zone_industries_' . $district] = 0;
        $zone_wise_industries['total_zone_industries_' . $district] = 0;
    }

    function _get_basic_array_for_employee($district, &$employee) {
        $employee['total_employee_in_' . $district] = 0;
        $employee['total_vaccinated_in_' . $district] = 0;
        $employee['total_employee_in_' . $district . '_from_dnh'] = 0;
        $employee['total_employee_in_' . $district . '_from_daman'] = 0;
        $employee['total_employee_in_' . $district . '_from_diu'] = 0;
        $employee['total_employee_in_' . $district . '_from_other'] = 0;
    }

    function _calculate_array_for_zone_wise_industries($session_district, &$zone_wise_industries) {
        $temp_zone_wise_data = $this->dashboard_model->get_zone_wise_industries_count($session_district);
        if (!empty($temp_zone_wise_data)) {
            foreach ($temp_zone_wise_data as $tzwd) {
                if ($tzwd['total_score'] <= 40) {
                    $zone_wise_industries['total_red_zone_industries_' . $tzwd['district']] += $tzwd['total_record'];
                } else if ($tzwd['total_score'] > 40 && $tzwd['total_score'] <= 70) {
                    $zone_wise_industries['total_orange_zone_industries_' . $tzwd['district']] += $tzwd['total_record'];
                } else if ($tzwd['total_score'] > 70) {
                    $zone_wise_industries['total_blue_zone_industries_' . $tzwd['district']] += $tzwd['total_record'];
                }
                $zone_wise_industries['total_zone_industries_' . $tzwd['district']] += $tzwd['total_record'];
            }
        }
    }

    function _calculate_array_for_employee($session_district, &$employee) {
        $temp_emp_data = $this->dashboard_model->get_employee_count($session_district);
        if (!empty($temp_emp_data)) {
            foreach ($temp_emp_data as $ted) {
                $employee['total_vaccinated_in_' . $ted['district']] += $ted['total_vaccinated_emp'];
                $employee['total_employee_in_' . $ted['district']] += ($ted['from_dnh_emp'] + $ted['from_daman_emp'] + $ted['from_diu_emp'] + $ted['from_other_emp']);
                $employee['total_employee_in_' . $ted['district'] . '_from_dnh'] += $ted['from_dnh_emp'];
                $employee['total_employee_in_' . $ted['district'] . '_from_daman'] += $ted['from_daman_emp'];
                $employee['total_employee_in_' . $ted['district'] . '_from_diu'] += $ted['from_diu_emp'];
                $employee['total_employee_in_' . $ted['district'] . '_from_other'] += $ted['from_other_emp'];
            }
        }
    }

    function get_dashboard_data() {
        $this->load->model('dashboard_model');
        $session_user_id = get_from_session('temp_id_for_eims_admin');
        $session_district = get_from_session('temp_district_for_safe_admin');
        $success_array = get_success_array();
        $success_array['zone_wise_industries'] = array();
        $success_array['employee'] = array();
        if (is_admin() || is_sec_user() || (is_labour_dept_user() && is_daman_user())) {
            $this->_get_basic_array_for_zone_wise_industries(TALUKA_DAMAN, $success_array['zone_wise_industries']);
            $this->_get_basic_array_for_employee(TALUKA_DAMAN, $success_array['employee']);
        }
        if (is_admin() || is_sec_user() || (is_labour_dept_user() && is_diu_user())) {
            $this->_get_basic_array_for_zone_wise_industries(TALUKA_DIU, $success_array['zone_wise_industries']);
            $this->_get_basic_array_for_employee(TALUKA_DIU, $success_array['employee']);
        }
        if (is_admin() || is_sec_user() || (is_labour_dept_user() && is_dnh_user())) {
            $this->_get_basic_array_for_zone_wise_industries(TALUKA_DNH, $success_array['zone_wise_industries']);
            $this->_get_basic_array_for_employee(TALUKA_DNH, $success_array['employee']);
        }
        $this->db->trans_start();
        if (is_admin() || is_sec_user() || is_labour_dept_user()) {
            $this->_calculate_array_for_zone_wise_industries($session_district, $success_array['zone_wise_industries']);
            $this->_calculate_array_for_employee($session_district, $success_array['employee']);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo json_encode($success_array);
            return;
        }
        echo json_encode($success_array);
    }

}

/*
 * EOF: ./application/controller/Main.php
 */